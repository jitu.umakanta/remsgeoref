package configuration;

import lib.gps.GpsDistance;

/**
 * Created by sumeendranath on 27/08/17.
 */

public class AndroidDefaults {

    //// SOFTWARE VERSION
    public static String SOFTWAREVERSION              = "1000";


    //// EXTERNAL FOLDER
    public static String FOLDER_MEMORYCARD              = Config.PROJECTID;
    public static String UPLOADDIR_MEMORYCARD              = "UPLOAD";
    public static String TEMPDIR_MEMORYCARD              = "TEMP";
    public static String PHOTOS_TEMPDIR_MEMORYCARD      = "PHOTOSTEMP";
    public static String SIGNATURE_TEMPDIR_MEMORYCARD   = "SIGTEMP";
    public static String AUDIO_TEMPDIR_MEMORYCARD       = "AUDIOTEMP";
    public static String SELFIE_TEMPDIR_MEMORYCARD      = "SELFTEMP";


    public static long EMBEDTEXTSIZE = 30;

    //// signature
    public static int SIGNATURESTROKEWIDTH = 20;


    //// uploading
    public static long SCHEDULE_UPLOADRECURRING = 20;


    ////// ANDROID SPECFIC
    public static String GPS_LOCATIONPROVDER = "";
    public static GpsDistance.ACCURACY GPS_ACCURACY = GpsDistance.ACCURACY.COARSE;
    public static Double GPS_MINDISTANCE_LOCUPDATE = 10d; //// in meters
    public static Double GPS_MINDISTANCE_FILTERPOINTS = 5000d; //// in meters
    public static Double GPS_SELECTABLEDISTANCE = 5000d; //// in meters
    public static int GPS_DEFAULT_ZOOM = 19; //// in meters


    //// Passcode file
    public static String PASSCODEFILENAME = "passcodefile.mp3";



    /// google drive API
    public static String GOOGLE_ROUTE_API = "https://www.google.com/maps/dir/?api=1";



    /// CAMERA
    public static int PHOTOMAIN_QUALITY = 80;
    public static int PHOTO_IDEALHEIGHT = 640;
    public static int PHOTO_IDEALWIDTH = 640;


    public static int IMAGE_IDEALJPEGCOMPRESS = 80;



    /// TAGS for unique files
    public static String TAG_SIGNATURE = "_SIG";
    public static String TAG_PHOTO = "_PHO";
    public static String TAG_SELFIE = "_SEL";
    public static String TAG_AUDIO = "_AUD";
    public static String TAG_CONSUME = "_CON";

    /// Extensions for unique files
    public static String EXT_SIGNATURE = ".jpg";
    public static String EXT_PHOTO = ".jpg";
    public static String EXT_SELFIE = ".jpg";
    public static String EXT_AUDIO = ".wav";
    public static String EXT_CONSUME = ".json";
    public static String EXT_REPORT = ".pdf";



    //// REPORT
    public static int PDF_HEIGHT = 2100;
    public static int PDF_WIDTH = 1275;
    public static int PDF_MARGINX = 50;
    public static int PDF_MARGINY = 50;


}
