package configuration;

import java.util.ArrayList;
import java.util.Arrays;

import lib.vars.Values;

/**
 * Created by sumeendranath on 27/08/17.
 */

public class Config {

    //public static String SERVER_URL = "http://whitesuntech.in:4568/";
    //public static String SERVER_URL = "http://192.168.1.8:4567/";//office
    //public static String SERVER_URL = "http://192.168.1.7:5001/";//office-Ram's laptop
    public static String SERVER_URL = "http://whitesuntech.in:4567/api/";//mobile
    //public static String SERVER_URL = "http://192.168.100.20:4567/";//pg
    //public static String SERVER_URL = "http://192.168.1.5:4567/";


    /// PROJECT VARS
    public static String PROJECTID = "RVNLGEOREF";
    public static String PROJECTNAME = "RVNL Georeferencing";
    public static String PROJECTTITLE = "RVNL Georeferencing App";
    public static String PROJECTDESC = "This is a mobile app for RVNL Georef";
    public static String CUSTOMERNAME = "Rail Vikas Nigam Limited";



    public static int MAX_HIERARCHY_LEVELS = 7;
    public static String HIERARCHY_DELIMITER = ",";
    public static String COMPLETEADDRESS_SEP = "|";


    public static ArrayList<String> HIERARCHYNAMES = new ArrayList<String>(Arrays.asList("State", "PIU", "Main Project","Sub Project","Work Type","Work Location","Photo Point"));


    /// MODES
    public static Values.RUNMODE RELEASE_MODE = Values.RUNMODE.RELEASE_VERBOSE;

    /// FOLDERS
    public static String EXTRENALBASEURL = "http://rmsrvnl.com/rems/";
    public static String PROJIMAGEURL = EXTRENALBASEURL + "rvnl_images/";


    //// API JSON FILE
    public static String UPLOAD_API_JSONFILEEXT              = "upload.api";

}
