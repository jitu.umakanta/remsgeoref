package configuration;

import java.util.Arrays;
import java.util.List;

/**
 * Created by sumeendranath on 28/08/17.
 */

public class DirectApiConfig {

    // 02:01 PM " 24-11-2017  " by jitu '
    public static String DF_GETGPSDATA                    = "getgpsdata.json";
    public static String DF_GETMOBUSERS                   = "getmobusers.json";
    public static String DF_GETPHOTOPOINTLIST             = "getphotopointlist.json";

    // 04:37 PM " 24-11-2017  " by jitu '
    public static String DF_MOBCHECKLISTDEF="mobchecklistdef.json";
    public static String DF_MOBCHECKLISTTYPE="mobchecklisttypedef.json";
    public static String DF_MOBLOCATIONDEF="moblocationdef.json";
    public static String DF_MOBPROGRESSSTATUSDEF="mobprogresstatusdef.json";
    public static String DF_MOBPROJECTDEF="mobprojectdef.json";
    public static String DF_MOBPROJECTLOCATIONDEF="mobprojectlocationdef.json";
    public static String DF_MOBQUESTIONDEF="mobquestiondef.json";
    public static String DF_MOBQUESTIONGROUPDEF="mobquestiongroupdef.json";
    public static String DF_MOBQUESTIONTYPE="mobquestiontypedef.json";
    public static String DF_MOBROLESCHECKLISTDEF="mobroleschecklistdef.json";
    public static String DF_MOBROLESDEF="mobrolesdef.json";
    public static String DF_MOBTASKCHECKLISTDEF="mobtaskchecklistdef.json";
    public static String DF_MOBTASKDEF="mobtaskdef.json";
    public static String DF_MOBUSERDEF="mobuserdef.json";
    public static String DF_MOBUSERPROJECTROLESDEF="mobuserprojectrolesdef.json";
    public static String DF_MOBUSERTASKDEF="mobusertaskdef.json";








    public static String DF_ALLMOBILEINPUTCOMPRESSED         = "all.7z";


    public static List<String> DF_LIST_MOBILEDATAFILES = Arrays.asList(
            DF_GETGPSDATA, DF_GETMOBUSERS,DF_GETPHOTOPOINTLIST,DF_MOBUSERDEF,DF_MOBUSERPROJECTROLESDEF
    );




    public static String DF_HIERARCHYCOMPLETE               = "hierarchycomplete.json";
    public static String DF_HIERARCHYTONAME                 = "hierarchytoname.json";
    public static String DF_HIERARCHYTODESCRIPTION          = "hierarchytodescription.json";
    public static String DF_GPSTOLOCATION                   = "gpstolocation.json";
    public static String DF_HIERLEVELTOHIERID               = "hierleveltoid.json";
    public static String DF_HIERARCHYTOADDRESS              = "hierarchytoaddress.json";

   /* public static String DF_ALLMOBILEINPUTCOMPRESSED         = "allmobileinputcompressed.7z";


    public static List<String> DF_LIST_MOBILEDATAFILES = Arrays.asList(
            DF_HIERARCHYTONAME, DF_HIERARCHYTODESCRIPTION,DF_GPSTOLOCATION,DF_HIERLEVELTOHIERID,DF_HIERARCHYTOADDRESS
    );
*/


}
