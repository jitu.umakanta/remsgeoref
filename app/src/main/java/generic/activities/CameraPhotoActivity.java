package generic.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.Facing;
import com.whitesun.sparshhospital.BR;
import com.whitesun.sparshhospital.R;
import com.whitesun.sparshhospital.databinding.ActivityCameraPhotoBinding;
import com.whitesun.sparshhospital.databinding.ItemPhotogallerySmallBinding;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.UiThread;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import configuration.AndroidDefaults;
import httpdata.models.android.PhotoGallery;
import lib.ui.activity.WActivity;
import lib.ui.image.WPhoto;
import lib.utils.common.Wlog;
import lib.utils.datastructures.WConvert;
import lib.utils.file.FileExternal;
import lib.utils.file.FileUtils;
import projectspecific.Unique;



@EActivity
@Fullscreen
public class CameraPhotoActivity extends WActivity {



    public static String EXTRA_CAMERATYPE = "cameratype";
    public static String EXTRA_PHOTOLIMIT = "photolimit";
    public static String EXTRA_UNIQUEID = "uniqueid";


    public static String EXTRA_RESULTPHOTOITEMS = "photoitems";
    public static String EXTRA_RESULTUNIQUEID = "uniqueid";

    public static enum CAMERATYPE
    {
        FRONTFACING,
        BACKFACING
    }

ActivityCameraPhotoBinding vars;
ObservableList<PhotoGallery> photogalleryitems = new ObservableArrayList<>();
static int MAXPHOTOS = 1;
int _photolimit = MAXPHOTOS;

CAMERATYPE _cameratype = CAMERATYPE.BACKFACING;
String _uniqueid = "";


    public static void startBackCamera(Context context, int resultcode)
    {
        Intent i=new Intent(context,CameraPhotoActivity.class);
        i.putExtra(EXTRA_CAMERATYPE, CAMERATYPE.BACKFACING);
        i.putExtra(EXTRA_PHOTOLIMIT,MAXPHOTOS);
        ((Activity)context).startActivityForResult(i, resultcode);
    }

    public static void startFrontCamera(Context context,int resultcode, String uniqueid)
    {
        Intent i=new Intent(context,CameraPhotoActivity.class);
        i.putExtra(EXTRA_CAMERATYPE, CAMERATYPE.FRONTFACING);
        i.putExtra(EXTRA_PHOTOLIMIT,1);
        i.putExtra(EXTRA_UNIQUEID,uniqueid);
        ((Activity)context).startActivityForResult(i, resultcode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        intentvars();
        init();
        handleclicks();
        handleMenu();
        execute();

    }

    void intentvars()
    {
        _cameratype = (CAMERATYPE) getIntent().getSerializableExtra(EXTRA_CAMERATYPE);
        _photolimit =  getIntent().getIntExtra(EXTRA_PHOTOLIMIT, -1);
        _uniqueid = getIntent().getStringExtra(EXTRA_UNIQUEID);
    }

    void init()
    {
        vars  = DataBindingUtil.setContentView(this, R.layout.activity_camera_photo);
        vars.camera.setJpegQuality(AndroidDefaults.PHOTOMAIN_QUALITY);

        if(_cameratype == CAMERATYPE.BACKFACING) {
            vars.camera.setFacing(Facing.BACK);
        }
        else
        {
            vars.camera.setFacing(Facing.FRONT);
        }


        vars.photogallery.setLayout(this, LinearLayoutManager.HORIZONTAL);

        ItemType<ItemPhotogallerySmallBinding> itemtype = new ItemType<ItemPhotogallerySmallBinding>(R.layout.item_photogallery_small) {
            @Override
            public void onBind(@NotNull final Holder<ItemPhotogallerySmallBinding> holder) {

            }


        }; 

        new LastAdapter(photogalleryitems, BR.photoitem)
                .map(PhotoGallery.class,itemtype)
                .into(vars.photogallery);


        hideprogress();
        vars.progressbar.setText("Processing");
    }


    @UiThread
    void addToPhotoGallery(PhotoGallery pg)
    {
        photogalleryitems.add(pg);

        if(photogalleryitems.size() >= _photolimit)
        {
            closeAct();
        }

        hideprogress();

    }
    @Background
    void processingphoto(byte[]jpeg)
    {
        String uniqueid = Unique.getUniqueDataId();



        String filepath = FileExternal.getPhotosTempDir() + uniqueid;

        if(_cameratype == CAMERATYPE.FRONTFACING)
        {
            filepath = FileExternal.getSelfieTempDir() + FileExternal.getUploadFileName(uniqueid,AndroidDefaults.TAG_SELFIE  , AndroidDefaults.EXT_SELFIE);
        }
        else
        {
            filepath = FileExternal.getPhotosTempDir() + FileExternal.getUploadFileName(uniqueid,AndroidDefaults.TAG_PHOTO  , AndroidDefaults.EXT_PHOTO);
        }

        FileUtils.writeIntoBinaryFile(filepath, jpeg);


        WPhoto.reducepicturesize(filepath,AndroidDefaults.PHOTO_IDEALWIDTH);


        PhotoGallery pg = new PhotoGallery();
        pg.imagepath = filepath;
        pg.uniqueid = uniqueid;


        addToPhotoGallery(pg);
    }

    @UiThread
    void showprogress()
    {
        vars.progressbar.setInfinite();
        vars.progressbar.setVisibility(View.VISIBLE);
    }

    @UiThread
    void hideprogress()
    {
        vars.progressbar.setInfinite();
        vars.progressbar.setVisibility(View.GONE);
    }

    void handleclicks()
    {
        vars.camera.addCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(byte[] jpeg) {
                super.onPictureTaken(jpeg);

/*


                String uniqueid = Unique.getUniqueDataId();
                String filepath = FileExternal.getPhotosTempDir() + uniqueid;
                FileUtils.writeIntoBinaryFile(filepath, jpeg);


                WPhoto.reducepicturesize(filepath,AndroidDefaults.PHOTO_IDEALWIDTH);


                PhotoGallery pg = new PhotoGallery();
                pg.imagepath = filepath;
                pg.uniqueid = uniqueid;

                photogalleryitems.add(pg);

                if(photogalleryitems.size() >= _photolimit)
                {
                    closeAct();
                }
                */

                processingphoto(jpeg);
                showprogress();


            }
        });


        vars.btClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vars.camera.capturePicture();
            }
        });




    }
    void handleMenu()
    {

    }
    void execute()
    {

    }


    void closeAct()
    {

        Intent intent = getIntent();
        Wlog.d("before sizes = " + photogalleryitems.size());


        ArrayList<PhotoGallery> pglist = new WConvert<PhotoGallery>().observeListToArrayList(photogalleryitems);

        Wlog.d("sizes = " + photogalleryitems.size() + "," + pglist.size());


        if(photogalleryitems.size() > 0) {
            intent.putParcelableArrayListExtra(EXTRA_RESULTPHOTOITEMS, pglist);
            intent.putExtra(EXTRA_RESULTUNIQUEID,_uniqueid);
            setResult(RESULT_OK, intent);
        }
        else
        {
            intent.putParcelableArrayListExtra(EXTRA_RESULTPHOTOITEMS, pglist);
            intent.putExtra(EXTRA_RESULTUNIQUEID,_uniqueid);
            setResult(RESULT_CANCELED, intent);
        }

        finish();
    }



    @Override
    public void onBackPressed() {
        closeAct();
    }

    @Override
    protected void onResume() {
        super.onResume();
        vars.camera.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        vars.camera.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        vars.camera.destroy();
    }




}
