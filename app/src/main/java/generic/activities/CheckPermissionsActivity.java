package generic.activities;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;


import com.whitesun.sparshhospital.R;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;


@Fullscreen
@EActivity
public class CheckPermissionsActivity extends AppCompatActivity {


    static final int PHOTO_BACK_CAMERA_REQUESTCODE = 101;
    static final int WRITESTORAGE_REQUESTCODE = 102;
    static final int READSTORAGE_REQUESTCODE = 103;
    static final int GPS_REQUESTCODE_COARSE = 105;
    static final int GPS_REQUESTCODE_FINE = 106;
    static final int RECEIVESMS_REQUESTCODE = 107;
    static final int READSMS_REQUESTCODE = 108;
    static final int READPHONESTATE_REQUESTCODE_FINE = 109;
    static final int CALLPHONE_REQUESTCODE = 110;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_permissions);
        init();
        handleclicks();
        execute();
    }

    void init()
    {

    }

    void handleclicks()
    {
    }
    void execute()
    {
        requestGpsCoarse();
    }


    void nextAction()
    {
        //// All permissions are in place... now the app can start
        //// oops the permissions not set.. the app will not be able to function
        setResult(Activity.RESULT_OK);
      //  LoginActivity_.intent(CheckPermissionsActivity.this).start();
        finish();
    }

    void closeAct()
    {
        //// oops the permissions not set.. the app will not be able to function
        setResult(Activity.RESULT_CANCELED);
        finish();
    }



    void requestGpsCoarse()
    {
        // Check permission for CAMERA
        if (ActivityCompat.checkSelfPermission(CheckPermissionsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            // Callback onRequestPermissionsResult interceptado na Activity MainActivity

            ActivityCompat.requestPermissions(CheckPermissionsActivity.this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    CheckPermissionsActivity.GPS_REQUESTCODE_COARSE);
        } else {
            // permission has been granted, continue as usual
            // new PhotoBackCamera(CheckPermissionsActivity.this, "testing");

            requestGpsFine();
        }
    }

    void requestGpsFine()
    {
        // Check permission for CAMERA
        if (ActivityCompat.checkSelfPermission(CheckPermissionsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            // Callback onRequestPermissionsResult interceptado na Activity MainActivity

            ActivityCompat.requestPermissions(CheckPermissionsActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    CheckPermissionsActivity.GPS_REQUESTCODE_FINE);
        } else {
            // permission has been granted, continue as usual
            // new PhotoBackCamera(CheckPermissionsActivity.this, "testing");

            // requestCamera();

            requestNetworkState();
        }
    }

    void requestCamera()
    {
        // Check permission for CAMERA
        if (ActivityCompat.checkSelfPermission(CheckPermissionsActivity.this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            // Callback onRequestPermissionsResult interceptado na Activity MainActivity

            ActivityCompat.requestPermissions(CheckPermissionsActivity.this,
                    new String[]{Manifest.permission.CAMERA},
                    CheckPermissionsActivity.PHOTO_BACK_CAMERA_REQUESTCODE);
        } else {
            // permission has been granted, continue as usual
            // new PhotoBackCamera(CheckPermissionsActivity.this, "testing");

            requestNetworkState();
        }
    }

    void requestNetworkState()
    {
        // Check permission for CAMERA
        if (ActivityCompat.checkSelfPermission(CheckPermissionsActivity.this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            // Callback onRequestPermissionsResult interceptado na Activity MainActivity

            ActivityCompat.requestPermissions(CheckPermissionsActivity.this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    CheckPermissionsActivity.READPHONESTATE_REQUESTCODE_FINE);
        } else {
            // permission has been granted, continue as usual
            // new PhotoBackCamera(CheckPermissionsActivity.this, "testing");

            requestWriteStorage();
        }
    }




    void requestWriteStorage()
    {
        if (ActivityCompat.checkSelfPermission(CheckPermissionsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            // Callback onRequestPermissionsResult interceptado na Activity MainActivity

            ActivityCompat.requestPermissions(CheckPermissionsActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    CheckPermissionsActivity.WRITESTORAGE_REQUESTCODE);
        } else {
            // permission has been granted, continue as usual
            requestReadStorage();
        }
    }

    void requestReadStorage()
    {
        if (ActivityCompat.checkSelfPermission(CheckPermissionsActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            // Callback onRequestPermissionsResult interceptado na Activity MainActivity

            ActivityCompat.requestPermissions(CheckPermissionsActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    CheckPermissionsActivity.READSTORAGE_REQUESTCODE);
        } else {
            // permission has been granted, continue as usual
            // new PhotoBackCamera(CheckPermissionsActivity.this, "testing");

            nextAction();
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == GPS_REQUESTCODE_COARSE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera

                requestGpsFine();
            }
            else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
                Toast.makeText(this, "NO PERMISSION FOR LOCATION ACCESS" , Toast.LENGTH_SHORT).show();

                closeAct();

            }
        }

        if (requestCode == GPS_REQUESTCODE_FINE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera

                requestCamera();
            }
            else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
                Toast.makeText(this, "NO PERMISSION FOR LOCATION ACCESS" , Toast.LENGTH_SHORT).show();

                closeAct();

            }
        }


        if (requestCode == PHOTO_BACK_CAMERA_REQUESTCODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera

                requestNetworkState();
            }
            else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
                Toast.makeText(this, "NO PERMISSION FOR CAMERA ACCESS" , Toast.LENGTH_SHORT).show();
                closeAct();

            }
        }


        if (requestCode == READPHONESTATE_REQUESTCODE_FINE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera

                requestWriteStorage();
            }
            else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
                Toast.makeText(this, "NO PERMISSION FOR READING PHONE STATE" , Toast.LENGTH_SHORT).show();
                closeAct();

            }
        }


        if (requestCode == WRITESTORAGE_REQUESTCODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera
                requestReadStorage();
            }
            else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
                Toast.makeText(this, "NO PERMISSION FOR STORAGE READ ACCESS" , Toast.LENGTH_SHORT).show();
                closeAct();

            }
        }

        if (requestCode == READSTORAGE_REQUESTCODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera


                nextAction();
                //initiateScanning();
            }
            else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
                Toast.makeText(this, "NO PERMISSION FOR STORAGE WRITE ACCESS" , Toast.LENGTH_SHORT).show();

                closeAct();

            }
        }

    }




}
