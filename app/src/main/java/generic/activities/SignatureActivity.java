package generic.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.whitesun.sparshhospital.R;
import com.whitesun.sparshhospital.databinding.ActivitySignatureBinding;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;

import configuration.AndroidDefaults;
import lib.ui.image.WPhoto;
import lib.utils.common.Wlog;
import lib.utils.file.FileExternal;
import lib.utils.file.FileUtils;

@EActivity
@Fullscreen
public class SignatureActivity extends AppCompatActivity {

    public static String EXTRA_UNIQUEID = "uniqueid";
    public static String EXTRA_RETURN_SIGNATUREPATH = "signaturepath";
    public static String EXTRA_RETURN_UNIQUEID = "uniqueid";


    public static void start(Context context,int requestcode, String uniqueid)
    {
        Intent i=new Intent(context,SignatureActivity_.class);
        i.putExtra(EXTRA_UNIQUEID, uniqueid);
        ((Activity)context).startActivityForResult(i, requestcode);
    }




    ActivitySignatureBinding vars;
    String _uniqueid = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        intentvars();
        init();
        handleclicks();
        handleMenu();
        execute();




    }

    void intentvars()
    {
        _uniqueid = (String) getIntent().getStringExtra(EXTRA_UNIQUEID);
        Wlog.d("Got uniquid from data act : " + _uniqueid);
    }

    void init()
    {
        vars = DataBindingUtil.setContentView(this, R.layout.activity_signature);

    }
    void handleclicks()
    {

        vars.btsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(vars.signaturepad.isEmpty())
                {
                    Wlog.d("Signature is cancelled!!");
                    Intent intent = getIntent();
                    setResult(RESULT_CANCELED, intent);
                    finish();
                }
                else
                {
                    Intent intent = getIntent();
                    intent.putExtra(EXTRA_RETURN_SIGNATUREPATH,processSignature());
                    intent.putExtra(EXTRA_RETURN_UNIQUEID,_uniqueid);

                    setResult(RESULT_OK, intent);
                    finish();
                }


            }
        });

        vars.signaturepad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {



            }

            @Override
            public void onSigned() {

                //vars.signaturepad.getSignatureBitmap();
            }

            @Override
            public void onClear() {

            }
        });

    }


    String processSignature()
    {
        String filepath = FileExternal.getSignatureTempDir() +  FileExternal.getUploadFileName(_uniqueid , AndroidDefaults.TAG_SIGNATURE , AndroidDefaults.EXT_SIGNATURE);
        FileUtils.saveBitmapToFile(vars.signaturepad.getSignatureBitmap(),filepath);

        WPhoto.reducepicturesize(filepath, AndroidDefaults.PHOTO_IDEALWIDTH);

        return filepath;

    }
    void handleMenu()
    {

    }
    void execute()
    {

    }
}

