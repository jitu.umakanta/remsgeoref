package genericfragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.databinding.library.baseAdapters.BR;
import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;
import com.whitesun.sparshhospital.R;
import com.whitesun.sparshhospital.databinding.FragmentSlideupViewBinding;
import com.whitesun.sparshhospital.databinding.SliderExpandPointinfoBinding;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import configuration.Config;
import httpdata.models.android.LocationCard;
import httpdata.models.direct.LocationDetails;
import lib.gps.GpsDistance;
import lib.map.MapRoute;
import lib.ui.activity.WActivity;
import lib.ui.fragment.WFragment;
import lib.utils.string.StringUtils;


public class SlideupViewFragment extends WFragment {


    public SlideupViewFragment() {
        // Required empty public constructor
    }


    FragmentSlideupViewBinding fragvars;
    Map<Double,LocationDetails> _locationdetials = new HashMap<>();
    ObservableList<LocationCard> locationcards = new ObservableArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

         fragvars = DataBindingUtil.inflate(inflater,
                R.layout.fragment_slideup_view, container, false);;

        init(inflater, container);

        return fragvars.getRoot();
    }



    void init(LayoutInflater inflater, ViewGroup container)
    {
        fragvars.testtext.setText("This is view that is set");

        fragvars.smallportion.btDriveto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(StringUtils.stringIsNullOrEmpty((String) view.getTag()))
                {
                    return;
                }
                MapRoute.driveTo(getActivity(),(String)view.getTag());
            }
        });
        fragvars.smallportion.btSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendDataToActivity(fragvars.smallportion.getCarditem());
            }
        });



        fragvars.smallportion.setCarditem(new LocationCard("Initialising...","","","","",""));
        fragvars.smallportion.recommenedtag.setVisibility(View.GONE);

        ItemType<SliderExpandPointinfoBinding> itemtype = new ItemType<SliderExpandPointinfoBinding>(R.layout.slider_expand_pointinfo) {
            @Override
            public void onBind(@NotNull final Holder<SliderExpandPointinfoBinding> holder) {

                holder.getBinding().btDriveto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(StringUtils.stringIsNullOrEmpty((String) view.getTag()))
                        {
                            return;
                        }
                        MapRoute.driveTo(getActivity(),(String)view.getTag());
                    }
                });

                holder.getBinding().btSelect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        sendDataToActivity(holder.getBinding().getCarditem());
                    }
                });
            }
        };

        new LastAdapter(locationcards, BR.carditem)
                .map(LocationCard.class,itemtype)
                .into(fragvars.recycleexpandview);

    }


    public  void sendDataToActivity(LocationCard locationcard)
    {
        ((WActivity) getActivity()).datafromFragment(locationcard);
    }

    @Override
    public void getDataFromActivity(Object object) {
        super.getDataFromActivity(object);

        fragvars.smallportion.recommenedtag.setVisibility(View.VISIBLE);

        _locationdetials = (Map<Double,LocationDetails>)object;

        if(_locationdetials.size() > 0) {
            Double firstdistance = getFirstKey(_locationdetials);
            fragvars.smallportion.setCarditem(convertLocationDetailsToLocationCard(firstdistance,_locationdetials.get(firstdistance)));
        }

        for(Map.Entry<Double, LocationDetails> entry : _locationdetials.entrySet())
        {
            locationcards.add(convertLocationDetailsToLocationCard(entry.getKey(),entry.getValue()));
        }

    }

    LocationCard convertLocationDetailsToLocationCard(Double distance, LocationDetails ld)
    {
        LocationCard lc = new LocationCard("assigning","","","","","");

        lc.name = ld.getHier_desc();
        lc.hierid = ld.getFull_hier();
        lc.gpslat = ld.GPS_LAT;
        lc.gpslon = ld.GPS_LON;
        lc.distance = "" + distance.intValue() + " meters ";
        lc.completeaddress = ld.address;
        lc.gpscombined = ld.GPS_LAT + GpsDistance.GPS_SEP + ld.GPS_LON;

        lc.hiername = "PP" ;//Config.HIERARCHYNAMES.get(Config.HIERARCHYNAMES.size() - 1);

        List<String> heirnames = StringUtils.split(ld.address, Config.COMPLETEADDRESS_SEP);
        if(Config.MAX_HIERARCHY_LEVELS > 1) {
            lc.hiernameparent1 = "WL";//Config.HIERARCHYNAMES.get(Config.HIERARCHYNAMES.size() - 2);
            lc.parent1 = heirnames.get(Config.MAX_HIERARCHY_LEVELS - 2);
        }

        if(Config.MAX_HIERARCHY_LEVELS > 2) {
            lc.hiernameparent2 = "WT";// Config.HIERARCHYNAMES.get(Config.HIERARCHYNAMES.size() - 3);
            lc.parent2 = heirnames.get(Config.MAX_HIERARCHY_LEVELS - 3);
        }

        if(Config.MAX_HIERARCHY_LEVELS > 3) {
            lc.hiernameparent3 = "Sub Proj";// Config.HIERARCHYNAMES.get(Config.HIERARCHYNAMES.size() - 3);
            lc.parent3 = heirnames.get(Config.MAX_HIERARCHY_LEVELS - 4);
        }

        if(Config.MAX_HIERARCHY_LEVELS > 4) {
            lc.hiernameparent4 = "Main Proj";// Config.HIERARCHYNAMES.get(Config.HIERARCHYNAMES.size() - 3);
            lc.parent4 = heirnames.get(Config.MAX_HIERARCHY_LEVELS - 5);
        }

        return lc;
    }

    public static Double getFirstKey(Map<Double,LocationDetails> map)
    {
        Map.Entry<Double,LocationDetails> entry=map.entrySet().iterator().next();
        return entry.getKey();
    }





    private OnFragmentInteractionListener mListener;

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(LocationCard lc) {
        if (mListener != null) {
            mListener.onFragmentInteraction(lc);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by generic.activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(LocationCard lc);
    }
}
