package genericservices;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import org.androidannotations.annotations.EService;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import configuration.AndroidDefaults;
import lib.rest.HttpUpload;
import lib.timer.ScheduleTask;
import lib.utils.common.Wlog;
import lib.utils.file.FileExternal;
import lib.utils.file.FileUtils;
import lib.utils.network.Connectivity;
import offlinemodesave.Offline;
import offlinemodesave.UploadOffline;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import static android.content.ContentValues.TAG;

@EService
public class UploadBackgroundService extends Service {


    ScheduleTask ST;
    List<String> allfiles = new ArrayList<>();
    int filecounter = 0;

    public UploadBackgroundService() {
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ST = new ScheduleTask(0, AndroidDefaults.SCHEDULE_UPLOADRECURRING, "Upload background service!") {
            @Override
            protected void onCall() {
                super.onCall();
                checkServerAndUpload();


            }
        };


        return Service.START_STICKY;
    }


    @Override
    public IBinder onBind(Intent intent) {


        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    void checkServerAndUpload() {
        if (!Connectivity.checkNetworkConnectivity(this)) {
            Wlog.i("no network connectivity");
            return;
        }


        if (FileUtils.getAllFilesWithinDirectory(FileExternal.getUploadDir()).size() <= 0) {
            Wlog.i("no pending files for upload");
            return;
        }

        new HttpUpload<ResponseBody>() {
            @Override
            protected void onComplete(ResponseBody response) {
                super.onComplete(response);
                Wlog.i("Server is available! Uploads can be performed");
                doUpload();
            }

            @Override
            protected void onError(String message) {
                super.onError(message);
                Wlog.i("Server is not available!");
            }
        }.httpConnect(HttpUpload.APIS.serverIsAvailable());
    }


    void doUpload() {

        if (Connectivity.checkNetworkConnectivity(this)) {

            //// if allfiles list has files then uploads are happenning... dont disturb now
            if (allfiles.size() > 0) {
                Wlog.i("Uploads in process. do not disturb");
                // 07:11 PM " 23-11-2017  " by jitu '

                UploadOffline uploadOffline = new UploadOffline();
                uploadOffline.uploadOffline();

                return;
            }

            allfiles = FileUtils.getAllFilesWithinDirectory(FileExternal.getUploadDir());

            if (allfiles.size() > 0) {


                filecounter = 0;

                uploadFile(filecounter);
            } else {
                Wlog.i("no pending uploads");
                //CommonlyUsed.showMsg(getApplicationContext(),"No Pending Uploads");
            }

        } else {
            Wlog.i("no connectivity");
            //CommonlyUsed.showMsg(getApplicationContext(),"No Connectivity");
        }


    }


    void uploadFile(int index) {
        if (index >= allfiles.size()) {
            allfiles.clear();
            return;
        }


        File file = new File(allfiles.get(index));

        Wlog.i("Uploading : " + file.getName());
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));


        new HttpUpload<ResponseBody>() {
            @Override
            protected void onComplete(ResponseBody response) {
                super.onComplete(response);

                try {
                    //Wlog.i("Success !!" + response.string());
                    if (response.string().toLowerCase().contains("success")) {
                        Wlog.i("removing file : " + allfiles.get(filecounter));
                        FileUtils.removeFile(allfiles.get(filecounter));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }


                filecounter++;
                uploadFile(filecounter);
            }

            @Override
            protected void onError(String message) {
                super.onError(message);
                Wlog.i("received error !!" + message);
                filecounter++;
                uploadFile(filecounter);
            }

        }.httpConnect(HttpUpload.APIS.uploadAttachment(filePart));
    }


}
