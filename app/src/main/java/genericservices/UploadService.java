package genericservices;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

public class UploadService extends IntentService {


    private static final String ACTION_UPLOAD = "genericservices.action.upload";


    public static void start(Context context) {
        Intent intent = new Intent(context, UploadService.class);
        intent.setAction(ACTION_UPLOAD);
        context.startService(intent);
    }


    public UploadService() {
        super("UploadService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_UPLOAD.equals(action)) {
                handleActionUpload();
            }
        }
    }

    private void handleActionUpload() {



    }

}
