package httpdata.examplemodals;

/**
 * Created by anupamchugh on 09/01/17.
 */

public class UserWithoutSerial {


    public String name;
    public String job;
    public String id;
    public String createdAt;

    public UserWithoutSerial(String name, String job) {
        this.name = name;
        this.job = job;
    }


}
