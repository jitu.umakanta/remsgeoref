package httpdata.models.android;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sumeendranath on 31/08/17.
 */

public class LocationCard implements Parcelable{
    public String name = "";
    public String hierid = "";
    public String gpslat = "";
    public String gpslon = "";
    public String distance = "";
    public String completeaddress = "";
    public String gpscombined = "";
    public String parent1 = "";
    public String parent2 = "";
    public String parent3 = "";
    public String parent4 = "";

    public String hiername = "";
    public String hiernameparent1 = "";
    public String hiernameparent2 = "";
    public String hiernameparent3 = "";
    public String hiernameparent4 = "";


    public LocationCard(String name, String hierid, String gpslat, String gpslon, String distance, String completeaddress) {
        this.name = name;
        this.hierid = hierid;
        this.gpslat = gpslat;
        this.gpslon = gpslon;
        this.distance = distance;
        this.completeaddress = completeaddress;
    }

    protected LocationCard(Parcel in) {
        name = in.readString();
        hierid = in.readString();
        gpslat = in.readString();
        gpslon = in.readString();
        distance = in.readString();
        completeaddress = in.readString();
        gpscombined = in.readString();
        parent1 = in.readString();
        parent2 = in.readString();
        parent3 = in.readString();
        parent4 = in.readString();
        hiername = in.readString();
        hiernameparent1 = in.readString();
        hiernameparent2 = in.readString();
        hiernameparent3 = in.readString();
        hiernameparent4 = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(hierid);
        dest.writeString(gpslat);
        dest.writeString(gpslon);
        dest.writeString(distance);
        dest.writeString(completeaddress);
        dest.writeString(gpscombined);
        dest.writeString(parent1);
        dest.writeString(parent2);
        dest.writeString(parent3);
        dest.writeString(parent4);
        dest.writeString(hiername);
        dest.writeString(hiernameparent1);
        dest.writeString(hiernameparent2);
        dest.writeString(hiernameparent3);
        dest.writeString(hiernameparent4);

    }

    @SuppressWarnings("unused")
    public static final Creator<LocationCard> CREATOR = new Creator<LocationCard>() {
        @Override
        public LocationCard createFromParcel(Parcel in) {
            return new LocationCard(in);
        }

        @Override
        public LocationCard[] newArray(int size) {
            return new LocationCard[size];
        }
    };
}
