package httpdata.models.android;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sumeendranath on 03/09/17.
 */

public class PhotoGallery implements Parcelable{
    public String uniqueid = "";
    public String imagepath = "";
    public String hierid = "";
    public String gpslon = "";
    public String gpslat = "";
    public String signaturepath = "";
    public String audiopath = "";
    public String comment = "";
    public String selfiepath = "";
    public String audioisrecorded = "";
    public String noteisrecorded = "";
    public String api = "";
    public String passcode = "";

    public PhotoGallery() {
    }




    protected PhotoGallery(Parcel in) {
        uniqueid = in.readString();
        imagepath = in.readString();
        hierid = in.readString();
        gpslon = in.readString();
        gpslat = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uniqueid);
        dest.writeString(imagepath);
        dest.writeString(hierid);
        dest.writeString(gpslon);
        dest.writeString(gpslat);
    }

    @SuppressWarnings("unused")
    public static final Creator<PhotoGallery> CREATOR = new Creator<PhotoGallery>() {
        @Override
        public PhotoGallery createFromParcel(Parcel in) {
            return new PhotoGallery(in);
        }

        @Override
        public PhotoGallery[] newArray(int size) {
            return new PhotoGallery[size];
        }
    };


}
