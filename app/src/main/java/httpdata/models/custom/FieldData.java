package httpdata.models.custom;


import httpdata.models.database.RemsData;

public class FieldData extends RemsData {

    public String photopath;
    public String hierid;
    public String datemob;
    public String dateserver;



    public FieldData(String entryid, String mid, String imageph, String imagesig, String imageself, String audio, String oimageph, String oimagesig, String oimageself, String oaudio, String gps, String byid, String capon, String synced, String hier_id, String full_hier, String capon_ph, String mnotes, String wnotes, String mile, String imei, String sw_version) {
        super(entryid, mid, imageph, imagesig, imageself, audio, oimageph, oimagesig, oimageself, oaudio, gps, byid, capon, synced, hier_id, full_hier, capon_ph, mnotes, wnotes, mile, imei, sw_version);
    }
}
