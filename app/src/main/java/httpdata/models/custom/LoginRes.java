package httpdata.models.custom;

public class LoginRes {

    public String login;
    public String name;
    public String designation ="";
    public boolean isvalid;

    public LoginRes(String login, String name, String designation, boolean isvalid) {
        this.login = login;
        this.name = name;
        this.designation = designation;
        this.isvalid = isvalid;
    }
}
