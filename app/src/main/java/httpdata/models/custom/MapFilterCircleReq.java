package httpdata.models.custom;



public class MapFilterCircleReq {

    public String radius;
    public String lat;
    public String lon;

    public MapFilterCircleReq(String radiusr, String lat, String lon) {
        this.radius = radiusr;
        this.lat = lat;
        this.lon = lon;
    }
}
