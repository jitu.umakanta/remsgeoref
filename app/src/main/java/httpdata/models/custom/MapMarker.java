package httpdata.models.custom;

public class MapMarker  {
    public String lat;
    public String lon;
    public String url;
    public String hier;
    public String completeaddress;
    public String name;
    public String distance;
    public String detailsurl;
    public String detailstitle;

}
