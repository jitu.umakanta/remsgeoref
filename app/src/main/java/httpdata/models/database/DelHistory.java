package httpdata.models.database;

public class DelHistory {
    private String table_name;
    private String descr;
    private String full_hier;
    private  int level;
    private java.util.Date dt_tm;

    public String getTable_name(){
        return table_name;
    }

    public void setTable_name(String table_name){
        this.table_name=table_name;
    }

    public String getDescr(){
        return descr;
    }

    public void setDescr(String descr){
        this.descr=descr;
    }


    public String getFull_hier(){
        return full_hier;
    }

    public void setFull_hier(String full_hier){
        this.full_hier=full_hier;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public java.util.Date getDt_tm(){
        return dt_tm;
    }

    public void setDt_tm(java.util.Date dt_tm){
        this.dt_tm=dt_tm;
    }
}