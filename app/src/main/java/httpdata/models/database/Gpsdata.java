package httpdata.models.database;

public class Gpsdata {
        private String GPS_ID;
        private String GPS_HIER;
        private String GPS_LAT;
        private String GPS_LON;
        private String GPS_PASSCODE;
        private String GPS_IMEI;
        private String GPS_TIMESTAMP;

    public Gpsdata(String gps_id, String gps_hier, String gps_lat, String gps_lon, String gps_passcode, String gps_imei,String gps_timestamp) {
        this.GPS_ID=gps_id;
        this.GPS_HIER=gps_hier;
        this.GPS_LAT =gps_lat;
        this.GPS_LON=gps_lon;
        this.GPS_PASSCODE=gps_passcode;
        this.GPS_IMEI=gps_imei;
        this.GPS_TIMESTAMP=gps_timestamp;
    }

    public String getGPS_ID() {
        return GPS_ID;
    }

    public void setGPS_ID(String GPS_ID) {
        this.GPS_ID = GPS_ID;
    }

    public String getGps_hier(){
        return GPS_HIER;
    }

    public void setGps_hier(String GPS_HIER){
        this.GPS_HIER=GPS_HIER;
    }

    public String getGps_lat(){
        return GPS_LAT;
    }

    public void setGps_lat(String GPS_LAT){
        this.GPS_LAT=GPS_LAT;
    }

    public String getGps_lon(){
        return GPS_LON;
    }

    public void setGps_lon(String GPS_LON){
        this.GPS_LON=GPS_LON;
    }

    public String getGps_passcode(){
        return GPS_PASSCODE;
    }

    public void setGps_passcode(String GPS_PASSCODE){
        this.GPS_PASSCODE=GPS_PASSCODE;
    }

    public String getGps_imei(){
        return GPS_IMEI;
    }

    public void setGps_imei(String GPS_IMEI){
        this.GPS_IMEI=GPS_IMEI;
    }

    public String getGps_timestamp(){
        return GPS_TIMESTAMP;
    }

    public void setGps_timestamp(String GPS_TIMESTAMP){
        this.GPS_TIMESTAMP=GPS_TIMESTAMP;
    }

}
