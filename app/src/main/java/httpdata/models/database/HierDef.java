package httpdata.models.database;

public class HierDef {
    private String hier_id;
    private String parent_id;
    private String hier_level;
    private String hier_desc;
    private String full_hier;

    public HierDef(String hier_id,String parent_id,String hier_level, String hier_desc, String full_hier) {
    this.hier_id=hier_id;
    this.parent_id=parent_id;
    this.hier_level=hier_level;
    this.hier_desc=hier_desc;
    this.full_hier=full_hier;
    }

    public String getHier_id() {
        return hier_id;
    }

    public void setHier_id(String hier_id) {
        this.hier_id = hier_id;
    }

    public String getHier_level() {
        return hier_level;
    }

    public void setHier_level(String hier_level) {
        this.hier_level = hier_level;
    }

    public String getParent_id(){
        return parent_id;
    }

    public void setParent_id(String parent_id){
        this.parent_id=parent_id;
    }

    public String getHier_desc(){
        return hier_desc;
    }

    public void setHier_desc(String hier_desc){
        this.hier_desc=hier_desc;
    }

    public String getFull_hier(){
        return full_hier;
    }

    public void setFull_hier(String full_hier){
        this.full_hier=full_hier;
    }
}
