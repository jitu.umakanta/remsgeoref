package httpdata.models.database;

public class HierLevels {
    private String hier_level;
    private String parent_hier;
    private String child_hier;
    private String hier_name;

    public HierLevels(String hier_level, String hier_name,String parent_hier,String child_hier) {
    this.hier_level=hier_level;
    this.hier_name=hier_name;
    this.parent_hier=parent_hier;
    this.child_hier=child_hier;
    }

    public void setHier_level(String hier_level) {
        this.hier_level = hier_level;
    }

    public String getHier_level() {
        return hier_level;
    }

    public String getChild_hier() {
        return child_hier;
    }

    public void setChild_hier(String child_hier) {
        this.child_hier = child_hier;
    }

    public String getParent_hier() {
        return parent_hier;
    }

    public void setParent_hier(String parent_hier) {
        this.parent_hier = parent_hier;
    }

    public String getHier_name(){
        return hier_name;
    }

    public void setHier_name(String hier_name){
        this.hier_name=hier_name;
    }
}
