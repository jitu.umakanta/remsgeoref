package httpdata.models.database;

public class HierProps {
    private String hier_id;
    private String full_hier;
    private String propname;
    private String propval;

    public HierProps(String ENTRYID, String MID, String imageph, String imagesig) {

        this.hier_id=hier_id;
        this.full_hier=full_hier;
        this.propname=propname;
        this.propval=propval;

    }

    public String getHier_id() {
        return hier_id;
    }

    public void setHier_id(String hier_id) {
        this.hier_id = hier_id;
    }

    public String getFull_hier(){
        return full_hier;
    }

    public void setFull_hier(String full_hier){
        this.full_hier=full_hier;
    }

    public String getPropname(){
        return propname;
    }

    public void setPropname(String propname){
        this.propname=propname;
    }

    public String getPropval(){
        return propval;
    }

    public void setPropval(String propval){
        this.propval=propval;
    }
}
