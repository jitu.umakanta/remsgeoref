package httpdata.models.database;

public class HierdefOld {
    private int hier_id;
    private int parent_id;
    private int hier_level;
    private String hier_desc;
    private String full_hier;

    public int getHier_id() {
        return hier_id;
    }

    public void setHier_id(int hier_id) {
        this.hier_id = hier_id;
    }

    public int getHier_level() {
        return hier_level;
    }

    public void setHier_level(int hier_level) {
        this.hier_level = hier_level;
    }

    public int getParent_id(){
        return parent_id;
    }

    public void setParent_id(int parent_id){
        this.parent_id=parent_id;
    }

    public String getHier_desc(){
        return hier_desc;
    }

    public void setHier_desc(String hier_desc){
        this.hier_desc=hier_desc;
    }

    public String getFull_hier(){
        return full_hier;
    }

    public void setFull_hier(String full_hier){
        this.full_hier=full_hier;
    }
}
