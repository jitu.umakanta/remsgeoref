package httpdata.models.database;

public class Misc {
    private String prop_name;
    private String prop_val;

    public String getProp_name() {
        return prop_name;
    }

    public void setProp_name(String prop_name) {
        this.prop_name = prop_name;
    }

    public String getProp_val(){
        return prop_val;
    }

    public void setProp_val(String prop_val){
        this.prop_val=prop_val;
    }
}
