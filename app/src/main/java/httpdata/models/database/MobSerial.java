package httpdata.models.database;

public class MobSerial {
    private int num;
    private int id;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id=id;
    }

}
