package httpdata.models.database;

import java.util.Date;

public class Mobile {
    private int login;
    private int mob_id;
    private int mob_no;
    private String imei_no;
    private String full_hier;
    private Date created_on;

    public int getLogin() {
        return login;
    }

    public void setLogin(int login) {
        this.login = login;
    }


    public int getMob_id(){
        return mob_id;
    }

    public void setMob_id(int mob_id){
        this.mob_id=mob_id;
    }

    public int getMob_no(){
        return mob_no;
    }

    public void setMob_no(int mob_no){
        this.mob_no=mob_no;
    }

    public String getImei_no(){
        return imei_no;
    }

    public void setImei_no(String imei_no){
        this.imei_no=imei_no;
    }

    public String getFull_hier(){
        return full_hier;
    }

    public void setFull_hier(String full_hier){
        this.full_hier=full_hier;
    }

    public Date getCreated_on() {
        return created_on;
    }

    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }
}
