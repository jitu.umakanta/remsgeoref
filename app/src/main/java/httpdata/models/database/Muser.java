package httpdata.models.database;

public class Muser {
    private String BYID;
    private String login;
    private String passwd;
    private String name;
    private String pmobile;
    private String photo;
    private String full_hier;
    private String email;
    private String desig;

    public String getBYID() {
        return BYID;
    }

    public void setBYID(String BYID) {
        this.BYID = BYID;
    }

    public String getLogin(){
        return login;
    }

    public void setLogin(String login){
        this.login=login;
    }

    public String getPasswd(){
        return passwd;
    }

    public void setPasswd(String passwd){
        this.passwd=passwd;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name=name;
    }

    public String getPmobile(){
        return pmobile;
    }

    public void setPmobile(String pmobile){
        this.pmobile=pmobile;
    }

    public String getPhoto(){
        return photo;
    }

    public void setPhoto(String photo){
        this.photo=photo;
    }

    public String getFull_hier(){
        return full_hier;
    }

    public void setFull_hier(String full_hier){
        this.full_hier=full_hier;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email=email;
    }

    public String getDesig(){
        return desig;
    }

    public void setDesig(String desig){
        this.desig=desig;
    }
}
