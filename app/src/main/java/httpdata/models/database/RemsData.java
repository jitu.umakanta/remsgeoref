package httpdata.models.database;

public class RemsData {
    private String ENTRYID;
    private String MID;
    private String imageph;
    private String imagesig;
    private String imageself;
    private String AUDIO;
    private String OIMAGEPH;
    private String OIMAGESIG;
    private String OIMAGESELF;
    private String OAUDIO;
    private String GPS;
    private String BYID;
    private String CAPON;
    private String SYNCED;
    private String HIER_ID;
    private String FULL_HIER;
    private String CAPON_PH;
    private String mnotes;
    private String wnotes;
    private String mile;
    private String imei;
    private String sw_version;
    public String api = "";

    public RemsData(String entryid, String mid, String imageph, String imagesig, String imageself, String audio, String oimageph, String oimagesig, String oimageself, String oaudio, String gps, String byid, String capon, String synced, String hier_id, String full_hier, String capon_ph, String mnotes, String wnotes, String mile, String imei, String sw_version) {

    this.ENTRYID=entryid;
    this.MID=mid;
    this.imageph=imageph;
    this.imagesig=imagesig;
    this.imageself=imageself;
    this.AUDIO=audio;
    this.OIMAGEPH=oimageph;
    this.OIMAGESIG=oimagesig;
    this.OIMAGESELF=oimageself;
    this.OAUDIO=oaudio;
    this.GPS=gps;
    this.BYID=byid;
    this.CAPON=capon;
    this.SYNCED=synced;
    this.HIER_ID=hier_id;
    this.FULL_HIER=full_hier;
    this.CAPON_PH=capon_ph;
    this.mnotes=mnotes;
    this.wnotes=wnotes;
    this.mile=mile;
    this.imei=imei;
    this.sw_version=sw_version;

    }

    public String getENTRYID() {
        return ENTRYID;
    }

    public void setENTRYID(String ENTRYID) {
        this.ENTRYID = ENTRYID;
    }

    public String getCAPON() {
        return CAPON;
    }

    public void setCAPON(String CAPON) {
        this.CAPON = CAPON;
    }

    public String getSYNCED() {
        return SYNCED;
    }

    public void setSYNCED(String SYNCED) {
        this.SYNCED = SYNCED;
    }

    public String getCAPON_PH() {
        return CAPON_PH;
    }

    public void setCAPON_PH(String CAPON_PH) {
        this.CAPON_PH = CAPON_PH;
    }

    public String getWnotes() {
        return wnotes;
    }

    public void setWnotes(String wnotes) {
        this.wnotes = wnotes;
    }

    public String getMile() {
        return mile;
    }

    public void setMile(String mile) {
        this.mile = mile;
    }

    public String getMid(){
        return MID;
    }

    public void setMid(String MID){
        this.MID=MID;
    }

    public String getImageph(){
        return imageph;
    }

    public void setImageph(String imageph){
        this.imageph=imageph;
    }

    public String getImagesig(){
        return imagesig;
    }

    public void setImagesig(String imagesig){
        this.imagesig=imagesig;
    }

    public String getImageself(){
        return imageself;
    }

    public void setImageself(String imageself){
        this.imageself=imageself;
    }

    public String getAudio(){
        return AUDIO;
    }

    public void setAudio(String AUDIO){
        this.AUDIO=AUDIO;
    }

    public String getOimageph(){
        return OIMAGEPH;
    }

    public void setOimageph(String OIMAGEPH){
        this.OIMAGEPH=OIMAGEPH;
    }

    public String getOimagesig(){
        return OIMAGESIG;
    }

    public void setOimagesig(String OIMAGESIG){
        this.OIMAGESIG=OIMAGESIG;
    }

    public String getOimageself(){
        return OIMAGESELF;
    }

    public void setOimageself(String OIMAGESELF){
        this.OIMAGESELF=OIMAGESELF;
    }

    public String getOaudio(){
        return OAUDIO;
    }

    public void setOaudio(String OAUDIO){
        this.OAUDIO=OAUDIO;
    }

    public String getGps(){
        return GPS;
    }

    public void setGps(String GPS){
        this.GPS=GPS;
    }

    public String getByid(){
        return BYID;
    }

    public void setByid(String BYID){
        this.BYID=BYID;
    }

    public String getHier_id(){
        return HIER_ID;
    }

    public void setHier_id(String HIER_ID){
        this.HIER_ID=HIER_ID;
    }

    public String getFull_hier(){
        return FULL_HIER;
    }

    public void setFull_hier(String FULL_HIER){
        this.FULL_HIER=FULL_HIER;
    }

    public String getMnotes(){
        return mnotes;
    }

    public void setMnotes(String mnotes){
        this.mnotes=mnotes;
    }

    public String getImei(){
        return imei;
    }

    public void setImei(String imei){
        this.imei=imei;
    }

    public String getSw_version(){
        return sw_version;
    }

    public void setSw_version(String sw_version){
        this.sw_version=sw_version;
    }

}
