package httpdata.models.database;

public class Report01 {
       private  String piu;
        private String main_proj;
        private int p_image;
        private int t_image;
        private String piu_name;
        private String p_name;
        private java.util.Date r_dt;

    public String getPiu() {
        return piu;
    }

    public void setPiu(String piu) {
        this.piu = piu;
    }

    public String getMain_proj(){
        return main_proj;
    }

    public void setMain_proj(String main_proj){
        this.main_proj=main_proj;
    }

    public int getP_image(){
        return p_image;
    }

    public void setP_image(int p_image){
        this.p_image=p_image;
    }

    public int getT_image(){
        return t_image;
    }

    public void setT_image(int t_image){
        this.t_image=t_image;
    }

    public String getPiu_name(){
        return piu_name;
    }

    public void setPiu_name(String piu_name){
        this.piu_name=piu_name;
    }

    public String getP_name(){
        return p_name;
    }

    public void setP_name(String p_name){
        this.p_name=p_name;
    }

    public java.util.Date getR_dt(){
        return r_dt;
    }

    public void setR_dt(java.util.Date r_dt){
        this.r_dt=r_dt;
    }
}
