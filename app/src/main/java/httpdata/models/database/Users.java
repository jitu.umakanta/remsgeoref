package httpdata.models.database;

public class Users {


    private String login;
    private String passwd;
    private String priv;
    private String name;
    private String photo;
    private String desig;
    private String full_hier;
    private String phone;
    private String email;
    private String BYID;
    private String isadmin;
    private String isactive;


    public String getLogin(){
        return login;
    }

    public void setLogin(String login){
        this.login=login;
    }

    public String getPasswd(){
        return passwd;
    }

    public void setPasswd(String passwd){
        this.passwd=passwd;
    }
    public String getPriv(){
        return priv;
    }

    public void setPriv(String priv){
        this.priv=priv;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name=name;
    }

    public String getPhoto(){
        return photo;
    }

    public void setPhoto(String photo){
        this.photo=photo;
    }

    public String getDesig(){
        return desig;
    }

    public void setDesig(String desig){
        this.desig=desig;
    }

    public String getFull_hier(){
        return full_hier;
    }

    public void setFull_hier(String full_hier){
        this.full_hier=full_hier;
    }

    public String getPhone(){
        return phone;
    }

    public void setPhone(String phone){
        this.phone=phone;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email=email;
    }

    public String getByid(){
        return BYID;
    }

    public void setByid(String BYID){
        this.BYID=BYID;
    }

    public String getIsadmin(){
        return isadmin;
    }

    public void setIsadmin(String isadmin){
        this.isadmin=isadmin;
    }

    public String getIsactive(){
        return isactive;
    }

    public void setIsactive(String isactive){
        this.isactive=isactive;
    }
}
