package httpdata.models.database;

public class Wcom {
    private String ENTRYID;
    private String CMTBY;
    private String CMTON;

    public String getENTRYID(){
        return ENTRYID;
    }

    public void setENTRYID(String ENTRYID){
        this.ENTRYID=ENTRYID;
    }

    public String getCMTBY() {
        return CMTBY;
    }

    public void setCMTBY(String CMTBY) {
        this.CMTBY = CMTBY;
    }

    public String getCMTON() {
        return CMTON;
    }

    public void setCMTON(String CMTON) {
        this.CMTON = CMTON;
    }
}
