package httpdata.models.direct;


import httpdata.models.database.HierDef;

public class LocationDetails extends HierDef {

    public String name;
    public String address;
    public String GPS_LAT;
    public String GPS_LON;



    public LocationDetails(String hier_id, String parent_id, String hier_level, String hier_desc, String full_hier) {
        super(hier_id, parent_id, hier_level, hier_desc, full_hier);
    }
}
