package httpdata.projectspecificmodels;

/**
 * Created by rahul on 2/11/17.
 */

public class AnswerReply {

    public int user_def_id;
    public int question_def_id;
    public int uplr_id;
    public String answer_text;
    public int answer_number;
    public String answer_filepath;

}
