package httpdata.projectspecificmodels;

import java.util.List;

/**
 * Created by sumeendranath on 05/10/17.
 */

public class CrudModel <T> {

    public int api_status;
    public String api_message;
    public String api_authorization;
    public List<T> data;
}
