package httpdata.projectspecificmodels;

/**
 * Created by rahul on 26/10/17.
 */

public class GetProjectLocation {

    public int user_def_id;
    public int roles_def_id;
    public String roles_name;
    public String project_name;
    public int project_id;
    public String location_name;
    public int location_id;
    public String status_name;

    public GetProjectLocation(int user_def_id, int roles_def_id, int project_id, int location_id, String roles_name, String project_name, String location_name, String status_name){

        this.user_def_id=user_def_id;
        this.roles_def_id=roles_def_id;
        this.project_id=project_id;
        this.location_id=location_id;
        this.roles_name=roles_name;
        this.project_name=project_name;
        this.location_name=location_name;
        this.status_name=status_name;

    }

    public int getUser_def_id() {
        return user_def_id;
    }

    public void setUser_def_id(int user_def_id) {
        this.user_def_id = user_def_id;
    }

    public int getRoles_def_id() {
        return roles_def_id;
    }

    public void setRoles_def_id(int roles_def_id) {
        this.roles_def_id = roles_def_id;
    }

    public String getRoles_name() {
        return roles_name;
    }

    public void setRoles_name(String roles_name) {
        this.roles_name = roles_name;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public int getProject_id() {
        return project_id;
    }

    public void setProject_id(int project_id) {
        this.project_id = project_id;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public String getStatus_name() {
        return status_name;
    }

    public void setStatus_name(String status_name) {
        this.status_name = status_name;
    }
}
