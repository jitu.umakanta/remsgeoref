package httpdata.projectspecificmodels;

/**
 * Created by sumeendranath on 17/10/17.
 */

public class GpsData {

    //"id":2,"pp_id":13,"gps_lat":"28.5519399","gps_lon":"77.1209264","gps_passcode":"456789","gps_imei":null,"gps_timestamp":"2017-10-17 09:31:22","valid":1
    public int id;
    public int pp_id;
    public String gps_lat;
    public String gps_lon;
    public String gps_imei;
    public String created_at;
    public int valid;
}
