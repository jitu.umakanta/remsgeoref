package httpdata.projectspecificmodels;

import org.parceler.Parcel;

@Parcel
public class OutpatientRecord {


        public int id;
        public int patient;
        public int consultant;
        public int hospital;
        public int ward;
        public int IP;
        public int occupation;
        public int services;
        public int speciality;
        public String date;
        public double amount;
        public String reciept_no;
        public String imagepath;
        public int  status;
        public String timeofupdate;
        public int valid;
        public String patientname;
        public int age;
        public int gender;
        public String regdate;
        public String address;
        public int marital;
        public String father_husband;
        public String consultantname;
        public String hospitalname;
        public String wardname;
        public int IPnumber;
        public String occupationname;
        public  String servicesname;
        public  String specialityname;
        public String statusname;

    public OutpatientRecord() {}




        public OutpatientRecord(int id, int patient, int consultant, int hospital, int ward, int IP, int occupation, int services, int speciality, String date, double amount, String reciept_no, String imagepath, int  status, String timeofupdate, int valid, String patientname,Integer age,Integer gender,String regdate,String address,Integer marital,String father_husband,String consultantname,String hospitalname,String wardname,Integer IPnumber,String occupationname,String servicesname,String specialityname,String statusname)

        {
            this.id=id;
            this.patient=patient;
            this.consultant=consultant;
            this.hospital=hospital;
            this.ward=ward;
            this.IP=IP;
            this.occupation=occupation;
            this.services=services;
            this.speciality=speciality;
            this.date=date;
            this.amount=amount;
            this.reciept_no=reciept_no;
            this.imagepath=imagepath;
            this.status=status;
            this.timeofupdate=timeofupdate;
            this.valid=valid;
            this.patientname=patientname;
            this.age=age;
            this.regdate=regdate;
            this.address=address;
            this.marital=marital;
            this.father_husband=father_husband;
            this.consultantname=consultantname;
            this.hospitalname=hospitalname;
            this.wardname=wardname;
            this.IPnumber=IPnumber;
            this.occupationname=occupationname;
            this.specialityname=specialityname;
            this.servicesname=servicesname;
            this.statusname=statusname;

        }

        public int getPatient(){
            return patient;
        }

        public void setPatient(int patient){
            this.patient=patient;
        }

        public int getConsultant(){
            return consultant;
        }

        public void setConsultant(int consultant){
            this.consultant=consultant;
        }

        public int getHospital(){
            return hospital;
        }

        public void setHospital(int hospital){
            this.hospital=hospital;
        }

        public int getWard(){
            return ward;
        }

        public void setWard(int ward){
            this.ward=ward;
        }

        public int getIp(){
            return IP;
        }

        public void setIp(int IP){
            this.IP=IP;
        }

        public int getOccupation(){
            return occupation;
        }

        public void setOccupation(int occupation){
            this.occupation=occupation;
        }

        public int getServices(){
            return services;
        }

        public void setServices(int services){
            this.services=services;
        }

        public int getSpeciality(){
            return speciality;
        }

        public void setSpeciality(int speciality){
            this.speciality=speciality;
        }


        public double getAmount(){
            return amount;
        }

        public void setAmount(double amount){
            this.amount=amount;
        }

        public String getReciept_no(){
            return reciept_no;
        }

        public void setReciept_no(String reciept_no){
            this.reciept_no=reciept_no;
        }

        public String getImagepath(){
            return imagepath;
        }

        public void setImagepath(String imagepath){
            this.imagepath=imagepath;
        }


}
