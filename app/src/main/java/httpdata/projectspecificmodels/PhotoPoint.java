package httpdata.projectspecificmodels;

/**
 * Created by sumeendranath on 17/10/17.
 */

public class PhotoPoint {

    public int id;
    public String kilometer;
    public String name;
    public String department_id;
    public int valid;
    public String departmentname;
    public String locationname;
    public int type;
    public String typename;
    public String distance;
}
