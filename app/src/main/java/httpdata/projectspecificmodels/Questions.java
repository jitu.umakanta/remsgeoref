package httpdata.projectspecificmodels;

/**
 * Created by rahul on 31/10/17.
 */

public class Questions {

    public int id;
    public String text;
    public int type;
    public String value;
    public int group;
    public int question_order;
    public int manadatory;

    public Questions(int id, String text, int type, String value, int group, int question_order, int manadatory) {
        this.id = id;
        this.text = text;
        this.type = type;
        this.value = value;
        this.group = group;
        this.question_order = question_order;
        this.manadatory = manadatory;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int getQuestion_order() {
        return question_order;
    }

    public void setQuestion_order(int question_order) {
        this.question_order = question_order;
    }

    public int getManadatory() {
        return manadatory;
    }

    public void setManadatory(int manadatory) {
        this.manadatory = manadatory;
    }
}
