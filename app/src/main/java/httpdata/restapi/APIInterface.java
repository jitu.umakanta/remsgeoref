package httpdata.restapi;


import httpdata.projectspecificmodels.AnswerReply;
import httpdata.projectspecificmodels.BreweryUsersLogin;
import httpdata.projectspecificmodels.CheckUserTask;
import httpdata.projectspecificmodels.Consultant;
import httpdata.projectspecificmodels.CrudModel;
import httpdata.projectspecificmodels.GetProjectLocation;
import httpdata.projectspecificmodels.GpsData;
import httpdata.projectspecificmodels.Hospital;
import httpdata.projectspecificmodels.LocationProject;
import httpdata.projectspecificmodels.MobUsers;
import httpdata.projectspecificmodels.OutpatientRecord;
import httpdata.projectspecificmodels.PhotoPoint;
import httpdata.projectspecificmodels.QuestionGroup;
import httpdata.projectspecificmodels.Questions;
import httpdata.projectspecificmodels.UserProject;
import httpdata.projectspecificmodels.UserRole;
import httpdata.projectspecificmodels.UserRoleChecklist;
import httpdata.projectspecificmodels.UserRoleTasklist;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by anupamchugh on 09/01/17.
 */

public interface APIInterface {

    //download zip files at a time
    @GET("/api2/mob")
    Call<ResponseBody> downloadFileWithFixedUrl();

    // 02:06 PM " 24-11-2017  " by jitu '
    @GET("/api2/mob")
    Call<ResponseBody> getAllMobileData();

    @GET("/api/selectuserdef")
    Call<CrudModel<BreweryUsersLogin>> getlistofprojectusers();

    @GET("/api/getuserprojectlocation")
    Call<CrudModel<LocationProject>> getlocationproject(@Query("user_def_id") int user_def_id);

    @GET("/api/getuserproject")
    Call<CrudModel<UserProject>> getuserproject(@Query("user_def_id") int user_def_id, @Query("project_location_def_id") int project_location_def_id);

    @GET("/api/getuserroles")
    Call<CrudModel<UserRole>> getuserroles(@Query("user_def_id") int user_def_id, @Query("project_location_def_id") int project_location_def_id);

    @GET("/api/getchecklist")
    Call<CrudModel<UserRoleChecklist>> getuserchecklist(@Query("roles_def_id") int roles_def_id);

    @GET("/api/getusertask")
    Call<CrudModel<UserRoleTasklist>> getusertask();

    @GET("/api/getquestiongroup")
    Call<CrudModel<QuestionGroup>> getquestiongroup(@Query("checklist_def_id") int checklist_def_id);

    @GET("/api/getquestion")
    Call<CrudModel<Questions>> getquestion(@Query("group") int group);

    @POST("/api/adduseranswer")
    @FormUrlEncoded
    Call<ResponseBody> setuseranswer(@Field("user_def_id") int user_def_id, @Field("checklist_def_id") int checklist_def_id, @Field("question_def_id") int question_def_id, @Field("uplr_id") int uplr_id, @Field("answer_text") String answer_text, @Field("answer_number") String answer_number, @Field("answer_filepath") String answer_filepath, @Field("formcount") String formcount);

    @POST("/api/adduseranswerimage")
    @Multipart
    Call<ResponseBody> setuseranswerimage(@Part("user_def_id") RequestBody user_def_id, @Part("checklist_def_id") RequestBody checklist_def_id, @Part("question_def_id") RequestBody question_def_id, @Part("uplr_id") RequestBody uplr_id, @Part("answer_text") RequestBody answer_text, @Part("answer_number") RequestBody answer_number, @Part MultipartBody.Part answer_filepath, @Part("formcount") RequestBody formcount);


    @POST("/api/addfirebaseid")
    @FormUrlEncoded
    Call<ResponseBody> setuserfirebaseid(@Field("user_def_id") String user_def_id, @Field("firebase_def_id") String firebase_id);


    @Multipart
    @POST("/api/adduseranswerimage")
    Call<ResponseBody> adduseranswerimage(@Part("user_def_id") RequestBody user_def_id, @Part("question_def_id") RequestBody question_def_id, @Part("uplr_id") RequestBody uplr_id, @Part("answer_text") RequestBody answer_text, @Part("answer_number") RequestBody answer_number, @Part MultipartBody.Part filePart);

//Added by Rahul End

    @GET("/api/getmobusers")
    Call<CrudModel<MobUsers>> getlistofmobusers();

    @GET("/api/getphotopointlist")
    Call<CrudModel<PhotoPoint>> getlistofphotopoints();

    @GET("/api/getgpsdata")
    Call<CrudModel<GpsData>> getlistofgpsdata();

    @POST("/api/updategps")
    @FormUrlEncoded
    Call<CrudModel<ResponseBody>> updategps(@Field("gps_lat") String gpslat, @Field("gps_lon") String gpslon, @Field("gps_passcode") String gpspasscode, @Field("pp_id") int photopointid);


    @Multipart
    @POST("/api/uploadphoto")
    Call<ResponseBody> uploadphoto(@Part("photopoint_id") RequestBody photopointid, @Part("user_id") RequestBody user_id,
                                   @Part MultipartBody.Part filePart,
                                   @Part("gps_lat") RequestBody gps_lat,
                                   @Part("gps_lon") RequestBody gps_lon
    );


    //// project specific
    @GET("/api/selecthospital")
    Call<CrudModel<Hospital>> getlistofhospitals();


    @GET("/api/selectconsultant")
    Call<CrudModel<Consultant>> getlistofconsultants();

    @GET("/api/getwaitingpatients")
    Call<CrudModel<OutpatientRecord>> getWaitingPatients();


    @Multipart
    @POST("/api/updateopd")
    Call<ResponseBody> updateOpdStatus(@Part("id") RequestBody id, @Part("status") RequestBody status, @Part MultipartBody.Part filePart);


    @Multipart
    @POST("/api/uploadopdreport")
    Call<ResponseBody> uploadOpdReport(@Part("id") RequestBody id, @Part("status") RequestBody status, @Part MultipartBody.Part filePart);


    /////

    @GET("/available")
    Call<ResponseBody> serverIsAvailable();

    @GET("/d/allmobilezipped")
    Call<ResponseBody> saveGpsLocationDetailsIntoFile();


    @Multipart
    @POST("/b/upload")
    Call<ResponseBody> uploadAttachment(@Part MultipartBody.Part filePart);

}

