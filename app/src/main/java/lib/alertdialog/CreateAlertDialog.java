package lib.alertdialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by rahul on 6/11/17.
 */

public class CreateAlertDialog {

    private void createAndShowAlertDialog(Context ctx,String title,String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle("My Title");
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //TODO
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //TODO
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
