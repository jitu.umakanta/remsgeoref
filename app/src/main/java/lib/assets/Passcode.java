package lib.assets;

import android.content.Context;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import configuration.AndroidDefaults;

/**
 * Created by sumeendranath on 30/08/17.
 */

public class Passcode {


    public static boolean checkIfPasscodeIsCorrect(Context context,String passcode)
    {
        if(readPasscodeFile(context).contains(passcode))
        {
            return true;
        }

        return false;
    }


    public static List<String> readPasscodeFile(Context context) {
        List<String> returnList = new ArrayList<>();
        try {
            InputStream is = context.getAssets().open(AndroidDefaults.PASSCODEFILENAME);
            BufferedReader BR = new BufferedReader(new InputStreamReader(is));

            String item = "";
            while((item = BR.readLine()) != null)
            {
                returnList.add(item);
            }
            is.close();
            BR.close();

        } catch (Exception ex2) {
            ex2.printStackTrace();
        }
        return returnList;
    }

}
