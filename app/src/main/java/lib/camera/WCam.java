package lib.camera;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.otaliastudios.cameraview.CameraView;

/**
 * Created by sumeendranath on 02/09/17.
 */

public class WCam extends CameraView{
    public WCam(@NonNull Context context) {
        super(context);
    }

    public WCam(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }
}
