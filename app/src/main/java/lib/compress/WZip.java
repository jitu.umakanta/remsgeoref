package lib.compress;

import android.content.Context;

import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZFile;
import org.apache.commons.compress.archivers.sevenz.SevenZOutputFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lib.utils.common.Wlog;
import lib.utils.file.FileUtils;

/**
 * Created by sumeendranath on 28/08/17.
 */

public class WZip {


    public static void compress(List<String> inputfiles, String outputfilepath) {

        SevenZOutputFile sevenZOutput = null;

        File outputfile = new File(outputfilepath);


        try {
            sevenZOutput = new SevenZOutputFile(outputfile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < inputfiles.size(); i++) {
            File filtoarchive = new File(inputfiles.get(i));
            SevenZArchiveEntry entry = null;
            try {
                //BufferedInputStream instream = new BufferedInputStream(new FileInputStream(inputfiles.get(i)));


                Wlog.i("writing : " + filtoarchive.getName());
                entry = sevenZOutput.createArchiveEntry(filtoarchive, filtoarchive.getName());

                try {
                    sevenZOutput.putArchiveEntry(entry);
                } catch (IOException e) {
                    e.printStackTrace();
                }

               // copy(filtoarchive,sevenZOutput);


                sevenZOutput.write(FileUtils.fullyReadFileToBytes(filtoarchive));


                try {
                    sevenZOutput.closeArchiveEntry();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } catch (IOException e) {
                e.printStackTrace();
            }


        }

        try {
            sevenZOutput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    private static void copy (final File src, final SevenZOutputFile dst) throws IOException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(src);
            final byte[] buffer = new byte[8*1024];
            int bytesRead;
            while ((bytesRead = fis.read(buffer)) >= 0) {
                dst.write(buffer, 0, bytesRead);
            }
        } finally {
            if (fis != null) {
                fis.close();
            }
        }
    }



    public static List<String> decompressIntoInternalMem(Context context, String zippedfile) {

        List<String> listoffilenamesdecom = new ArrayList<>();

        SevenZFile sevenZFile = null;
        try {
            sevenZFile = new SevenZFile(new File(zippedfile));
        } catch (IOException e) {
            e.printStackTrace();
        }


        SevenZArchiveEntry entry = null;

        try {
            entry = sevenZFile.getNextEntry();
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (entry!=null) {
            try {
                byte[] content = new byte[(int) entry.getSize()];
                sevenZFile.read(content, 0, content.length);
                FileUtils.saveToInternalFile(context, entry.getName(),content);

                Wlog.d("uncompressing : " + entry.getName());
                listoffilenamesdecom.add(entry.getName());
                entry = sevenZFile.getNextEntry();


            } catch (IOException e) {
                e.printStackTrace();
            }
        }




        try {
            sevenZFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return listoffilenamesdecom;
    }
}