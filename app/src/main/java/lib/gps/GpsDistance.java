package lib.gps;

import android.location.Location;

import lib.vars.Values;

/**
 * Created by sumeendranath on 27/08/17.
 */

public class GpsDistance {


    public static enum ACCURACY {

        COARSE,
        FINE
    };


    public static String GPS_SEP =",";




    public static double get(String gps1, String gps2)
    {
        return get(gps1,gps2, ACCURACY.COARSE);
    }


    public static double get(Location loc1, Location loc2, ACCURACY accuracy)
    {
        return get(loc1.getLatitude(),loc1.getLongitude(),loc2.getLatitude(),loc2.getLongitude(),accuracy);
    }


    public static double get(Location loc1, Location loc2)
    {
        return get(loc1.getLatitude(),loc1.getLongitude(),loc2.getLatitude(),loc2.getLongitude());
    }


    public static double get(String gps1, String gps2, ACCURACY accuracy)
    {
        if(!gps1.contains(GPS_SEP) || !gps2.contains(GPS_SEP))
        {
            return Values.INVALID_DOUBLE;
        }

        Location loc1 = GpsUtils.stringToGpsLocation(gps1);
        Location loc2 = GpsUtils.stringToGpsLocation(gps2);

        Double lat1 = loc1.getLatitude();
        Double lon1 = loc1.getLongitude();

        Double lat2 = loc2.getLatitude();
        Double lon2 = loc2.getLongitude();

        if(lat1 == Values.INVALID_DOUBLE
                || lon1 == Values.INVALID_DOUBLE
                || lat2 == Values.INVALID_DOUBLE
                || lon2 == Values.INVALID_DOUBLE)
        {
            return Values.INVALID_DOUBLE;
        }

        return get(lat1,lon1,lat2,lon2,accuracy);

    }



    public static double get(double lat1, double lng1,
                             double lat2, double lng2)
    {
           return get( lat1,  lng1, lat2,  lng2, ACCURACY.COARSE);
    }


    public static double get(double lat1, double lng1,
                             double lat2, double lng2, ACCURACY accuracy)
    {
            if(accuracy == ACCURACY.COARSE)
            {
                return distance_short(lat1,lng1,lat2,lng2);
            }
            else
            {
                return distance_long(lat1,lng1,0,lat2,lng2,0); /// height is assumed as 0 for both
            }
    }


    public static double distance_short(double lat1, double lng1,
                                  double lat2, double lng2){
        double a = (lat1-lat2)*distPerLat(lat1);
        double b = (lng1-lng2)*distPerLng(lat1);
        return Math.sqrt(a*a+b*b);
    }

    private static double distPerLng(double lat){
        return 0.0003121092*Math.pow(lat, 4)
                +0.0101182384*Math.pow(lat, 3)
                -17.2385140059*lat*lat
                +5.5485277537*lat+111301.967182595;
    }

    private static double distPerLat(double lat){
        return -0.000000487305676*Math.pow(lat, 4)
                -0.0033668574*Math.pow(lat, 3)
                +0.4601181791*lat*lat
                -1.4558127346*lat+110579.25662316;
    }



    public static double distance_long(double lat1, double lon1,double el1,double lat2,
                                  double lon2, double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }

}
