package lib.gps;

import android.content.Context;
import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import configuration.AndroidDefaults;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import lib.utils.common.Wlog;

/**
 * Created by sumeendranath on 27/08/17.
 */
public class GpsLocation {

    public static String LATESTGPS_LAT = "";
    public static String LATESTGPS_LON = "";


    Location _locationlatest;

    Context _maincontext;

    Location _previouslocation;

    Double _locationchangedistancelimit = AndroidDefaults.GPS_MINDISTANCE_LOCUPDATE;


    GpsDistance.ACCURACY _accuracy = AndroidDefaults.GPS_ACCURACY;

    public GpsLocation(Context context, Double distancelimitforchange, GpsDistance.ACCURACY accuracy) {

        _maincontext = context;
        _locationchangedistancelimit = distancelimitforchange;
        _accuracy = accuracy;
        start();
    }


    public GpsLocation(Context context) {

        _maincontext = context;
        start();
    }



    private void start()
    {
        _previouslocation = GpsUtils.getNewGpsLocInstance();
        _locationlatest = GpsUtils.getNewGpsLocInstance();

        SmartLocation.with(_maincontext).location()
                .start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        _previouslocation = _locationlatest;
                        _locationlatest = location;

                        Double distance = GpsDistance.get(_previouslocation,_locationlatest, _accuracy);

                        String gpsstring = GpsUtils.locationToGpsString(location);
                        onLocationChange(_locationlatest, distance, gpsstring);

                        if(distance > _locationchangedistancelimit)
                        {
                            Wlog.d("for: " + gpsstring);
                            LocChangeBeyondLimit(location, gpsstring);
                        }

                    }
                });
    }


    public Location getLatestLocation()
    {
        return _locationlatest;
    }

    public LatLng getLatestLatLon()
    {
        return GpsUtils.locationToLatLng(_locationlatest);
    }


    public void reset()
    {
        try
        {
            SmartLocation.with(_maincontext).location().stop();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        start();
    }


    public void stop()
    {
        try
        {
            SmartLocation.with(_maincontext).location().stop();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }




    protected void onLocationChange(Location loc, Double distancefromprevloc, String gpsstring)
    {

    }

    protected void LocChangeBeyondLimit(Location loc, String gpsstring)
    {

    }

}
