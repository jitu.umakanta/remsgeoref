package lib.gps;

import android.content.Context;
import android.location.Location;

import java.util.Map;

import configuration.AndroidDefaults;

/**
 * Created by sumeendranath on 27/08/17.
 */
public class GpsMaster<T> extends GpsLocation {

    Map<Double,T> _modelhashforfilterReturn;
    Map<String,T> _modelhashforfilter;

    public GpsMaster(Context context, Map<String,T> modalhashforfilter, Double distancelimitforchange, GpsDistance.ACCURACY accuracy) {
        super(context, distancelimitforchange, accuracy);
        _modelhashforfilter = modalhashforfilter;
    }

    public GpsMaster(Context context, Map<String,T> modalhashforfilter) {
        super(context);

        _modelhashforfilter = modalhashforfilter;
    }



    @Override
    protected void onLocationChange(Location loc, Double distancefromprevloc, String gpsstring) {
        super.onLocationChange(loc, distancefromprevloc, gpsstring);
    }

    @Override
    protected void LocChangeBeyondLimit(final Location loc, final String gpsstring) {
        super.LocChangeBeyondLimit(loc, gpsstring);

        /*_modelhashforfilterReturn = new GpsNearest<T>().get(gpsstring,_modelhashforfilter, AndroidDefaults.GPS_MINDISTANCE_FILTERPOINTS,AndroidDefaults.GPS_ACCURACY)
        {

        };

        onMasterLocationChange(loc,gpsstring,_modelhashforfilterReturn);*/
        new GpsNearest<T>()     {

            @Override
            protected void onProgressStart_GN() {
                super.onProgressStart_GN();
                onProgressStart_GM();
            }

            @Override
            protected void onProgress_GN(Double progress) {
                super.onProgress_GN(progress);
                onProgress_GM(progress);
            }

            @Override
            protected void onComplete_GN(Map<Double, T> results) {
                super.onComplete_GN(results);

                onComplete_GM();

                _modelhashforfilterReturn = results;
                onMasterLocationChange(loc,gpsstring,_modelhashforfilterReturn);
            }

        }.get(gpsstring,_modelhashforfilter, AndroidDefaults.GPS_MINDISTANCE_FILTERPOINTS,AndroidDefaults.GPS_ACCURACY);



    }

    protected void onMasterLocationChange(Location loc, String gpsstring, Map<Double,T> modelhashforfilterReturn) {

    }



    protected void onProgressStart_GM()
    {

    }

    protected void onProgress_GM(Double progress)
    {

    }

    protected void onComplete_GM()
    {

    }


}
