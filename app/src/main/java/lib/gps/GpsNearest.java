package lib.gps;

import android.os.AsyncTask;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.SupposeBackground;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import lib.utils.datautils.WCalc;
import lib.vars.Values;

/**
 * Created by sumeendranath on 27/08/17.
 */
@EBean
public class GpsNearest<T> {

    /// ASSUMING First element of json object is gps latlon
    @SupposeBackground
    public void  get(final String gpslocation,final  Map<String,T> listofgpsmapwithdetails,final  double distancelimitinmeters,final  GpsDistance.ACCURACY accuracy)
    {
        onProgressStart_GN();
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //TODO your background code
                Map<Double,T> results = getOnMainThread(gpslocation,listofgpsmapwithdetails,distancelimitinmeters,accuracy);
                onComplete_GN(results);

            }
        });



    }


    public Map<Double,T> getOnMainThread(String gpslocation, Map<String,T> listofgpsmapwithdetails, double distancelimitinmeters, GpsDistance.ACCURACY accuracy)
    {
        long maxprogress = listofgpsmapwithdetails.size();
        long progresstillnow = 0l;

        Map<Double, T> distancehash = new HashMap<>();

        for (Map.Entry<String,T> locationentry : listofgpsmapwithdetails.entrySet())
        {

           Double distance =  GpsDistance.get(gpslocation,locationentry.getKey());

            progresstillnow++;
            onProgress_GN(WCalc.getScaledProgress(progresstillnow,maxprogress));

           if(distance == Values.INVALID_DOUBLE)
           {
               continue;
           }

           if(distance > distancelimitinmeters)
           {
               continue;
           }

            distancehash.put(distance,locationentry.getValue());
        }


        /// now sort the values in ascending order of distance
        Map<Double, T> sortedhash = new TreeMap<>(distancehash);

        return sortedhash;
    }

    protected void onProgressStart_GN()
    {

    }

    protected void onProgress_GN(Double progress)
    {

    }

    protected void onComplete_GN(Map<Double,T> results)
    {

    }

}
