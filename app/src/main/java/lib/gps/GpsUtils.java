package lib.gps;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import configuration.AndroidDefaults;
import lib.utils.datastructures.Wto;
import lib.vars.Values;

/**
 * Created by sumeendranath on 27/08/17.
 */

public class GpsUtils {

    public static void resetGpsLocation(Location loc)
    {
        loc.setLatitude(Values.INVALID_GPS_DOUBLE);
        loc.setLongitude(Values.INVALID_GPS_DOUBLE);
    }

    public static String locationToGpsString(Location loc)
    {
        return loc.getLatitude() + GpsDistance.GPS_SEP + loc.getLongitude();
    }

    public static Location latLngToLocation(LatLng latlng)
    {
        return getNewGpsLocInstance(latlng.latitude,latlng.longitude);
    }

    public static LatLng locationToLatLng(Location loc)
    {
        return  new LatLng(loc.getLatitude(),loc.getLongitude());
    }

    public static Location stringToGpsLocation(String gpsstring)
    {
        String [] gpstok1 = gpsstring.split(GpsDistance.GPS_SEP);

        if(gpstok1.length<2)
        {
            return getNewGpsLocInstance();
        }

        return getNewGpsLocInstance(Wto.Double(gpstok1[0]), Wto.Double(gpstok1[1]));
    }

    public static Location getNewGpsLocInstance(Double lat, Double lon)
    {
        Location loc = new Location(AndroidDefaults.GPS_LOCATIONPROVDER);

        loc.setLatitude(lat);
        loc.setLongitude(lon);
        return loc;
    }


    public static Location getNewGpsLocInstance()
    {
        Location loc = new Location(AndroidDefaults.GPS_LOCATIONPROVDER);

        resetGpsLocation(loc);

        return loc;
    }
}
