package lib.map;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import configuration.AndroidDefaults;

/**
 * Created by sumeendranath on 31/08/17.
 */

public class MapRoute {



    public static void launchMapsWithApi(Context context, String api)
    {
        //https://www.google.com/maps/dir/?api=1&destination=12.954143, 77.656189
        Intent intent = new Intent(Intent.ACTION_VIEW,

                Uri.parse( api)
                );
        context.startActivity(intent);

    }

    public static void driveTo(Context context, String destinationgps)
    {
        launchMapsWithApi(context,AndroidDefaults.GOOGLE_ROUTE_API + "&destination=" + destinationgps);

    }

    public static void driveTo(Context context,String sourcegps, String destinationgps)
    {
        launchMapsWithApi(context,AndroidDefaults.GOOGLE_ROUTE_API + "&origin=" + sourcegps + "&destination=" + destinationgps);
    }

}
