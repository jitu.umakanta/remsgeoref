package lib.map;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import configuration.AndroidDefaults;
import lib.gps.GpsDistance;
import lib.gps.GpsUtils;

/**
 * Created by sumeendranath on 27/08/17.
 */

public class WMap {


    GoogleMap _googlemap;
    CameraPosition mPreviousCameraPosition;
    int _zoom = AndroidDefaults.GPS_DEFAULT_ZOOM;
    Double _selectabledistance = AndroidDefaults.GPS_SELECTABLEDISTANCE;

    Marker _mycurrentmarker;

    public WMap(GoogleMap googlemap) {

        _googlemap = googlemap;
        init();
    }

    public void set_selectabledistance(Double distanceinmeters)
    {
        _selectabledistance = distanceinmeters;
    }
    public Double get_selectabledistance()
    {
        return _selectabledistance ;
    }

    public void set_zoom(int zoom)
    {
        _zoom = zoom;
    }
    public int get_zoom()
    {
        return _zoom;
    }

    void init()
    {
        _googlemap.setMyLocationEnabled(true);

        _googlemap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {


                if(mPreviousCameraPosition != null )
                {
                    if(_googlemap.getCameraPosition().equals( mPreviousCameraPosition))
                    {
                        return;
                    }
                }
                mPreviousCameraPosition = _googlemap.getCameraPosition();

                LatLng newlatlng = _googlemap.getCameraPosition().target;

                onDragged(newlatlng);

            }
        });

    }


    protected void onDragged(LatLng newdraggedposition)
    {

    }

    public boolean checkIfWithinSelectableRegion(Location location)
    {
        LatLng newlatlng = _googlemap.getCameraPosition().target;
        double distanceasperourform = GpsDistance.get(GpsUtils.latLngToLocation(newlatlng),location);
        if( distanceasperourform > _selectabledistance)
        {
            return true;
        }
        return false;
    }

    public boolean checkIfWithinSelectableRegion(LatLng currentlatlon)
    {
        LatLng newlatlng = _googlemap.getCameraPosition().target;
        double distanceasperourform = GpsDistance.get(GpsUtils.latLngToLocation(newlatlng), GpsUtils.latLngToLocation(currentlatlon));
        Log.i("gps","GOT DIFF : " + distanceasperourform);
        if( distanceasperourform > _selectabledistance)
        {
            return false;
        }
        return true;
    }


    public void changeMyLocationMarkerTo(LatLng newlatlng)
    {
        String title = _mycurrentmarker.getTitle();
        changeMyLocationMarkerTo(newlatlng,title, false);
    }


    public void changeMyLocationMarkerTo(LatLng newlatlng, String title, boolean animate)
    {
        if(_mycurrentmarker != null) {
            _mycurrentmarker.remove();
        }
        _mycurrentmarker = _googlemap.addMarker(new MarkerOptions().position(newlatlng).title(title));
        if(animate) {
            CameraUpdate camupdate = CameraUpdateFactory.newLatLngZoom(newlatlng, _zoom);
            _googlemap.moveCamera(CameraUpdateFactory.newLatLng(newlatlng));
            _googlemap.animateCamera(camupdate);
        }

    }



    public void addMarker(LatLng newlatlng, String title, int mapiconresource)
    {
        MarkerOptions marker = new MarkerOptions().position(newlatlng).title(title);
        marker.icon(BitmapDescriptorFactory.fromResource(mapiconresource));
        _googlemap.addMarker(marker);
    }


    public void addMarkerAndAnimate(LatLng previouspoint, LatLng newlatlng, String title, int mapiconresource)
    {
        addMarker( newlatlng,  title, mapiconresource);
        CameraUpdate camupdate = CameraUpdateFactory.newLatLngZoom(previouspoint, _zoom);

        _googlemap.moveCamera(CameraUpdateFactory.newLatLng(newlatlng));
        _googlemap.animateCamera(camupdate);

    }





    public void clear()
    {
         _googlemap.clear();
    }



    public GoogleMap getMap()
    {
        return _googlemap;
    }


}
