package lib.rest;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;


import com.whitesun.sparshhospital.R;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.SupposeBackground;
import org.androidannotations.annotations.SupposeUiThread;
import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import httpdata.restapi.APIInterface;
import lib.ui.ProgDialog;
import lib.utils.common.Wlog;
import lib.utils.datautils.WCalc;
import lib.utils.file.FileUtils;
import lib.utils.string.StringUtils;
import lib.vars.Values;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sumeendranath on 18/08/17.
 */
@EBean
public class HttpCall<T> {

    long _compressedfilesize = 0;
    long _totaluncompressedfilesize = 0;
    long _totaluncompressedtillnow = 0;

    public static APIInterface APIS = APIClient.getClient().create(APIInterface.class);

    public static String LOG = "HttpCall";

    public HttpCall() {

    }

    public void httpConnect(Call<T> call)
    {
        httpConnect(call, null, Values.FILESTREAMTYPE.TEXT, Values.INVALID_STRING);

    }

    public void httpConnect(Call<T> call, final  Context context, final Values.FILESTREAMTYPE filestreamtype, final String savefilename)
    {


        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(final Call<T> call, final Response<T> response) {

                //Log.d(LOG,response.code()+"");
                //Log.d(LOG,response.body()+"");

                ProgDialog.close();


                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        //TODO your background code

                        handlerCompletion(context,filestreamtype,savefilename,call,response);

                    }
                });


            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                call.cancel();
                ProgDialog.close();

                if(call.isCanceled())
                {
                    t.printStackTrace();
                    onError(t.getMessage());
                }
            }
        });
    }


    @SupposeBackground
    void handlerCompletion(final  Context context, final Values.FILESTREAMTYPE filestreamtype, final String savefilename, Call<T> call, Response<T> response)
    {

        if(!StringUtils.stringIsNullOrEmpty(savefilename) && !savefilename.equals(Values.INVALID_STRING))
        {

            if(filestreamtype == Values.FILESTREAMTYPE.COMPRESSED) {
                try {


                    ///// save the 7z file in external folder first
                    String zippedfilepath = Environment.getExternalStorageDirectory()  + File.separator + savefilename;

                    writeResponseBodyToDisk(((Response<ResponseBody>) response).body(), zippedfilepath);

                    /// send message to caller that 7z file is downloaded
                    onCompressedFileDownloadComplete();

                    _compressedfilesize = ((Response<ResponseBody>) response).body().contentLength();

                    //// decompress the 7z files into internal memory (saved as json or so)
                    List<String> listoffilenames = decompressIntoInternalMem(context,zippedfilepath);

                    Wlog.d(listoffilenames);

                    ///// remove the 7z file in external folder
                    FileUtils.removeFile(zippedfilepath);

                    //// the body is not relevant for compressed files... since its handled here. Free the memory
                    //response = null;

                    /// send a message to the called that the file processing is complete
                    onCompressedFileProcessComplete();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else
            {

                try {
                    FileUtils.saveToInternalFile(context, savefilename, ((Response<ResponseBody>) response).body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }

        onComplete(response.body());

    }


    public void httpConnectWithProgress(Context context, Call<T> call)
    {
        ProgDialog.show(context, R.string.http_prog_loading,false);
        httpConnect(call);
    }


    public void httpConnectWithProgress(Context context,String title,boolean cancellable, Call<T> call)
    {
        ProgDialog.show(context,title,cancellable);
        httpConnect(call);
    }



    @SupposeUiThread
    protected void onComplete(T response)
    {
    }

    @SupposeUiThread
    protected void onCompressedFileDownloadComplete()
    {

    }
    @SupposeUiThread
    protected void onCompressedFileDownloadProgress(double progress)
    {

    }
    @SupposeUiThread
    protected void onCompressedFileProcessComplete()
    {

    }
    @SupposeUiThread
    protected void onCompressedFileProcessProgress(double progress)
    {

    }


    protected void onError(String message)
    {

    }



    @SupposeBackground
    public void writeResponseBodyToDisk(ResponseBody body, String filepath) {
        try {
            // todo change the file location/name according to your needs
            File futureStudioIconFile = new File(filepath);

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Wlog.d( "file download: " + fileSizeDownloaded + " of " + fileSize);
                    onCompressedFileDownloadProgress(WCalc.getScaledProgress(fileSizeDownloaded,fileSize));

                }

                outputStream.flush();

                return ;
            } catch (IOException e) {
                return ;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return ;
        }
    }




    @SupposeBackground
    public List<String> decompressIntoInternalMem(Context context, String zippedfile) {

        _totaluncompressedfilesize = FileUtils.getTotalSizeOfUncompressedFiles(zippedfile);


        List<String> listoffilenamesdecom = new ArrayList<>();



        SevenZFile sevenZFile = null;
        try {
            sevenZFile = new SevenZFile(new File(zippedfile));
        } catch (IOException e) {
            e.printStackTrace();
        }


        SevenZArchiveEntry entry = null;

        try {
            entry = sevenZFile.getNextEntry();
        } catch (IOException e) {
            e.printStackTrace();
        }

        long totaluncompressedtillnow = 0l;

        while (entry!=null) {
            try {

                byte[] content = new byte[(int) entry.getSize()];
                //sevenZFile.read(content, 0, content.length);

                final byte [] buf = new byte [1024];
                FileOutputStream outputStream;
                try {
                    outputStream = context.openFileOutput(entry.getName(), Context.MODE_PRIVATE);
                    for (int len = 0; (len = sevenZFile.read(buf)) > 0;) {
                        outputStream.write(buf, 0, len);
                        totaluncompressedtillnow += 1024;
                        onCompressedFileProcessProgress(WCalc.getScaledProgress((long)totaluncompressedtillnow, _totaluncompressedfilesize));

                    }
                    outputStream.flush();
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }



               // saveToInternalFile(context, entry.getName(),content);

               // Wlog.d("uncompressing : " +content.length + "/" + _totaluncompressedfilesize + " of " + entry.getName());
                listoffilenamesdecom.add(entry.getName());
                //totaluncompressedtillnow += content.length;
                entry = sevenZFile.getNextEntry();


            } catch (IOException e) {
                e.printStackTrace();
            }
        }




        try {
            sevenZFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return listoffilenamesdecom;
    }


    public void saveToInternalFile(Context context, String filename, byte[] bytes)
    {
        byte[] buf = new byte[8192];

        FileOutputStream outputStream;
        try {
            outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(bytes);
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
