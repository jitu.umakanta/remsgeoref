package lib.rest;

import android.content.Context;

import org.androidannotations.annotations.EBean;

import httpdata.restapi.APIInterface;
import lib.utils.common.Wlog;
import lib.vars.Values;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sumeendranath on 18/08/17.
 */
@EBean
public class HttpUpload<T> {


    public static APIInterface APIS = APIClient.getClient().create(APIInterface.class);

    public static String LOG = "HttpUpload";

    public HttpUpload() {

    }

    public void httpConnect(Call<T> call)
    {
        httpConnect(call, null, Values.FILESTREAMTYPE.TEXT, Values.INVALID_STRING);

    }

    public void httpConnect(Call<T> call, final  Context context, final Values.FILESTREAMTYPE filestreamtype, final String savefilename)
    {


        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(final Call<T> call, final Response<T> response) {

                if(response.isSuccessful())
                {
                    Wlog.i("UPLOADED SUCCESSFULLY");
                }
                onComplete(response.body());


            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                call.cancel();

                if(call.isCanceled())
                {
                    onError("");
                }
            }
        });
    }



        protected void onComplete(T response)
        {
        }
        protected void onError(String message)
        {

        }




    }
