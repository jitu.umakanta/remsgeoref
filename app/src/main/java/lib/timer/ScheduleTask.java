package lib.timer;

import java.util.Timer;
import java.util.TimerTask;

import lib.utils.common.Wlog;

public class ScheduleTask extends TimerTask {

    long _recurringtime = -1;
    String _taskname = "";
    long _delayinsecs = 0;
    public ScheduleTask(long delayinsecs,long recurringtimeinseconds, String taskname) {


        _recurringtime = recurringtimeinseconds * 1000;
        _taskname = taskname;
        _delayinsecs = delayinsecs * 1000;

        if(_recurringtime <= 0)
        {
            Wlog.i("error!!! timer set to invalid period");
            return;
        }
        if(_delayinsecs < 0)
        {
            Wlog.i("error!!! timer set to invalid period");
            return;
        }


        Timer timer = new Timer();
        // Schedule to run after every X seconds
        timer.schedule(this, _delayinsecs,_recurringtime);

    }

    @Override
    public void run() {
            onCall();
        Wlog.i("Scheduler ended:: " + _taskname);
    }


    protected void onCall()
    {
            Wlog.i("Scheduler started:: " + _taskname);
    }
}
