package lib.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.whitesun.sparshhospital.R;


/**
 * Created by sumeendranath on 18/08/17.
 */

public class ProgDialog {


    static AlertDialog alertDialog;
    public static void show(Context context, String title, boolean cancelable)
    {


        try
        {
            alertDialog.dismiss();
        }
        catch (Exception ex){}

        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.lib_dialog_progress, null);


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);


        final TextView textview_gps_title = (TextView) promptsView.findViewById(R.id.progress_title);

        textview_gps_title.setText(title);

        alertDialogBuilder.setCancelable(cancelable);

        alertDialog = alertDialogBuilder.create();

        alertDialog.setView(promptsView,0,0,0,0);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        alertDialog.show();

    }

    public static void show(Context context, int titleResourceString, boolean cancelable)
    {
        String title = context.getString(titleResourceString);
        show(context,title,cancelable);
    }


    public static void close() {
        try {
            if(alertDialog != null) {
                alertDialog.dismiss();
                alertDialog = null;
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

}
