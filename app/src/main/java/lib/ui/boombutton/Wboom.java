package lib.ui.boombutton;

import android.content.Context;
import android.util.AttributeSet;

import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.SimpleCircleButton;
import com.nightonke.boommenu.BoomMenuButton;

/**
 * Created by sumeendranath on 02/09/17.
 */

public class Wboom extends BoomMenuButton {
    public Wboom(Context context) {
        super(context);
    }

    public Wboom(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Wboom(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void add(int color, int resource, String text, OnBMClickListener click)
    {
        addBuilder(new SimpleCircleButton.Builder()
                .listener(click)
                .buttonRadius(80)
                .normalColorRes(color)
                .normalImageRes(resource))
                ;

    }
}
