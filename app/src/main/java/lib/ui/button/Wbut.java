package lib.ui.button;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

/**
 * Created by sumeendranath on 30/08/17.
 */

public class Wbut extends AppCompatButton {
    public Wbut(Context context) {
        super(context);
    }

    public Wbut(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Wbut(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
