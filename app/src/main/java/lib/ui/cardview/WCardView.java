package lib.ui.cardview;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;

/**
 * Created by sumeendranath on 31/08/17.
 */

public class WCardView extends CardView {
    public WCardView(Context context) {
        super(context);
    }

    public WCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
