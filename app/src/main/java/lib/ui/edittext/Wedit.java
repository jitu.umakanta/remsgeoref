package lib.ui.edittext;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

/**
 * Created by sumeendranath on 30/08/17.
 */

public class Wedit extends AppCompatEditText {
    public Wedit(Context context) {
        super(context);
        init();
    }

    public Wedit(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Wedit(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    void init()
    {
        setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if(submitbutton!= null) {
                        submitbutton.performClick();
                    }
                    return true;
                }
                return false;
            }
        });
    }


    View submitbutton;
    public void setSubmitButton(View button) {
        submitbutton = button;
    }


    public void openKeyBoardAutomatically(Context context)
    {
        requestFocus();
        InputMethodManager imm = (InputMethodManager)context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);

    }



}
