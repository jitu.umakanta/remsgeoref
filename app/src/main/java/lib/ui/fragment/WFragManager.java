package lib.ui.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

/**
 * Created by sumeendranath on 31/08/17.
 */

public class WFragManager {
    public  static void replace(Activity context, Fragment fragment, int idtoreplace)
    {
        if(context.getFragmentManager().findFragmentById(idtoreplace) == null) {
            Fragment menuListFragment = fragment;
            FragmentManager fm = context.getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(idtoreplace, menuListFragment);
            ft.commit();
        }
    }


}
