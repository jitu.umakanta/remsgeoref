package lib.ui.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

/**
 * Created by sumeendranath on 31/08/17.
 */

public class WFragment extends Fragment {

    public void replace(Activity context, int idtoreplace)
    {

        if(context.getFragmentManager().findFragmentById(idtoreplace) == null) {
            FragmentManager fm = context.getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(idtoreplace, this);
            ft.commit();
        }
    }

    public void refresh()
    {
        FragmentTransaction t = getActivity().getFragmentManager().beginTransaction();
        //t.setAllowOptimization(false);
        t.detach(this).attach(this).commitAllowingStateLoss();
    }


    public void getDataFromActivity(Object object)
    {

    }

}
