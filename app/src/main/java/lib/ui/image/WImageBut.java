package lib.ui.image;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;


import com.whitesun.sparshhospital.R;

import lib.utils.file.FileUtils;
import lib.utils.string.StringUtils;

/**
 * Created by sumeendranath on 31/08/17.
 */

public class WImageBut extends AppCompatImageButton {

    public String _uniqueid = "";

    public WImageBut(Context context) {
        super(context);
    }

    public WImageBut(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public WImageBut(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.WImageBut, defStyleAttr, 0);

        String filepath = a.getString(R.styleable.WImageBut_setSrc);

        setSrc(filepath);

        String uid = a.getString(R.styleable.WImageBut_setUniqueId);
        setUniqueId(uid);

        a.recycle();



    }


    public void setSrc(String filepath)
    {
        if(!StringUtils.stringIsNullOrEmpty(filepath)) {
            if(FileUtils.fileExists(filepath)) {
                WPhoto.setImageThumbIntoImageView(getContext(), filepath, this);
            }
            else
            {
                this.setImageResource(R.mipmap.ic_collapse_select);
            }
        }
    }


    public void setUniqueId(String uniqueid)
    {
        _uniqueid = uniqueid;
    }

    public String getUniqueId()
    {
        return _uniqueid;
    }

}
