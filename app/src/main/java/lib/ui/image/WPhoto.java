package lib.ui.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLConnection;

import lib.utils.file.FileUtils;

/**
 * Created by sumeendranath on 03/09/17.
 */

public class WPhoto {

        static public void setImageOnImageView(Context context, String filecompletepath, WImageView iv)
        {

            File file = new File(filecompletepath);
            Uri imageUri = Uri.fromFile(file);
            Glide.with(context)
                    .load(imageUri)
                    .into(iv);
        }

    static public void setImageThumbIntoImageView(Context context, String filecompletepath, WImageView iv)
    {
        RequestOptions options = new RequestOptions();
        options.centerInside();


        File file = new File(filecompletepath);
        Uri imageUri = Uri.fromFile(file);
        Glide.with(context)
                .load(imageUri)
                .thumbnail(.1f)
                .apply(options)
                .into(iv);
    }

    static public void setImageThumbIntoImageView(Context context, String filecompletepath, WImageBut iv)
    {
        RequestOptions options = new RequestOptions();
        options.diskCacheStrategy(DiskCacheStrategy.NONE);
       // options.fitCenter();
        options.centerInside();
        options.skipMemoryCache(true);

        File file = new File(filecompletepath);
        Uri imageUri = Uri.fromFile(file);
        Glide.with(context)
                .load(imageUri)
                .thumbnail(.1f)
                .apply(options)
                .into(iv);
    }

    public static boolean isImageFile(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType != null && mimeType.startsWith("image");
    }



    public static void reducepicturesize(String filepath, int maxedge)
    {
       /* final BitmapFactory.Options options = new BitmapFactory.Options();

        options.inSampleSize = calculateInSampleSize(options, maxedge, maxedge);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        Bitmap bm= BitmapFactory.decodeByteArray(picbytes, 0, picbytes.length, options);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();


        */


        scale(filepath,maxedge);


    }


    public static void scale(String filepath, int mazedge)  {
        Bitmap b = BitmapFactory.decodeFile(filepath);
// original measurements
        int origWidth = b.getWidth();
        int origHeight = b.getHeight();

        final int destWidth = mazedge;//or the width you need

        if (origWidth > destWidth) {
            // picture is wider than we want it, we calculate its target height
            int destHeight = origHeight / (origWidth / destWidth);
            // we create an scaled bitmap so it reduces the image, not just trim it
            Bitmap b2 = Bitmap.createScaledBitmap(b, destWidth, destHeight, false);
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            // compress to the format you want, JPEG, PNG...
            // 70 is the 0-100 quality percentage
            b2.compress(Bitmap.CompressFormat.JPEG, 70, outStream);
            // we save the file, at least until we have made use of it

            ExifInterface oldexif = null;
            try {
                 oldexif = new ExifInterface(filepath);
            } catch (IOException e) {
                e.printStackTrace();
            }


            FileUtils.removeFile(filepath);
            File f = new File(filepath);
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //write the bytes in file
            FileOutputStream fo = null;
            try {
                fo = new FileOutputStream(f);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                fo.write(outStream.toByteArray());
            } catch (IOException e) {
                e.printStackTrace();
            }
            // remember close de FileOutput
            try {
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


            try {
                copyExif(oldexif,filepath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }




    public static void copyExif(ExifInterface oldexif, String newPath) throws IOException
    {

        String[] attributes = new String[]
                {
                        ExifInterface.TAG_APERTURE,
                        ExifInterface.TAG_DATETIME,
                        ExifInterface.TAG_DATETIME_DIGITIZED,
                        ExifInterface.TAG_EXPOSURE_TIME,
                        ExifInterface.TAG_FLASH,
                        ExifInterface.TAG_FOCAL_LENGTH,
                        ExifInterface.TAG_GPS_ALTITUDE,
                        ExifInterface.TAG_GPS_ALTITUDE_REF,
                        ExifInterface.TAG_GPS_DATESTAMP,
                        ExifInterface.TAG_GPS_LATITUDE,
                        ExifInterface.TAG_GPS_LATITUDE_REF,
                        ExifInterface.TAG_GPS_LONGITUDE,
                        ExifInterface.TAG_GPS_LONGITUDE_REF,
                        ExifInterface.TAG_GPS_PROCESSING_METHOD,
                        ExifInterface.TAG_GPS_TIMESTAMP,
                        ExifInterface.TAG_IMAGE_LENGTH,
                        ExifInterface.TAG_IMAGE_WIDTH,
                        ExifInterface.TAG_ISO,
                        ExifInterface.TAG_MAKE,
                        ExifInterface.TAG_MODEL,
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.TAG_SUBSEC_TIME,
                        ExifInterface.TAG_SUBSEC_TIME_DIG,
                        ExifInterface.TAG_SUBSEC_TIME_ORIG,
                        ExifInterface.TAG_WHITE_BALANCE
                };

        ExifInterface newExif = new ExifInterface(newPath);
        for (int i = 0; i < attributes.length; i++)
        {
            String value = oldexif.getAttribute(attributes[i]);
            if (value != null)
                newExif.setAttribute(attributes[i], value);
        }
        newExif.saveAttributes();
    }



    public static  int calculateInSampleSize(BitmapFactory.Options options,
                                       int reqWidth, int reqHeight) {
// Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }


}
