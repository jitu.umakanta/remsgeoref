package lib.ui.keyboard;

import android.app.Activity;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by sumeendranath on 30/08/17.
 */

public class WKey {

    public static void close(Activity context)
    {
        if(context.getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(), 0);
        }
    }
}
