package lib.ui.listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by sumeendranath on 30/08/17.
 */

public class GenerciListViewAdapter<T> extends BaseAdapter {

    private ArrayList<T> mData = new ArrayList<T>();
    private LayoutInflater mInflater;

    Context _maincontext;


    public GenerciListViewAdapter(Context context) {
        _maincontext = context;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public void addItem(final T item) {
        mData.add(item);
        notifyDataSetChanged();
    }



    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int i) {
        return mData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) _maincontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //View rowView = inflater.inflate(R.layout.item_listview, parent, false);



        return onViewInserted( position,  inflater, parent);

    }


    protected View onViewInserted(int position, LayoutInflater inflater,ViewGroup parent)
    {
        return null;
    }
}
