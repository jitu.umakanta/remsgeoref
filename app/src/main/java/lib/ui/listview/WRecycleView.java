package lib.ui.listview;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by sumeendranath on 02/09/17.
 */

public class WRecycleView extends RecyclerView {
    public WRecycleView(Context context) {
        super(context);
        setLayout(context,LinearLayoutManager.VERTICAL);
    }

    public WRecycleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setLayout(context,LinearLayoutManager.VERTICAL);

    }

    public WRecycleView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setLayout(context,LinearLayoutManager.VERTICAL);
    }


    public void setLayout(Context context,int orientation)
    {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        mLayoutManager.setOrientation(orientation);
        setLayoutManager(mLayoutManager);
    }

}
