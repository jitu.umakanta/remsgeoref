package lib.ui.progress;

import android.content.Context;
import android.util.AttributeSet;

import com.github.lzyzsd.circleprogress.CircleProgress;

/**
 * Created by sumeendranath on 29/08/17.
 */

public class WProgressCircle extends CircleProgress {

    public WProgressCircle(Context context) {
        super(context);
        init();
    }


    public WProgressCircle(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WProgressCircle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init()
    {
        setMax(100);
        setProgress(0);
    }

    public void setProgress(Double p)
    {
        setProgress(p.intValue());
    }

}
