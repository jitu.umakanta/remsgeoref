package lib.ui.progress;

import android.content.Context;
import android.util.AttributeSet;

import com.github.lzyzsd.circleprogress.DonutProgress;

/**
 * Created by sumeendranath on 29/08/17.
 */

public class WProgressDonut extends DonutProgress {

    public WProgressDonut(Context context) {
        super(context);
        init();
    }

    public WProgressDonut(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WProgressDonut(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init()
    {
        setMax(100);
        setProgress(0);
    }

    public void setProgress(Double p)
    {
        setProgress(p.floatValue());
    }

}
