package lib.ui.progress;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;

import ch.halcyon.squareprogressbar.SquareProgressBar;

/**
 * Created by sumeendranath on 29/08/17.
 */

public class WProgressSquare extends SquareProgressBar{


    private int bitmapwidth = 250;
    private int bitmapheight = 250;
    private int bitmapfontsize = 30;
    private int bitmapfontcolor = Color.RED;

    Bitmap bmp;


    public WProgressSquare(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public WProgressSquare(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WProgressSquare(Context context) {
        super(context);
    }



    public void setBitMapDimens(int w, int h)
    {
        bitmapheight = h;
        bitmapwidth = w;
    }


    public void setBitMapDimens(int w, int h, int fontsize)
    {
        bitmapheight = h;
        bitmapwidth = w;
        bitmapfontsize = fontsize;
    }

    public void setBitmapfontcolor(int color)
    {
        bitmapfontcolor = color;
    }



    public void setText(String text)
    {
        if (bmp != null) {
            bmp.recycle();
            bmp = null;
        }

        bmp = Bitmap.createBitmap(bitmapwidth, bitmapheight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bmp);
        Paint paint = new Paint();
        paint.setTextSize(bitmapfontsize);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setColor(bitmapfontcolor);//Color.parseColor("#D11111"));
        canvas.drawText(text,bitmapwidth/2,bitmapheight/2, paint);

        drawOutline(true);

        this.setImageBitmap(bmp);

    }

    public void setInfinite()
    {
        setIndeterminate(true);
    }
}
