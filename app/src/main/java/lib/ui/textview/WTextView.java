package lib.ui.textview;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by sumeendranath on 29/08/17.
 */

public class WTextView extends AppCompatTextView{


    public WTextView(Context context) {
        super(context);
    }

    public WTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
