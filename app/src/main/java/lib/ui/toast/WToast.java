package lib.ui.toast;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by sumeendranath on 02/09/17.
 */

public class WToast extends Toast {
    public WToast(Context context) {
        super(context);
    }

    public static void msg(Context context, String msg)
    {
        makeText(context,msg,Toast.LENGTH_SHORT).show();
    }
    public static void msgLong(Context context, String msg)
    {
        makeText(context,msg,Toast.LENGTH_LONG).show();
    }

}
