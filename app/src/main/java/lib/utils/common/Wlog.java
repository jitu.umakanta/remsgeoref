package lib.utils.common;

import android.util.Log;

import configuration.Config;
import lib.utils.datautils.JsonUtil;
import lib.vars.Values;

/**
 * Created by sumeendranath on 27/08/17.
 */
public class Wlog {

   // static Logger log = Logger.getLogger(Config.PROJECTID);

    static String defaultlog = Config.PROJECTNAME;
    public static void i(String str)
    {
        if(checkIfLoggable()) {
            //log.info(str);

            Log.i(defaultlog,str);
        }
    }

    public static void d(String filter, String str)
    {
        if(checkIfLoggable()) {
            //log.info(str);

            Log.d(filter,str);
        }
    }


    public static void d(String str)
    {
        if(checkIfLoggable()) {
            //log.info(str);

            Log.d(defaultlog,str);
        }
    }

    public static void t(String filter, String str)
    {
        if(checkIfLoggable()) {
            //log.info(str);
            Log.v(filter,str);
        }
    }


    public static void t(String str)
    {
        if(checkIfLoggable()) {
            //log.info(str);
            Log.v(defaultlog,str);
        }
    }


    public static boolean checkIfLoggable()
    {
        if(
                Config.RELEASE_MODE == Values.RUNMODE.DEBUG_QUIET
                        || Config.RELEASE_MODE == Values.RUNMODE.RELEASE
                ) {
            return false;
        }
        return true;
    }


    public static void d (Object jsonobject)
    {
        d(JsonUtil.toJson(jsonobject).toString());
    }

    public static void i (Object jsonobject)
    {
        i(JsonUtil.toJson(jsonobject).toString());
    }
}
