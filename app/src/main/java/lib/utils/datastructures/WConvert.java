package lib.utils.datastructures;

import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by sumeendranath on 03/09/17.
 */

public class WConvert<T> {

    public List<T> observeListToList(ObservableList<T> observelist)
    {
       T[] array = (T[]) observelist.toArray();
        return Arrays.asList(array);
    }

    public ArrayList<T> observeListToArrayList(ObservableList<T> observelist)
    {
        ArrayList<T> list = new ArrayList<>();
        for(int i=0;i<observelist.size();i++)
        {
            list.add(observelist.get(i));
        }

        return list;
    }

    public ObservableList<T> ArrayListToObservableList(ArrayList<T> arraylist)
    {
        ObservableList<T> list = new ObservableArrayList<>();
        for(int i=0;i<arraylist.size();i++)
        {
            list.add(arraylist.get(i));
        }

        return list;
    }

}
