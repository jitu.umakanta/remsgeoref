package lib.utils.datastructures;

import java.util.Map;

/**
 * Created by sumeendranath on 31/08/17.
 */

public class WMapUtils {
    public static Object getFirstKey(Map<Object,Object> map)
    {
        Map.Entry<Object,Object> entry=map.entrySet().iterator().next();
        return entry.getKey();
    }

}
