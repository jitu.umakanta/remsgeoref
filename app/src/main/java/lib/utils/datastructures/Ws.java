package lib.utils.datastructures;

import lib.utils.string.StringUtils;

/**
 * Created by sumeendranath on 28/08/17.
 */

public class Ws {

    String s = "";


    public static Ws from(String s)
    {
        return new Ws(s);
    }

    public Ws(String s) {
        this.s = s;
    }

    public Ws() {
    }

    public void st(String str) {
        s=str;
    }


    public boolean isNullOrEmpty() {
        if(StringUtils.stringIsNullOrEmpty(s))
        {
            return true;
        }
        return false;
    }

    public int len()
    {
        return s.length();
    }


    public String s()
    {
        return s;
    }

    public Double toDouble()
    {
        return Wto.Double(s);
    }
    public int toInt()
    {
        return Wto.Int(s);
    }

}
