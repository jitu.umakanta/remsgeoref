package lib.utils.datastructures;

import lib.vars.Values;

public class Wto {

    public static Integer Int(String s)
    {
        try {
            Integer i = Integer.parseInt(s);
            return i;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return Values.INVALID_INT;
    }


    public static Double Double(String s)
    {
        try {
            Double i = Double.parseDouble(s);
            return i;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return Values.INVALID_DOUBLE;
    }




}
