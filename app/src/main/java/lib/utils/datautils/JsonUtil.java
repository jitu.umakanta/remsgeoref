package lib.utils.datautils;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import lib.utils.file.FileUtils;

public class JsonUtil {

   public void jsonStringtoMap()
    {
        //return new Gson().fromJson(jsonstring,classtype);
    }

    public static Map<String, Object> jsonStringtoMap(String jsonstr)  {
        try {
            return jsonToMap(createJsonObjectFromString(jsonstr));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return new HashMap<>();
    }


        public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if(json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<>();

        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<>();
        for(int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    public static String toJson(Object object) {

        return new Gson().toJson(object);
    }

    public static String convertPojoToJson(Object obj) {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(obj);// obj is your object
    }

    public static JSONObject jsonObjectFromInternalFile(Context context, String filepath) {

        return createJsonObjectFromString(FileUtils.readFromInternalFile(context,filepath));
    }




    public static String getJsonStringFromInternalFile(Context context, String filepath) {

        return FileUtils.readFromInternalFile(context,filepath);
    }


    public static JSONObject createJsonObjectFromString(String jsonstring)
    {
        JSONObject obj = null;



        try {

            obj = new JSONObject(jsonstring);


        } catch (Exception ex) {

            ex.printStackTrace();

        }

        if(obj == null)
        {
            try {
                obj = new JSONObject("[]");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return obj;
    }


}