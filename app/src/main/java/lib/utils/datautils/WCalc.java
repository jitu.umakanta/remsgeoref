package lib.utils.datautils;

/**
 * Created by sumeendranath on 29/08/17.
 */

public class WCalc {

    public static Double getScaledProgress(Double progress, Double maxsize)
    {
        return ((double)progress/(double)maxsize) * 100d;
    }
    public static Double getScaledProgress(int progress, int maxsize)
    {
        return ((double)progress/(double)maxsize) * 100d;
    }
    public static Double getScaledProgress(long progress, long maxsize)
    {
        return ((double)progress/(double)maxsize) * 100d;
    }

}
