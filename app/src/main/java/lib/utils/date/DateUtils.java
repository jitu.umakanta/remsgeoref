package lib.utils.date;

import java.util.Calendar;

import lib.utils.string.StringUtils;

/**
 * Created by sumeendranath on 03/09/17.
 */

public class DateUtils {

    static public String getCurrentdateTimeString(String sep)
    {
        String retValue = "";

        Calendar c1 = Calendar.getInstance();

        int day = c1.get(Calendar.DAY_OF_MONTH);
        int month = c1.get(Calendar.MONTH) + 1;
        int year = c1.get(Calendar.YEAR);
        int hour = c1.get(Calendar.HOUR_OF_DAY);
        int minute = c1.get(Calendar.MINUTE);
        int second = c1.get(Calendar.SECOND);

        retValue =
                convertNumberTodateTimeFormat(day) + sep +
                        convertNumberTodateTimeFormat(month) + sep +
                        convertNumberTodateTimeFormat(year).substring(2)  + sep +
                        convertNumberTodateTimeFormat(hour) + sep +
                        convertNumberTodateTimeFormat(minute) + sep +
                        convertNumberTodateTimeFormat(second);


        return retValue;
    }

    static public String getCurrentdateTimeString_shortID(String sep)
    {
        String retValue = "";

        Calendar c1 = Calendar.getInstance();

        int day = c1.get(Calendar.DAY_OF_MONTH);
        int month = c1.get(Calendar.MONTH) + 1;
        int year = c1.get(Calendar.YEAR);
        int hour = c1.get(Calendar.HOUR_OF_DAY);
        int minute = c1.get(Calendar.MINUTE);
        int second = c1.get(Calendar.SECOND);

        retValue =
                convertNumberTodateTimeFormat(day) + sep +
                        convertNumberTodateTimeFormat(month) + sep +
                        convertNumberTodateTimeFormat(year) + sep +
                        convertNumberTodateTimeFormat(hour) + sep +
                        convertNumberTodateTimeFormat(minute) + sep +
                        convertNumberTodateTimeFormat(second)
        ;


        return retValue;
    }

    static public String getDateTimeStringFromDateNumber(String datetime, String sep)
    {
        String retString = "";

        if(datetime.length() < 14)
        {
            return "";
        }

        if(StringUtils.stringIsNullOrEmpty(sep))
        {
            retString =
                    "Date: " + datetime.charAt(0) + datetime.charAt(1) + "/" +
                            datetime.charAt(2) + datetime.charAt(3) + "/" +
                            datetime.charAt(4) + datetime.charAt(5) + datetime.charAt(6) + datetime.charAt(7)
                            + " Time: " +
                            datetime.charAt(8) + datetime.charAt(9) + ":" +
                            datetime.charAt(10) + datetime.charAt(11) + ":" +
                            datetime.charAt(12) + datetime.charAt(13);



        }

        return retString;
    }

    static public String convertNumberTodateTimeFormat( int No)
    {

        if(No < 10)
        {
            return "0" + No;
        }
        else
        {
            return "" + No;
        }
    }

    static public String getCurrentTimeString(String sep)
    {
        String retValue = "";

        Calendar c1 = Calendar.getInstance();

        int hour = c1.get(Calendar.HOUR_OF_DAY);
        int minute = c1.get(Calendar.MINUTE);

        retValue = 	hour + sep +
                minute;
        return retValue;
    }

    static public String getCurrentDateString(String sep)
    {
        String retValue = "";

        Calendar c1 = Calendar.getInstance();

        int day = c1.get(Calendar.DAY_OF_MONTH);
        int month = c1.get(Calendar.MONTH) + 1;
        int year = c1.get(Calendar.YEAR);

        retValue =  day + sep +
                month + sep +
                year ;
        return retValue;
    }

    static public String getCurrentDateTimeStringMySqlFormat()
    {
        String sep1= "-";
        String sep2= ":";

        String retValue = "";

        Calendar c1 = Calendar.getInstance();

        int day = c1.get(Calendar.DAY_OF_MONTH);
        int month = c1.get(Calendar.MONTH) + 1;
        int year = c1.get(Calendar.YEAR);
        int hour = c1.get(Calendar.HOUR_OF_DAY);
        int minute = c1.get(Calendar.MINUTE);
        int second = c1.get(Calendar.SECOND);

        retValue =
                convertNumberTodateTimeFormat(year)  + sep1 +
                        convertNumberTodateTimeFormat(month) + sep1 +
                        convertNumberTodateTimeFormat(day) + " " +
                        convertNumberTodateTimeFormat(hour) + sep2 +
                        convertNumberTodateTimeFormat(minute) + sep2 +
                        convertNumberTodateTimeFormat(second);


        return retValue;
    }

}
