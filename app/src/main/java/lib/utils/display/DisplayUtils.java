package lib.utils.display;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Created by sumeendranath on 05/10/17.
 */

public class DisplayUtils {


    public static float getDisplayHeightinDp(Context context)
    {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;

        return dpHeight;

    }

    public static float getDisplayWidthinDp(Context context)
    {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        return dpWidth;

    }

}
