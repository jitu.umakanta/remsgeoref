package lib.utils.file;

import android.os.Environment;

import java.io.File;

import configuration.AndroidDefaults;

/**
 * Created by sumeendranath on 03/09/17.
 */

public class FileExternal {

    public static String getSeparator()
    {
        return File.separator;
    }

    public static String getMemoryCard()
    {
        return Environment.getExternalStorageDirectory() + getSeparator();
    }

    public static String getRootDirectory()
    {
        String dirName =  getMemoryCard() + getSeparator() + AndroidDefaults.FOLDER_MEMORYCARD +  getSeparator();
        File tempf = new File(dirName);

        if(!tempf.exists())
        {
            tempf.mkdirs();
        }

        return dirName;
    }


    public static String getUploadDir()
    {
        String dirName =  getRootDirectory() + getSeparator() + AndroidDefaults.UPLOADDIR_MEMORYCARD +  getSeparator();
        File tempf = new File(dirName);

        if(!tempf.exists())
        {
            tempf.mkdirs();
        }

        return dirName;
    }

    public static String getUploadFinalFile(String uniqueid)
    {
        String dirName =  getUploadDir() + getSeparator() + uniqueid;

        return dirName;
    }


    public static String getTempDir()
    {
        String dirName =  getRootDirectory() + getSeparator() + AndroidDefaults.TEMPDIR_MEMORYCARD +  getSeparator();
        File tempf = new File(dirName);

        if(!tempf.exists())
        {
            tempf.mkdirs();
        }

        return dirName;
    }

    public static String getUploadFileName(String uniqueid, String tag, String ext)
    {
        String dirName =  uniqueid + tag + ext;

        return dirName;
    }



    public static String getAPITempFile(String uniqueid)
    {
        //String dirName = getTempDir() + AndroidDefaults.TAG_CONSUME + uniqueid + AndroidDefaults.EXT_CONSUME;

        return getTempDir() + getUploadFileName(uniqueid,AndroidDefaults.TAG_CONSUME,AndroidDefaults.EXT_CONSUME);
    }

    public static String getPhotosTempDir()
    {
        String dirName =  getTempDir() + getSeparator()  + AndroidDefaults.PHOTOS_TEMPDIR_MEMORYCARD + getSeparator();
        File tempf = new File(dirName);

        if(!tempf.exists())
        {
            tempf.mkdirs();
        }

        return dirName;
    }
    public static String getSignatureTempDir()
    {
        String dirName =  getTempDir() + getSeparator()  + AndroidDefaults.SIGNATURE_TEMPDIR_MEMORYCARD + getSeparator();
        File tempf = new File(dirName);

        if(!tempf.exists())
        {
            tempf.mkdirs();
        }

        return dirName;
    }

    public static String getAudioTempDir()
    {
        String dirName =  getTempDir() + getSeparator()  + AndroidDefaults.AUDIO_TEMPDIR_MEMORYCARD + getSeparator();
        File tempf = new File(dirName);

        if(!tempf.exists())
        {
            tempf.mkdirs();
        }

        return dirName;
    }

    public static String getSelfieTempDir()
    {
        String dirName =  getTempDir() + getSeparator()  + AndroidDefaults.SELFIE_TEMPDIR_MEMORYCARD + getSeparator();
        File tempf = new File(dirName);

        if(!tempf.exists())
        {
            tempf.mkdirs();
        }

        return dirName;
    }

}
