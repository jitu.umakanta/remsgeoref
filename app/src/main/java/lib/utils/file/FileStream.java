package lib.utils.file;

import android.content.Context;

import java.io.FileOutputStream;

import lib.vars.Values;

/**
 * Created by sumeendranath on 29/08/17.
 */

public class FileStream {

    public FileStream(Context context, String filepath, Values.STORAGETYPE storagetype) {

        switch(storagetype)
        {
            case INTERNAL:
                //saveToInternalFile(context,filepath);
                break;

        }


    }


    public void saveToInternalFile(Context context, String filename, byte[] bytes)
    {
        FileOutputStream outputStream;
        try {
            outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(bytes);
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }




    protected void onProgress(Double progress)
    {

    }

}
