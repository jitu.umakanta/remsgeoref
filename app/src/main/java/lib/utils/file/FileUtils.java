package lib.utils.file;

import android.content.Context;
import android.graphics.Bitmap;

import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZFile;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import configuration.AndroidDefaults;
import lib.utils.common.Wlog;
import lib.utils.string.StringUtils;
import lib.vars.Values;
import okhttp3.ResponseBody;

/**
 * Created by sumeendranath on 28/08/17.
 */

public class FileUtils {


    public static boolean internalFileExists(Context context, String fname) {
        File file = context.getFileStreamPath(fname);
        return file.exists();
    }

    public static String leaf(String fname) {
        File file = new File(fname);
        if (StringUtils.stringIsNullOrEmpty(file.getName())) {
            return fname;
        }
        return file.getName();
    }


    static public void saveToInternalFile(Context context, String filename, String string) {
        FileOutputStream outputStream;
        try {

            Wlog.t(filename);

            if (context == null) {

                Wlog.d("context is NULL !!!");
            }
            outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(string.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    static public void saveToInternalFile(Context context, String filename, byte[] bytes) {
        FileOutputStream outputStream;
        try {
            outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(bytes);
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static String readFromInternalFile(Context context, String filename) {
        try {
            FileInputStream fis = context.openFileInput(filename);
            InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            return sb.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }


    public static byte[] decompressByteArray(byte[] bytes) {

        ByteArrayOutputStream baos = null;
        Inflater iflr = new Inflater();
        iflr.setInput(bytes);
        baos = new ByteArrayOutputStream();
        byte[] tmp = new byte[4 * 1024];
        try {
            while (!iflr.finished()) {
                int size = iflr.inflate(tmp);
                baos.write(tmp, 0, size);
            }
        } catch (Exception ex) {

        } finally {
            try {
                if (baos != null) baos.close();
            } catch (Exception ex) {
            }
        }

        return baos.toByteArray();
    }

    static public boolean removeFile(String filename) {
        File filetodel = new File(filename);

        return filetodel.delete();
    }

    static public String getDirectoryOfFile(String filename) {
        File fileHand = new File(filename);
        return fileHand.getParent();
    }

    static public boolean fileExists(String filename) {
        File filetochk = new File(filename);
        if (filetochk.exists()) {
            return true;
        } else {
            return false;
        }
    }

    static public long getFileSize(String filename) {
        File filetochk = new File(filename);
        return filetochk.length();
    }

    static public boolean isFileEmpty(String filename) {
        File filetochk = new File(filename);
        if (filetochk.length() <= 0) {
            return true;
        }


        return false;
    }

    static public String getLeafFileName(String filename) {
        File filetochk = new File(filename);
        return filetochk.getName();
    }

    static public boolean moveFile(String inputFileName, String outputFileName) {
        File inputFile = new File(inputFileName);
        File outputFile = new File(outputFileName);

        return inputFile.renameTo(outputFile);
    }

    public static void copy(File src, File dst) {
        FileInputStream inStream = null;
        try {
            inStream = new FileInputStream(src);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        FileOutputStream outStream = null;
        try {
            outStream = new FileOutputStream(dst);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        try {
            inChannel.transferTo(0, inChannel.size(), outChannel);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            inStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            outStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    static public boolean copyFile(String inputFileName, String outputFileName) {
        File inputFile = new File(inputFileName);
        File outputFile = new File(outputFileName);

        try {
            FileReader in = new FileReader(inputFile);
            FileWriter out = new FileWriter(outputFile);
            int c;

            while ((c = in.read()) != -1)
                out.write(c);

            in.close();
            out.close();
        } catch (Exception ex) {
            return false;
        }


        return true;
    }

    static byte[] readDataFromFile(String filename) {


        byte[] data = new byte[1];


        try {
            RandomAccessFile raf = new RandomAccessFile(filename, "r");
            // Get and check length
            long longlength = raf.length();
            int length = (int) longlength;
            if (length != longlength) throw new IOException("File size >= 2 GB");

            // Read file and return data
            data = new byte[length];
            raf.readFully(data);
            raf.close();

        } catch (Exception ex) {

        } finally {
        }

        return data;
    }


    static public boolean reverseFileContents(String inputFileName, String outputFileName) {

        byte[] bytesToWrite = readDataFromFile(inputFileName);

        byte[] inverted = invertByteArray(bytesToWrite);  //new byte[bytesToWrite.length];

        writeIntoBinaryFile(outputFileName, inverted);
        return true;
    }


    public static byte[] invertByteArray(byte[] data) {
        //checkNull(data, "Data");

        int d = 0;
        for (int i = 0; i < data.length; ++i) {
            d = data[i] & 0xFF;
            d = ~d;
            data[i] = (byte) d;
        }

        return data;
    }

    public static boolean writeIntoFile(String strToWrite, String fileName) {


        FileWriter fw;
        try {
            fw = new FileWriter(fileName, false);
            fw.write(strToWrite + "\n");
            fw.flush();
            fw.close();
            return true;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //Toast.makeText(,"writing file failed " + e.getMessage(), Toast.LENGTH_SHORT).show();


        }

        return false;
    }

    public static boolean writeResponseBodyToInternalStorage(Context context, ResponseBody body, String filepath) {
        try {
            // todo change the file location/name according to your needs
            //File futureStudioIconFile = new File(filepath);

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = context.openFileOutput(filepath, Context.MODE_PRIVATE);

                //outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Wlog.d("file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    /*
    public static boolean writeResponseBodyToDisk(ResponseBody body, String filepath) {
        try {
            // todo change the file location/name according to your needs
            File futureStudioIconFile = new File(filepath);

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Wlog.d( "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }
    */


    public static byte[] fullyReadFileToBytes(File f) throws IOException {
        int size = (int) f.length();
        byte bytes[] = new byte[size];
        byte tmpBuff[] = new byte[size];
        FileInputStream fis = new FileInputStream(f);
        ;
        try {

            int read = fis.read(bytes, 0, size);
            if (read < size) {
                int remain = size - read;
                while (remain > 0) {
                    read = fis.read(tmpBuff, 0, remain);
                    System.arraycopy(tmpBuff, 0, bytes, size - remain, read);
                    remain -= read;
                }
            }
        } catch (IOException e) {
            throw e;
        } finally {
            fis.close();
        }

        return bytes;
    }


    public static void writeIntoBinaryFileTry(String filename, byte[] bytes) {
        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(filename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            bos.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            bos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static long getTotalSizeOfUncompressedFiles(String zippedfile) {
        long retval = 0l;

        SevenZFile sevenZFile = null;
        try {
            sevenZFile = new SevenZFile(new File(zippedfile));
        } catch (IOException e) {
            e.printStackTrace();
        }

        SevenZArchiveEntry entry = null;

        try {
            entry = sevenZFile.getNextEntry();
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (entry != null) {
            try {
                retval += entry.getSize();
                entry = sevenZFile.getNextEntry();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            sevenZFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return retval;

    }


    public static void saveBitmapToFile(Bitmap bmp, String filename) {
        FileUtils.removeFile(filename);
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(filename);
            bmp.compress(Bitmap.CompressFormat.PNG, AndroidDefaults.IMAGE_IDEALJPEGCOMPRESS, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static boolean writeIntoBinaryFile(String fileName, byte[] bytesToWrite) {

        try {

            File filehandle = new File(fileName);
            FileOutputStream fout = new FileOutputStream(fileName, false);
            if (!filehandle.exists()) {
                filehandle.createNewFile();
            }
            try {
                int index = 0;
                int size = 1024;
                do {
                    if ((index + size) > bytesToWrite.length) {
                        size = bytesToWrite.length - index;
                    }
                    fout.write(bytesToWrite, index, size);
                    index += size;
                    fout.flush();
                } while (index < bytesToWrite.length);
            } catch (Exception exf) {
                exf.printStackTrace();
            }
            fout.close();

            return true;
        } catch (Exception e) {
        }
        return false;
    }


    public static String readLineFromFile(String filename, int lineNo) {
        String retString = "";
        try {
            RandomAccessFile fconn = new RandomAccessFile(filename, "r");
            int linenoRead = 1;
            while ((retString = fconn.readLine()) != null) {
                if (linenoRead == lineNo) {
                    break;
                }
                linenoRead++;
            }

            fconn.close();

        } catch (Exception ex2) {
        }

        return retString;
    }

    public static int getNumberOfLinesInFile(String filename) {
        int retVal = 0;
        try {
            RandomAccessFile fconn = new RandomAccessFile(filename, "r");
            String retString = "";
            while ((retString = fconn.readLine()) != null) {

                retVal++;
            }

            fconn.close();

        } catch (Exception ex2) {
        }

        return retVal;
    }


    public static int getLineNoWhichStartsWith(String filename, String startChars) {
        int retLineNo = 0;
        String line = "";

        try {
            RandomAccessFile fconn = new RandomAccessFile(filename, "r");
            while ((line = fconn.readLine()) != null) {
                retLineNo++;
                if (line.startsWith(startChars)) {
                    fconn.close();
                    return retLineNo;
                }
            }

            fconn.close();

        } catch (Exception ex2) {
        }

        return Values.INVALID_INT;
    }

    public static String readFirstValidLineFromFile(String filename) {
        String retString = "";
        try {
            RandomAccessFile fconn = new RandomAccessFile(filename, "r");
            while ((retString = fconn.readLine()) != null) {
                if (StringUtils.stringIsNullOrEmpty(retString) || retString.trim().startsWith("#")) {
                    continue;
                }

                break;
            }

            fconn.close();

        } catch (Exception ex2) {
        }

        return retString;
    }

    public static List<String> readPasscodeFile(Context context, String filename) {
        List<String> returnList = new ArrayList<>();
        try {
            //AssetFileDescriptor descriptor = context.getAssets().openFd("passcode.mp3");
            //FileReader FR = new FileReader(descriptor.getFileDescriptor());
            //BufferedReader BR = new BufferedReader(new InputStreamReader(in));

            InputStream is = context.getAssets().open("passcodefile.mp3");
            BufferedReader BR = new BufferedReader(new InputStreamReader(is));

            String item = "";
            while ((item = BR.readLine()) != null) {
                returnList.add(item);
            }

            is.close();
            BR.close();

        } catch (Exception ex2) {
            ex2.printStackTrace();
        }

        return returnList;
    }

    public static void copyAssetsFileToStorage(Context context, String inputfilename, String outputfilename) {
        try {
            InputStream is = context.getAssets().open(inputfilename);
            OutputStream out = new FileOutputStream(outputfilename);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = is.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }

            is.close();
            out.flush();
            out.close();
            out = null;
            is = null;

        } catch (Exception ex2) {

            ex2.printStackTrace();
        }

    }


    public static void removeDirectoryComplete(String dirname) {
        deleteRecursive(new File(dirname));
    }


    public static void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }


    static public List<String> getAllFilesWithinDirectory(String dirname) {
        List<String> retList = new ArrayList<String>();

        File dirhandle = new File(dirname);

        if (dirhandle.isDirectory()) {
            for (File child : dirhandle.listFiles()) {
                if (child.isFile()) {
                    retList.add(child.getAbsolutePath());
                }
            }
        }

        return retList;
    }


    public static boolean checkIfFileIsValid(String filepath) {
        if (StringUtils.stringIsNullOrEmpty(filepath)) {
            return false;
        }

        if (!fileExists(filepath)) {
            return false;
        }

        if (isFileEmpty(filepath)) {
            return false;
        }
        if (getFileSize(filepath) <= 0) {
            return false;
        }

        return true;
    }


}
