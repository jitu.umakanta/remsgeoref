package lib.utils.image;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;

import configuration.AndroidDefaults;
import lib.utils.common.Wlog;

/**
 * Created by sumeendranath on 05/10/17.
 */

public class BitmapUtils {


    public static Bitmap drawText(Bitmap originalBitmap, String text, int x, int y)
    {
        Wlog.i("writing on image " + text + " at " + x + "," + y);
        return drawText(originalBitmap,text, Color.BLACK, AndroidDefaults.SIGNATURESTROKEWIDTH,x,y,AndroidDefaults.EMBEDTEXTSIZE);
    }


    public static Bitmap drawText(Bitmap originalBitmap, String text, int pencolor, int strokewidth, int x, int y, float textsize)
    {
        Canvas canvas = new Canvas(originalBitmap);

        Paint paint = new Paint();
        paint.setTextSize(textsize);
        paint.setColor(pencolor); // Text Color
        paint.setStrokeWidth(strokewidth); // Text Size
        //paint.setShadowLayer(1f, 0, 0, Color.WHITE);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)); // Text Overlapping Pattern

        // some more settings...
        canvas.drawBitmap(originalBitmap, 0, 0, paint);

        for (String line: text.split("\n"))
        {
            canvas.drawText(line, x, y, paint);
            y += -paint.ascent() + paint.descent();
        }

        return originalBitmap;
    }


}
