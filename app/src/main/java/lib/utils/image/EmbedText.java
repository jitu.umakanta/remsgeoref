package lib.utils.image;

import android.content.Context;

import lib.utils.common.Wlog;
import lib.utils.display.DisplayUtils;
import projectspecificactivities.opdreport.PatientOpdReportActivity;

/**
 * Created by sumeendranath on 05/10/17.
 */

public class EmbedText {


    int IMAGEWIDTH = 0;
    int IMAGEHEIGHT = 0;
    int TOPPADDINGPARAM = 46; /// in percentage
    int LEFTPADDINGPARAM = 10; /// in percentage
    int ONELINEPARAM = 22; /// divide height by ONELINE
    int ONEPARTPARAM = 10; /// divide height by ONEPARTPARAM

    public EmbedText(Context cnt) {
        IMAGEHEIGHT = (int) DisplayUtils.getDisplayHeightinDp(cnt);
        IMAGEWIDTH = IMAGEHEIGHT * 2/3;
        Wlog.i("signaturwidth = " + IMAGEWIDTH + ", height = " + IMAGEHEIGHT);


    }

    void initImageDimens()
    {
    }

    int toppadding()
    {
        return IMAGEHEIGHT * TOPPADDINGPARAM / 100;
    }

    int leftpadding()
    {
        return IMAGEHEIGHT * LEFTPADDINGPARAM / 100;
    }

    int onelineheight()
    {
        return IMAGEHEIGHT  / ONELINEPARAM;
    }

    int onepartwidth()
    {
        return IMAGEWIDTH  / ONEPARTPARAM;
    }


    public int gy(int lineno)
    {
        initImageDimens();

        return toppadding() + lineno * onelineheight();
    }

    public int gx(int part)
    {
        initImageDimens();

        return leftpadding() + part * onepartwidth();
    }

}
