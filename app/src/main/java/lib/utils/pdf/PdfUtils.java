package lib.utils.pdf;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.pdf.PdfDocument;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import configuration.AndroidDefaults;

/**
 * Created by sumeendranath on 10/10/17.
 */

public class PdfUtils {

    public static int MARGINX = AndroidDefaults.PDF_MARGINX;
    public static int MARGINY = AndroidDefaults.PDF_MARGINY;


    public static void convertImagesToPdf(List<String> imagepaths, String outputpath, int pagewidth, int pageheight)
    {
        PdfDocument document = new PdfDocument();

//Create an A4 sized page 595 x 842 in Postscript points.
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder( pagewidth, pageheight,1).create();


        for(int i=0;i<imagepaths.size();i++) {
            PdfDocument.Page page = document.startPage(pageInfo);
            Canvas canvas = page.getCanvas();

            Bitmap pagetoinsert = BitmapFactory.decodeFile(imagepaths.get(i));

            canvas.drawBitmap(pagetoinsert, MARGINX, MARGINY, null);

            document.finishPage(page);
        }

        File outputpdf = new File(outputpath);
        try {
            outputpdf.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(outputpdf);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            document.writeTo(fOut);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // close the document
        document.close();



    }

}
