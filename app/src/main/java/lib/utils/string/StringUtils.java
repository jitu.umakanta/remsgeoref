package lib.utils.string;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class StringUtils {

    public static String getLastPartOfCsvString(String str, String sep, int lastbutoneplace)
    {

        String escaped = escapeSpecialRegexChars(sep);

        String retstr = "";
        try {

            String splitstr[] = str.split(escaped);

            retstr= splitstr[splitstr.length -1 - lastbutoneplace];

        }
        catch ( Exception ex)
        {
            ex.printStackTrace();
        }
        return retstr;

    }

    static Pattern SPECIAL_REGEX_CHARS = Pattern.compile("[{}()\\[\\].+*?^$\\\\|]");


    public static String escapeSpecialRegexChars(String str) {

        return SPECIAL_REGEX_CHARS.matcher(str).replaceAll("\\\\$0");
    }


    static public boolean stringIsNullOrEmpty(String text)
    {

        if(text == null ||text.length() <= 0  ||  text == "" )
        {
            return true;
        }
        else
        {
            return false;
        }

    }


    public static List<String> split(String original, String separator) {
        List<String> nodes = new ArrayList<>();
        // Parse nodes into vector
        int index = original.indexOf(separator);
        while (index >= 0) {
            nodes.add(original.substring(0, index));
            original = original.substring(index + separator.length());
            index = original.indexOf(separator);
        }
        // Get the last node
        nodes.add(original);

        return nodes;
    }





}
