package lib.vars;

/**
 * Created by sumeendranath on 27/08/17.
 */

public class Values {

    public static enum RUNMODE {
        DEBUG,
        RELEASE,
        DEBUG_QUIET,
        RELEASE_VERBOSE,
        RELEASE_VERBOSE_DUMMYSESSION,

    }

    public static enum STORAGETYPE {
        INTERNAL,
        EXTERNAL,
        MEMORY
    }


    public static enum FILESTREAMTYPE {

        TEXT,
        COMPRESSED,
        IMAGE,
        NONE
    }


    public static Integer INVALID_INT = -90909090;
    public static Double INVALID_DOUBLE = -90909090.0d;
    public static String INVALID_STRING = "";



    public static Double INVALID_GPS_DOUBLE = -1d;
    public static String INVALID_GPS_STRING = "-1,-1";


    public static Double MAX_PROGRESS_VALUE = 100d;

}
