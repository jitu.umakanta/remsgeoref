package notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.whitesun.sparshhospital.R;

import projectspecificactivities.login.LoginActivity;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Lenovo on 14-11-2017.
 */

public class Notification {

    Context context;

    public Notification(Context context) {
        this.context = context;
    }


    public  void Notification() {
        // Set Notification Title
        String strtitle = "notificationtitle";
        // Set Notification Text
        String strtext = "notificationtext";

        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(context, NotificationView.class);
        // Send data to NotificationView Class
        intent.putExtra("title", strtitle);
        intent.putExtra("text", strtext);
        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        //Create Notification using NotificationCompat.Builder
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                // Set Icon
                .setSmallIcon(R.drawable.ic_launcher_background)
                // Set Ticker Message
                .setTicker("notificationticker")
                // Set Title
                .setContentTitle("notificationtitle")
                // Set Text
                .setContentText("notificationtext")
                // Add an Action Button below Notification
                .addAction(R.drawable.aar_ic_pause, "Action Button", pIntent)
                // Set PendingIntent into Notification
                .setContentIntent(pIntent)
                // Dismiss Notification
                .setAutoCancel(true);

        // Create Notification Manager
        NotificationManager notificationmanager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(0, builder.build());

    }

}
