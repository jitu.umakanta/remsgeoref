package offlinemodesave;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import offlinemodesave.MyService;

public class MyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        //if there is change in network state then it will call the service to uploading the user answer to server
        Toast.makeText(context, "internet came now uploading will start", Toast.LENGTH_SHORT).show();
        Intent intent1 = new Intent(context, MyService.class);
        context.startService(intent1);

    }
}
