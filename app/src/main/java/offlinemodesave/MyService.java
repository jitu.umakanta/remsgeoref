package offlinemodesave;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.File;

import lib.rest.HttpUpload;
import lib.utils.common.Wlog;
import offlinemodesave.Offline;
import offlinemodesave.Offline1;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import static android.content.ContentValues.TAG;

public class MyService extends Service {
    String mediaPath;

    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d("aa", "MyService: " + "onStartCommand: " + "[intent, flags, startId]: " + "value: ");
        if (isNetworkAvailable() == true) {
            myUpload();
        } else {
            Toast.makeText(this, "network not available", Toast.LENGTH_SHORT).show();
        }
        return START_STICKY;
    }

    /**
     * uploading the answer to the server without internt
     */
    void myUpload() {

        //without image file
        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("answerwithoutimagepr", MODE_PRIVATE);
        String connectionsJSONString1 = pref1.getString("answerwithoutimage", null);
        Offline[] example = new Gson().fromJson(connectionsJSONString1, Offline[].class);
        try {
            for (int j = 0; j < example.length; j++) {
                String Userid = example[j].getUserid();
                String ChecklistID = example[j].getChecklistID();
                String QuestionId = example[j].getQuestionId();
                String ProjectLocationDefId = example[j].getProjectLocationDefId();
                String Answer = example[j].getAnswer();
                String AnswerNumber = example[j].getAnswerNumber();
                String AnswerFilePath = example[j].getAnswerFilePath();
                String FormCount = example[j].getFormCount();

                uploadUserAnswerWithoutImage(Integer.parseInt(Userid), Integer.parseInt(ChecklistID), Integer.parseInt(QuestionId), Integer.parseInt(ProjectLocationDefId), Answer, AnswerNumber, AnswerFilePath, FormCount);

            }
        } catch (Exception e) {

        }
        //with image file
        SharedPreferences pref11 = getApplicationContext().getSharedPreferences("answerwithimagepr", MODE_PRIVATE);
        String connectionsJSONString11 = pref11.getString("answerwithimage", null);
        Offline1[] example1 = new Gson().fromJson(connectionsJSONString11, Offline1[].class);
        try {
            for (int j = 0; j < example1.length; j++) {
                String Userid = example1[j].getUserid();
                String ChecklistID = example1[j].getChecklistID();
                String QuestionId = example1[j].getQuestionId();
                String ProjectLocationDefId = example1[j].getProjectLocationDefId();
                String Answer = example1[j].getAnswer();
                String AnswerNumber = example1[j].getAnswerNumber();
                String ImagePath = example1[j].getImagePath();
                String FormCount = example1[j].getFormCount();

                uploadUserAnswerWithImage(Integer.parseInt(Userid), Integer.parseInt(ChecklistID), Integer.parseInt(QuestionId), Integer.parseInt(ProjectLocationDefId), Answer, AnswerNumber, ImagePath, FormCount);

            }
        } catch (Exception e) {

        }
    }

    /**
     * @param getUserid
     * @param ChecklistID
     * @param QuestionId
     * @param ProjectLocationDefId
     * @param Answer
     * @param AnswerNumber
     * @param sAnswerFilePath
     * @param FormCountt
     */
    private void uploadUserAnswerWithoutImage(int getUserid, int ChecklistID, int QuestionId, int ProjectLocationDefId, String Answer, String AnswerNumber, String sAnswerFilePath, String FormCountt) {
        Log.d(TAG, "uploadUserAnswerWithoutImage: " + "getUserid: " + getUserid + "ChecklistID: " + ChecklistID + "QuestionId: " + QuestionId + "ProjectLocationDefId: " + ProjectLocationDefId + "Answer: " + Answer + "AnswerNumber: " + AnswerNumber + "sAnswerFilePath: " + sAnswerFilePath + "FormCountt: " + FormCountt);
        //sending user answer without image
        new HttpUpload<ResponseBody>() {
            @Override
            protected void onComplete(ResponseBody response) {
                super.onComplete(response);

                System.out.println("response: " + response);

            }

            @Override
            protected void onError(String message) {
                super.onError(message);
                Wlog.i("received error !!" + message);
                Toast.makeText(getApplicationContext(), "Issue in updating the record : " + message, Toast.LENGTH_SHORT).show();
            }

        }.httpConnect(HttpUpload.APIS.setuseranswer(getUserid, ChecklistID, QuestionId, ProjectLocationDefId, Answer, AnswerNumber, sAnswerFilePath, FormCountt));


    }

    /**
     * @param getUserid
     * @param ChecklistID
     * @param QuestionId
     * @param ProjectLocationDefId
     * @param Answer
     * @param AnswerNumber
     * @param imagePath
     * @param FormCountt
     */
    private void uploadUserAnswerWithImage(int getUserid, int ChecklistID, int QuestionId, int ProjectLocationDefId, String Answer, String AnswerNumber, String imagePath, String FormCountt) {

        Log.d(TAG, "uploadUserAnswerWithImage: " + "getUserid: " + getUserid + "ChecklistID: " + ChecklistID + "QuestionId: " + QuestionId + "ProjectLocationDefId: " + ProjectLocationDefId + "Answer: " + Answer + "AnswerNumber: " + AnswerNumber + "imagePath: " + imagePath + "FormCountt: " + FormCountt);

        //sending user answer with image
        mediaPath = imagePath;
        File file = new File(mediaPath);
        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("answer_filepath", file.getName(), requestBody);
        //giving the name to file
        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());

        //converting string to requestbody
       /* RequestBody user_def_id = RequestBody.create(okhttp3.MultipartBody.FORM, "" + _logpref.userID().get());
        RequestBody check_list = RequestBody.create(okhttp3.MultipartBody.FORM, "" + _logpref.checklistID().get());
        RequestBody uplr_id = RequestBody.create(okhttp3.MultipartBody.FORM, "" + _logpref.projectLocationDefID().get());
        RequestBody question_def_id = RequestBody.create(okhttp3.MultipartBody.FORM, "" + _logpref.questionGroupId().get());*/

        RequestBody Userid = RequestBody.create(okhttp3.MultipartBody.FORM, "" + getUserid);
        RequestBody check_list1 = RequestBody.create(okhttp3.MultipartBody.FORM, "" + ChecklistID);
        RequestBody QuestionId1 = RequestBody.create(okhttp3.MultipartBody.FORM, "" + QuestionId);
        RequestBody ProjectLocationDefId1 = RequestBody.create(okhttp3.MultipartBody.FORM, "" + ProjectLocationDefId);
        RequestBody Answer1 = RequestBody.create(okhttp3.MultipartBody.FORM, "" + Answer);
        RequestBody AnswerNumber1 = RequestBody.create(okhttp3.MultipartBody.FORM, "" + AnswerNumber);
        RequestBody FormCountt1 = RequestBody.create(okhttp3.MultipartBody.FORM, "" + FormCountt);
        new HttpUpload<ResponseBody>() {
            @Override
            protected void onComplete(ResponseBody response) {
                super.onComplete(response);


            }

            @Override
            protected void onError(String message) {
                super.onError(message);
                Wlog.i("received error !!" + message);
                Toast.makeText(getApplicationContext(), "Issue in updating the record : " + message, Toast.LENGTH_SHORT).show();
            }

        }.httpConnect(HttpUpload.APIS.setuseranswerimage(Userid, check_list1, QuestionId1, ProjectLocationDefId1, Answer1, AnswerNumber1, fileToUpload, FormCountt1));

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
