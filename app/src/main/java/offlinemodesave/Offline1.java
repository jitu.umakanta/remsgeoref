package offlinemodesave;

/**
 * Created by Lenovo on 24-11-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Offline1 {

    @SerializedName("Answer")
    @Expose
    private String answer;
    @SerializedName("AnswerNumber")
    @Expose
    private String answerNumber;
    @SerializedName("ChecklistID")
    @Expose
    private String checklistID;
    @SerializedName("FormCount")
    @Expose
    private String formCount;
    @SerializedName("ImagePath")
    @Expose
    private String imagePath;
    @SerializedName("ProjectLocationDefId")
    @Expose
    private String projectLocationDefId;
    @SerializedName("QuestionId")
    @Expose
    private String questionId;
    @SerializedName("Userid")
    @Expose
    private String userid;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswerNumber() {
        return answerNumber;
    }

    public void setAnswerNumber(String answerNumber) {
        this.answerNumber = answerNumber;
    }

    public String getChecklistID() {
        return checklistID;
    }

    public void setChecklistID(String checklistID) {
        this.checklistID = checklistID;
    }

    public String getFormCount() {
        return formCount;
    }

    public void setFormCount(String formCount) {
        this.formCount = formCount;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getProjectLocationDefId() {
        return projectLocationDefId;
    }

    public void setProjectLocationDefId(String projectLocationDefId) {
        this.projectLocationDefId = projectLocationDefId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
