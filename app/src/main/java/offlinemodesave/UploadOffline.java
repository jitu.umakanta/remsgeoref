package offlinemodesave;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import lib.rest.HttpUpload;
import lib.utils.common.Wlog;
import okhttp3.ResponseBody;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Lenovo on 23-11-2017.
 */

public class UploadOffline extends Activity {



    public void uploadOffline(){
        String connectionsJSONString = getPreferences(MODE_PRIVATE).getString("KEY_CONNECTIONS", null);

        Offline[] example = new Gson().fromJson(connectionsJSONString, Offline[].class);
        Log.d(TAG, "onClick: "+example);
        for(int j=0;j<10;j++) {
            String Userid=example[j].getUserid();
            String ChecklistID=example[j].getChecklistID();
            String QuestionId=example[j].getQuestionId();
            String ProjectLocationDefId=example[j].getProjectLocationDefId();
            String Answer=example[j].getAnswer();
            String AnswerNumber=example[j].getAnswerNumber();
            String AnswerFilePath=example[j].getAnswerFilePath();
            String FormCount=example[j].getFormCount();


            uploadUserAnswerWithoutImage(Integer.parseInt(Userid), Integer.parseInt(ChecklistID), Integer.parseInt(QuestionId), Integer.parseInt(ProjectLocationDefId), Answer, AnswerNumber,AnswerFilePath,FormCount);



        }
    }


    /**
     *
     * @param getUserid
     * @param ChecklistID
     * @param QuestionId
     * @param ProjectLocationDefId
     * @param Answer
     * @param AnswerNumber
     * @param sAnswerFilePath
     * @param FormCountt
     */
    private void uploadUserAnswerWithoutImage(int getUserid, int ChecklistID, int QuestionId, int ProjectLocationDefId, String Answer, String AnswerNumber, String sAnswerFilePath, String FormCountt) {
        Log.d(TAG, "uploadUserAnswerWithoutImage: " + "getUserid: " + getUserid + "ChecklistID: " + ChecklistID + "QuestionId: " + QuestionId + "ProjectLocationDefId: " + ProjectLocationDefId + "Answer: " + Answer + "AnswerNumber: " + AnswerNumber + "sAnswerFilePath: " + sAnswerFilePath + "FormCountt: " + FormCountt);
        //sending user answer without image
        new HttpUpload<ResponseBody>() {
            @Override
            protected void onComplete(ResponseBody response) {
                super.onComplete(response);

                System.out.println("response: " + response);

            }

            @Override
            protected void onError(String message) {
                super.onError(message);
                Wlog.i("received error !!" + message);
                Toast.makeText(getApplicationContext(), "Issue in updating the record : " + message, Toast.LENGTH_SHORT).show();
            }

        }.httpConnect(HttpUpload.APIS.setuseranswer(getUserid, ChecklistID, QuestionId, ProjectLocationDefId, Answer, AnswerNumber, sAnswerFilePath, FormCountt));

    }



}
