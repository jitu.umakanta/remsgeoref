package offlinemodesave;

/**
 * Created by Lenovo on 23-11-2017.
 */

public class UploadUserAnswerWithoutImageOffline {
    String Userid,ChecklistID,QuestionId,ProjectLocationDefId,Answer,AnswerNumber,AnswerFilePath,FormCount,ImagePath;

    public UploadUserAnswerWithoutImageOffline(String Userid, String ChecklistID, String QuestionId, String ProjectLocationDefId, String Answer, String AnswerNumber, String AnswerFilePath, String FormCountt) {

        this.Userid=Userid;
        this.ChecklistID=ChecklistID;
        this.QuestionId=QuestionId;
        this.ProjectLocationDefId=ProjectLocationDefId;
        this.Answer=Answer;
        this.AnswerNumber=AnswerNumber;
        this.AnswerFilePath=AnswerFilePath;
        this.FormCount=FormCountt;

    }

    public String getUserid() {
        return Userid;
    }

    public void setUserid(String userid) {
        Userid = userid;
    }

    public String getChecklistID() {
        return ChecklistID;
    }

    public void setChecklistID(String checklistID) {
        ChecklistID = checklistID;
    }

    public String getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(String questionId) {
        QuestionId = questionId;
    }

    public String getProjectLocationDefId() {
        return ProjectLocationDefId;
    }

    public void setProjectLocationDefId(String projectLocationDefId) {
        ProjectLocationDefId = projectLocationDefId;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }

    public String getAnswerNumber() {
        return AnswerNumber;
    }

    public void setAnswerNumber(String answerNumber) {
        AnswerNumber = answerNumber;
    }

    public String getAnswerFilePath() {
        return AnswerFilePath;
    }

    public void setAnswerFilePath(String answerFilePath) {
        AnswerFilePath = answerFilePath;
    }

    public String getFormCount() {
        return FormCount;
    }

    public void setFormCount(String formCount) {
        FormCount = formCount;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }
}
