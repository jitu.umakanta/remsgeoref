package projectspecific;

import android.app.Activity;
import android.content.Context;

import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder;
import cafe.adriel.androidaudiorecorder.model.AudioChannel;
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate;
import cafe.adriel.androidaudiorecorder.model.AudioSource;
import configuration.AndroidDefaults;
import lib.utils.file.FileExternal;

/**
 * Created by sumeendranath on 03/09/17.
 */

public class AudioRecord {

    String _filepath = "";
    String _uniqueId = "";

    public void start(Context context,int requestCode, String uniqueid, int backgroundcolor)
    {
        _uniqueId = uniqueid;
        String filePath = FileExternal.getAudioTempDir() + FileExternal.getUploadFileName(uniqueid,AndroidDefaults.TAG_AUDIO  , AndroidDefaults.EXT_AUDIO);
        AndroidAudioRecorder.with((Activity) context)
                // Required
                .setFilePath(filePath)
                .setColor(backgroundcolor)
                .setRequestCode(requestCode)
                // Optional
                .setSource(AudioSource.MIC)
                .setChannel(AudioChannel.MONO)
                .setSampleRate(AudioSampleRate.HZ_11025)
                .setAutoStart(true)
                .setKeepDisplayOn(true)
                // Start recording
                .record();


         _filepath = filePath;



    }

    public String getFilePath()
    {
        return _filepath;
    }
    public String getUniqueId()
    {
        return _uniqueId;
    }

}
