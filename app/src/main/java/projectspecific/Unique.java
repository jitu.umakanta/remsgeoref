package projectspecific;

import java.util.Calendar;

import configuration.defaults;
import lib.utils.date.DateUtils;
import lib.utils.string.RandomString;

/**
 * Created by sumeendranath on 03/09/17.
 */

public class Unique {

    public static String getUniqueDataId()
    {
        return getAlphaUniqueID();
    }

    static public String getAlphaUniqueID()
    {
        String retValue = "";

        RandomString RS = new RandomString(3);

        Calendar c1 = Calendar.getInstance();

        int year = c1.get(Calendar.YEAR);
        int day = c1.get(Calendar.DAY_OF_MONTH);
        int month = c1.get(Calendar.MONTH) + 1;
        int hour = c1.get(Calendar.HOUR_OF_DAY);
        int minute = c1.get(Calendar.MINUTE);
        int second = c1.get(Calendar.SECOND);


        retValue = DateUtils.convertNumberTodateTimeFormat(year) +
                DateUtils.convertNumberTodateTimeFormat(month) +
                DateUtils.convertNumberTodateTimeFormat(day) +
                defaults.UPLOADFILE_SEP +
                DateUtils.convertNumberTodateTimeFormat(hour) +
                DateUtils.convertNumberTodateTimeFormat(minute) +
                DateUtils.convertNumberTodateTimeFormat(second) +
                defaults.UPLOADFILE_SEP +
                RS.nextString();


        return retValue;
    }

}
