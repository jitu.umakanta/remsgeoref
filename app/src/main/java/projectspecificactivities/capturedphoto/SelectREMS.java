package projectspecificactivities.capturedphoto;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.databinding.library.baseAdapters.BR;
import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;
import com.whitesun.sparshhospital.R;
import com.whitesun.sparshhospital.databinding.ActivitySelectDepartmentBinding;
import com.whitesun.sparshhospital.databinding.ActivitySelectRemsBinding;
import com.whitesun.sparshhospital.databinding.ItemHospitalSelectBinding;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import generic.activities.CameraPhotoActivity;
import httpdata.models.android.PhotoGallery;
import httpdata.projectspecificmodels.CrudModel;
import httpdata.projectspecificmodels.GpsData;
import httpdata.projectspecificmodels.PhotoPoint;
import lib.gps.GpsDistance;
import lib.gps.GpsLocation;
import lib.gps.GpsUtils;
import lib.rest.HttpCall;
import lib.rest.HttpUpload;
import lib.ui.ProgDialog;
import lib.utils.common.Wlog;
import lib.utils.datastructures.Wto;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import projectspecificactivities.sharedpref.LoginPreferences_;

@Fullscreen
@EActivity
public class SelectREMS extends Activity {

    ActivitySelectRemsBinding vars;

    ObservableArrayList<PhotoPoint> photopointcards = new ObservableArrayList<>();

    @Pref
    LoginPreferences_ _loginpref;



    List<GpsData> listofgpsdata = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        handleclicks();
        handleMenu();
        execute();
    }


    int _selectedphotoid = -1;
    String _selecteduserid = "456789";
    String _selectedlat = "";
    String _selectedlon = "";

    void init()
    {
        vars = DataBindingUtil.setContentView(this,R.layout.activity_select_rems);

        ItemType<ItemHospitalSelectBinding> itemtype = new ItemType<ItemHospitalSelectBinding>(R.layout.item_hospital_select) {
            @Override
            public void onBind(@NotNull final Holder<ItemHospitalSelectBinding> holder) {

                if(Wto.Double(holder.getBinding().getCarditem().distance) > 300)
                {
                    holder.getBinding().btSelect.setVisibility(View.INVISIBLE);
                    holder.getBinding().dist.setText("DISTANCE : " + holder.getBinding().getCarditem().distance + " Meters");
                }

                holder.getBinding().btSelect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //sendDataToActivity(holder.getBinding().getCarditem());

                        ProgDialog.show(SelectREMS.this,"Capturing GPS",true);

                       final GpsLocation gps =  new GpsLocation(SelectREMS.this)
                        {
                            @Override
                            protected void onLocationChange(Location loc, Double distancefromprevloc, String gpsstring) {
                                super.onLocationChange(loc, distancefromprevloc, gpsstring);

                                this.stop();

                                _selectedphotoid = holder.getBinding().getCarditem().id;
                                _selecteduserid = _loginpref.passcode().getOr("456789");
                                _selectedlat = loc.getLatitude() + "";
                                _selectedlon = loc.getLongitude() + "";



                                CameraPhotoActivity.startBackCamera(SelectREMS.this,1234);

                            }
                        };


                       // SelectConsultantActivity_.intent(SelectDepartment.this).start();
                    }
                });
            }
        };

        new LastAdapter(photopointcards, BR.carditem)
                .map(PhotoPoint.class,itemtype)
                .into(vars.rvhospitallist);


        getGpsLocation();


        vars.btrefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadhospitals();
            }
        });

        vars.btrems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadhospitals();
            }
        });

    }

    String currentlat = "";
    String currentlon = "";


    void getGpsLocation()
    {
        final GpsLocation gps =  new GpsLocation(SelectREMS.this)
        {
            @Override
            protected void onLocationChange(Location loc, Double distancefromprevloc, String gpsstring) {
                super.onLocationChange(loc, distancefromprevloc, gpsstring);

                this.stop();

                currentlat = loc.getLatitude() + "";
                currentlon = loc.getLongitude() + "";

                Wlog.i("curr lat = " + currentlat);
                downloadgpsinfo();




            }
        };

    }


    void uploadPhotoData(int photopointid,String user_id, final String filename, String gps_lat, String gps_lon)
    {
        File file = new File(filename);

        Wlog.i("Uploading for id " + photopointid + " : " + file.getName());



        MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));

        RequestBody up_photopointid =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, "" + photopointid);


        RequestBody up_user_id =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, "" + user_id);

        RequestBody up_gps_lat =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, "" + gps_lat);

        RequestBody up_gps_lon =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, "" + gps_lon);


        new HttpUpload<ResponseBody>()
        {
            @Override
            protected void onComplete(ResponseBody response) {
                super.onComplete(response);

                try {
                    //Wlog.i("Success !!" + response.string());
                    if(response.string().toLowerCase().contains("success"))
                    {
                        Wlog.i("SUCCESS!!!");
                    }

                    //SelectWaitingPatientActivity.start(PatientOpdReportActivity.this);
                    finish();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            protected void onError(String message) {
                super.onError(message);
                Wlog.i("received error !!" + message);
                Toast.makeText(getApplicationContext(), "Issue in updating the record : " + message, Toast.LENGTH_SHORT).show();
            }

        }.httpConnect(HttpUpload.APIS.uploadphoto(up_photopointid,up_user_id,filePart,up_gps_lat,up_gps_lon));

    }


    void handleclicks()
    {

    }
    void handleMenu()
    {

    }
    void execute()
    {

    }

    @UiThread
    void fillphotopoints(List<PhotoPoint> photoPoints)
    {
        photopointcards.clear();
        for(int i=0;i<photoPoints.size();i++) {
            // Wlog.i("hospital : "   + response.data.get(i).name) ;

            if(photoPoints.get(i).department_id.contains(_loginpref.department().getOr(""))) {

                GpsData gd = getMatchingGpsData(photoPoints.get(i));
                if(gd != null) {
                    Double  distance = GpsDistance.get(Wto.Double(currentlat), Wto.Double(currentlon), Wto.Double(gd.gps_lat),Wto.Double(gd.gps_lon));
                   // if(distance > 10000) {
                        photoPoints.get(i).distance = distance + "";
                        photopointcards.add(photoPoints.get(i));
                    //}
                }
                else
                {
                    //photoPoints.get(i).distance =   "NA";
                    //photopointcards.add(photoPoints.get(i));
                }
            }
        }

    }


    GpsData getMatchingGpsData(PhotoPoint pp)
    {
            for(int i=0;i<listofgpsdata.size();i++)
            {
                if(listofgpsdata.get(i).pp_id == pp.id)
                {
                    return listofgpsdata.get(i);
                }
            }

            return null;
    }
    void downloadhospitals()
    {
        new HttpCall<CrudModel<PhotoPoint>>()
        {
            @Override
            protected void onComplete(CrudModel<PhotoPoint> response) {
                super.onComplete(response);

                /*hospitalcards.clear();
                for(int i=0;i<response.data.size();i++) {
                   // Wlog.i("hospital : "   + response.data.get(i).name) ;

                    hospitalcards.add(response.data.get(i));
                }*/
                fillphotopoints(response.data);



            }
        }.httpConnect(HttpCall.APIS.getlistofphotopoints());
    }

    void downloadgpsinfo()
    {
        new HttpCall<CrudModel<GpsData>>()
        {
            @Override
            protected void onComplete(CrudModel<GpsData> response) {
                super.onComplete(response);

                listofgpsdata = response.data;

                downloadhospitals();


            }
        }.httpConnect(HttpCall.APIS.getlistofgpsdata());
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if(requestCode == 1234)
        {
            if(resultCode == Activity.RESULT_OK) {
                ArrayList<PhotoGallery> pglist =  data.getParcelableArrayListExtra(CameraPhotoActivity.EXTRA_RESULTPHOTOITEMS);

                // photogalleryitems.clear();

                for(int i=0;i<pglist.size();i++)
                {
                    //photogalleryitems.add(pglist.get(i));


                    uploadPhotoData(_selectedphotoid,_selecteduserid,pglist.get(i).imagepath,_selectedlat,_selectedlon);
                }

                Wlog.d("sizes = " + pglist.size() + "," + pglist.size());

            }
            else
            {
                Wlog.d("Camera operation is cancelled");

            }

            return;
        }

    }
}
