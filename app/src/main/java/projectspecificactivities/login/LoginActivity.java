package projectspecificactivities.login;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Toast;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.whitesun.sparshhospital.R;
import com.whitesun.sparshhospital.databinding.ActivityLoginBinding;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import configuration.DirectApiConfig;
import httpdata.projectspecificmodels.BreweryUsersLogin;
import httpdata.projectspecificmodels.CheckUserTask;
import httpdata.projectspecificmodels.CrudModel;
import httpdata.projectspecificmodels.Hospital;
import httpdata.projectspecificmodels.MobUsers;
import lib.assets.Passcode;
import lib.rest.HttpCall;
import lib.ui.ProgDialog;
import lib.ui.keyboard.WKey;
import lib.utils.datastructures.Ws;
import lib.utils.datautils.JsonUtil;
import lib.utils.date.DateUtils;
import projectspecificactivities.mobiledatadownload.MobileDataDownloadActivity_;
import projectspecificactivities.selectdepartment.SelectDepartment_;
import projectspecificactivities.selectlocation.SelectLocationActivity;
import projectspecificactivities.selectlocation.SelectLocationActivity_;
import projectspecificactivities.sharedpref.LoginPreferences_;


@Fullscreen
@EActivity
public class LoginActivity extends AppCompatActivity {

    /*
    public static void start(Context context)
    {
        Intent i = new Intent(context, LoginActivity_.class);

        context.startActivity(i);

    }
    */

    public static int REQCODE_DOWNLOADDATA = 2001;
    ActivityLoginBinding vars;

    @Pref
    LoginPreferences_ _logpref;

    List<MobUsers> listofmobusers = new ArrayList<>();
    List<BreweryUsersLogin> listofprojectusers = new ArrayList<>();
    List<CheckUserTask> listOfTasks = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        handleclicks();
        execute();
    }

    void init() {
        downloadAllMobileData(true);
        vars = DataBindingUtil.setContentView(this, R.layout.activity_login);
        vars.etpasscode.setSubmitButton(vars.btsubmit);
        vars.etpasscode.openKeyBoardAutomatically(LoginActivity.this);
        //downloadusers();

        //getting data from online mode
        //downloadProjectUsers();



       /* String names[]=new String[listofprojectusers.size()];
        for(int i=0;i<listofprojectusers.size();i++) {
            names[i]=listofprojectusers.get(i).name;
        }
        String[] str=new String[]{"one","two","three"};

        ArrayAdapter arrayAdapter= new ArrayAdapter(LoginActivity.this,R.layout.activity_login, names);
        arrayAdapter.setDropDownViewResource(android.R.layout.activity_list_item);
        vars.selectUser.setAdapter(arrayAdapter);*/

    }


    void handleclicks() {
        vars.btrefreshmobdata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadAllMobileData(true);
            }
        });
        vars.btsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //getting data from offline
                getOfflineUsersData();

                if (listofprojectusers.size() < 1) {
                    Toast.makeText(LoginActivity.this, "No Data Connection!", Toast.LENGTH_SHORT).show();
                }
                Ws passcode = Ws.from(vars.etpasscode.getText().toString());
                if (passcode.isNullOrEmpty() || passcode.len() < 4) {
                    Log.d("passcode", "" + vars.etpasscode.getText().toString());
                    vars.etpasscode.setError(getString(R.string.et_invalidpasscode));
                    vars.etpasscode.setText("");
                    return;
                }


                for (int i = 0; i < listofprojectusers.size(); i++) {
                    if (listofprojectusers.get(i).passcode == Integer.parseInt(vars.etpasscode.getText().toString())) {
                        _logpref.passcode().put(listofprojectusers.get(i).passcode + "");
                        _logpref.userID().put(listofprojectusers.get(i).id);
                        // _logpref..put(listofprojectusers.get(i).department);
                        _logpref.name().put(listofprojectusers.get(i).name);
                        // _logpref.department().put(""+listofprojectusers.get(i).id);

                        _logpref.lastlogintime().put(DateUtils.getCurrentDateTimeStringMySqlFormat());
                        Toast.makeText(LoginActivity.this, "Successful login", Toast.LENGTH_SHORT).show();
                        SelectLocationActivity_.intent(LoginActivity.this).start();
                        // MobileDataDownloadActivity_.intent(LoginActivity.this).start();
                       /* if(!checkUsersTask())
                        {


                        }else {
                            SelectLocationActivity_.intent(LoginActivity.this).start();
                        }*/
                        closeAct();
                        return;
                    } else {
                        // Toast.makeText(LoginActivity.this, "noooo", Toast.LENGTH_SHORT).show();
                    }
                }
                //SelectLocationActivity_.intent(LoginActivity.this).start();
                vars.etpasscode.setError(getString(R.string.et_invalidpasscode));
                vars.etpasscode.setText("");
                return;
            }
        });
    }

    void execute() {

    }

    void closeAct() {
        WKey.close(LoginActivity.this);
        finish();
    }


    void downloadusers() {

        ProgDialog.show(this, "Downloading user data", true);
        new HttpCall<CrudModel<MobUsers>>() {
            @Override
            protected void onComplete(CrudModel<MobUsers> response) {
                super.onComplete(response);

                ProgDialog.close();

                listofmobusers = response.data;

            }
        }.httpConnect(HttpCall.APIS.getlistofmobusers());
    }

    void downloadProjectUsers() {
        ProgDialog.show(this, "Downloading Your Details..!", true);
        new HttpCall<CrudModel<BreweryUsersLogin>>() {
            @Override
            protected void onComplete(CrudModel<BreweryUsersLogin> response) {
                super.onComplete(response);
                ProgDialog.close();
                listofprojectusers = response.data;
                Log.d("download_projecr_users", "" + response.toString());
            }

            @Override
            protected void onError(String message) {
                super.onError(message);

            }
        }.httpConnect(HttpCall.APIS.getlistofprojectusers());


    }

    /*

    boolean checkUsersTask()
    {
        ProgDialog.show(this,"Downloading user's data",true);
        new HttpCall<CrudModel<CheckUserTask>>()
        {
            @Override
            protected void onComplete(CrudModel<CheckUserTask> response) {
                super.onComplete(response);
                ProgDialog.close();
                listOfTasks = response.data;
            }

            @Override
            protected void onError(String message) {
                super.onError(message);

            }
        }.httpConnect(HttpCall.APIS.getusertask(_logpref.userID().get()));


       for(int i=0;i<listOfTasks.size();i++)
       {
           Date start_time = listOfTasks.get(i).start_time;
           Date last_submission_time = listOfTasks.get(i).last_submission_time;
           int period= listOfTasks.get(i).period;
           SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
           Date now = new Date();
           //int difference = now.compareTo(formatter.parse(last_submission_time));
*//*
* compare current time and last submission time if the difference is greater than period the invoke
* task activity
* *//*
       }
        return false;
    }
*/

    void downloadAllMobileData(boolean force)
    {

        MobileDataDownloadActivity_.start(this,REQCODE_DOWNLOADDATA, force);
    }


    void getOfflineUsersData() {
        String jsonstring = JsonUtil.getJsonStringFromInternalFile(LoginActivity.this, DirectApiConfig.DF_MOBUSERDEF);
        Log.d("aa", "LoginActivity: "+"getOfflineUsersData: "+"[]: "+"DF_MOBUSERDEF: "+jsonstring);
        //Type type = new TypeToken<CrudModel<MobUsers>>() {}.getType();
       // CrudModel<MobUsers> mobusers = new Gson().fromJson(jsonstring, type);
        //listofmobusers = mobusers.data;


        Type type1 = new TypeToken<CrudModel<BreweryUsersLogin>>() {}.getType();
        CrudModel<BreweryUsersLogin> projectusers = new Gson().fromJson(jsonstring, type1);
        listofprojectusers = projectusers.data;
    }
}
