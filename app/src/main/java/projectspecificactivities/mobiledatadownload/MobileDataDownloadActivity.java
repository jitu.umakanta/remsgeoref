package projectspecificactivities.mobiledatadownload;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


import com.whitesun.sparshhospital.R;
import com.whitesun.sparshhospital.databinding.ActivityMobileDataDownloadBinding;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.UiThread;

import configuration.DirectApiConfig;
import lib.rest.HttpCall;
import lib.utils.common.Wlog;
import lib.utils.file.FileUtils;
import lib.vars.Values;
import okhttp3.ResponseBody;
import projectspecificactivities.selectopdpatient.SelectWaitingPatientActivity_;


@EActivity
@Fullscreen
public class MobileDataDownloadActivity extends AppCompatActivity {


    private static String INTENTVAR_FORCE = "force";

    public static void start(Activity context, int requestcode, boolean force) {
        Intent starter = new Intent(context, MobileDataDownloadActivity_.class);
        starter.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        starter.putExtra(INTENTVAR_FORCE, force);
        context.startActivityForResult(starter, requestcode);
    }


    boolean _force = false;
    ActivityMobileDataDownloadBinding vars;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_mobile_data_download);

        init();
        handleclicks();
        execute();
    }

    void init() {
        vars = DataBindingUtil.setContentView(this, R.layout.activity_mobile_data_download);

        if (getIntent().hasExtra(INTENTVAR_FORCE)) {
            _force = getIntent().getBooleanExtra(INTENTVAR_FORCE, false);
        }
    }

    void handleclicks() {

    }

    void execute() {
        if (checkIfMobileDownloadFilesExist() && !_force) {
            nextAction();
        } else {
            displayFileDownloadStarted();
            background_downloadmobiledata();
        }
    }

    void nextAction() {
        finish();
    }

    void closeAct() {
        finish();
    }


    @Background
    void background_downloadmobiledata() {
        downloadMobileDataFromServer();
    }

    @UiThread
    void setFileDownloadProgress(Double progress) {
        //Wlog.d("progress : GOT IN UI progress as : " + progress);
        vars.squareprogressfileprocess.setProgress(progress);
    }

    @UiThread
    void setFileProcessProgress(Double progress) {
        vars.circleprogressdownload.setProgress(progress.intValue());
    }

    @UiThread
    void displayFileDownloadStarted() {
        vars.messagedisplay.setText(R.string.mobiledata_download);
        vars.squareprogressfileprocess.setText("Downloading");
    }

    @UiThread
    void displayFileProcessStarted() {
        vars.messagedisplay.setText(R.string.mobiledata_fileprocess);
        vars.squareprogressfileprocess.setText("Processing");
    }

    @UiThread
    void displayFileProcessingAsComplete() {
        vars.messagedisplay.setText(R.string.mobiledata_complete);
        vars.squareprogressfileprocess.setText("Complete!!");
        nextAction();
    }


    private boolean checkIfMobileDownloadFilesExist() {
        for (int i = 0; i < DirectApiConfig.DF_LIST_MOBILEDATAFILES.size(); i++) {
            if (!FileUtils.internalFileExists(MobileDataDownloadActivity.this, DirectApiConfig.DF_LIST_MOBILEDATAFILES.get(i))) {
                Wlog.i("internal files : " + DirectApiConfig.DF_LIST_MOBILEDATAFILES.get(i) + " NOT exist");
                return false;
            }
        }
        return true;
    }


    ///// FUNCTIONS /////
    void downloadMobileDataFromServer() {
        new HttpCall<ResponseBody>() {


            //// download is progressing
            @Override
            protected void onCompressedFileDownloadProgress(double progress) {
                super.onCompressedFileDownloadProgress(progress);
                Wlog.d("prog completed: " + progress);
                setFileDownloadProgress(progress);
            }

            //// download is complete
            @Override
            protected void onCompressedFileDownloadComplete() {
                super.onCompressedFileDownloadComplete();
                setFileDownloadProgress(Values.MAX_PROGRESS_VALUE);
                displayFileProcessStarted();
            }

            //// download file is getting processed
            @Override
            protected void onCompressedFileProcessProgress(double progress) {
                // Wlog.d("progress : GOT file processing as : " + progress);
                super.onCompressedFileProcessProgress(progress);
                setFileProcessProgress(progress);
            }

            //// download processing is complete
            @Override
            protected void onCompressedFileProcessComplete() {
                super.onCompressedFileProcessComplete();
                setFileProcessProgress(Values.MAX_PROGRESS_VALUE);
            }

            //// the whole process is complete .... RESPONSE BODY IS NULL for compressed downloads
            @Override
            protected void onComplete(ResponseBody response) {
                super.onComplete(response);

                displayFileProcessingAsComplete();
                Wlog.d(FileUtils.readFromInternalFile(MobileDataDownloadActivity.this, DirectApiConfig.DF_GETGPSDATA));

            }


            @Override
            protected void onError(String message) {
                super.onError(message);

            }
        }.httpConnect(HttpCall.APIS.getAllMobileData(), this, Values.FILESTREAMTYPE.COMPRESSED, DirectApiConfig.DF_ALLMOBILEINPUTCOMPRESSED);


    }


}
