package projectspecificactivities.opdreport;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.pdf.PdfDocument;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.whitesun.sparshhospital.R;
import com.whitesun.sparshhospital.databinding.ActivityPatientOpdReportBinding;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.parceler.Parcels;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import configuration.AndroidDefaults;
import httpdata.projectspecificmodels.OutpatientRecord;
import lib.rest.HttpUpload;
import lib.ui.image.WPhoto;
import lib.utils.common.Wlog;
import lib.utils.display.DisplayUtils;
import lib.utils.file.FileExternal;
import lib.utils.file.FileUtils;
import lib.utils.image.BitmapUtils;
import lib.utils.image.EmbedText;
import lib.utils.pdf.PdfUtils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import projectspecificactivities.selectopdpatient.SelectWaitingPatientActivity;
import projectspecificactivities.sharedpref.ConsultantPreferences_;

@Fullscreen
@EActivity
public class PatientOpdReportActivity extends AppCompatActivity {

    public static String EXTRA_OUTPATIENTRECORD = "outpatientrecord";


    ActivityPatientOpdReportBinding vars;

    OutpatientRecord _patientrecord;

    @Pref
    ConsultantPreferences_ _consultantpref;

    public static void start(Context context, OutpatientRecord patient)
    {
        Intent starter = new Intent(context, PatientOpdReportActivity_.class);
        starter.putExtra(EXTRA_OUTPATIENTRECORD, Parcels.wrap(patient));
        context.startActivity(starter);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        intentvars();
        init();
        handleclicks();
        handleMenu();
        execute();
    }



    void intentvars()
    {
        _patientrecord = Parcels.unwrap(getIntent().getExtras().getParcelable(EXTRA_OUTPATIENTRECORD));
    }


    int pagecount = 0;
    List<String> listofallimages = new ArrayList<>();

    void init()
    {
        vars = DataBindingUtil.setContentView(this,R.layout.activity_patient_opd_report);

        vars.reportsignaturepad.setSignatureBitmap(fillPatientDetails());


        vars.btsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(vars.reportsignaturepad.isEmpty())
                {
                    Wlog.d("Report is cancelled!!");
                    Toast.makeText(getApplicationContext(),"Empty Report !",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    pagecount++;

                    if(pagecount <2)
                    {
                        vars.btsubmit.setText(R.string.bt_next);
                    }
                    else
                    {
                        vars.btsubmit.setText(R.string.bt_submit);
                    }

                    if(pagecount ==1 ) {
                        String filepath = FileExternal.getSignatureTempDir() + FileExternal.getUploadFileName("SIG", AndroidDefaults.TAG_SIGNATURE, AndroidDefaults.EXT_SIGNATURE);
                        FileUtils.saveBitmapToFile(vars.reportsignaturepad.getSignatureBitmap(), filepath);

                        uploadFirstPage(_patientrecord.id, 2, filepath);
                        vars.reportsignaturepad.getSignatureBitmap().recycle();

                        listofallimages.add(filepath);
                        makePageReady(pagecount + 1);
                    }

                    if(pagecount == 2)
                    {

                        String filepath = FileExternal.getSignatureTempDir() + FileExternal.getUploadFileName("SIG" + pagecount, AndroidDefaults.TAG_SIGNATURE, AndroidDefaults.EXT_SIGNATURE);
                        FileUtils.saveBitmapToFile(vars.reportsignaturepad.getSignatureBitmap(), filepath);
                        vars.reportsignaturepad.getSignatureBitmap().recycle();

                        listofallimages.add(filepath);

                        makePageReady(pagecount + 1);

                    }

                    if(pagecount == 3)
                    {
                        String filepath = FileExternal.getSignatureTempDir() + FileExternal.getUploadFileName("SIG" + pagecount, AndroidDefaults.TAG_SIGNATURE, AndroidDefaults.EXT_SIGNATURE);
                        FileUtils.saveBitmapToFile(vars.reportsignaturepad.getSignatureBitmap(), filepath);
                        listofallimages.add(filepath);

                        String consolidatedreport = FileExternal.getSignatureTempDir() + FileExternal.getUploadFileName("SIGREPORT" , AndroidDefaults.TAG_SIGNATURE, AndroidDefaults.EXT_REPORT);

                        PdfUtils.convertImagesToPdf(listofallimages,consolidatedreport,AndroidDefaults.PDF_WIDTH, AndroidDefaults.PDF_HEIGHT);

                        uploadFinalReport(_patientrecord.id,3,consolidatedreport);

                    }

                }


            }
        });
    }
    void handleclicks()
    {

    }
    void handleMenu()
    {

    }
    void execute()
    {

    }



    void makePageReady(int page)
    {
        vars.reportsignaturepad.clear();
        vars.reportsignaturepad.setSignatureBitmap(getPage(page));
    }


    void uploadFinalReport(int idofrecord,int statustochange, final String filename)
    {
        File file = new File(filename);

        Wlog.i("Uploading for id " + idofrecord + " : " + file.getName());



        MultipartBody.Part filePart = MultipartBody.Part.createFormData("report", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));

        RequestBody idforupload =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, "" + idofrecord);


        RequestBody statusforupdate =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, "" + statustochange);


        new HttpUpload<ResponseBody>()
        {
            @Override
            protected void onComplete(ResponseBody response) {
                super.onComplete(response);

                try {
                    //Wlog.i("Success !!" + response.string());
                    if(response.string().toLowerCase().contains("success"))
                    {
                        for(int i=0;i<listofallimages.size();i++) {
                            Wlog.i("removing file : " + listofallimages.get(i));
                            FileUtils.removeFile(listofallimages.get(i));
                        }
                    }

                    SelectWaitingPatientActivity.start(PatientOpdReportActivity.this);
                    finish();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            protected void onError(String message) {
                super.onError(message);
                Wlog.i("received error !!" + message);
                Toast.makeText(getApplicationContext(), "Issue in updating the record : " + message, Toast.LENGTH_SHORT).show();
            }

        }.httpConnect(HttpUpload.APIS.uploadOpdReport(idforupload,statusforupdate,filePart));

    }



    void uploadFirstPage(int idofrecord,int statustochange, final String filename)
    {
        File file = new File(filename);

        Wlog.i("Uploading for id " + idofrecord + " : " + file.getName());



        MultipartBody.Part filePart = MultipartBody.Part.createFormData("imagepath", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));

        RequestBody idforupload =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, "" + idofrecord);


        RequestBody statusforupdate =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, "" + statustochange);


        new HttpUpload<ResponseBody>()
        {
            @Override
            protected void onComplete(ResponseBody response) {
                super.onComplete(response);

                try {
                    //Wlog.i("Success !!" + response.string());
                    if(response.string().toLowerCase().contains("success"))
                    {
                        //Wlog.i("removing file : " + filename);
                        //FileUtils.removeFile(filename);
                    }

                    //SelectWaitingPatientActivity.start(PatientOpdReportActivity.this);
                    //finish();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            protected void onError(String message) {
                super.onError(message);
                Wlog.i("received error !!" + message);
                Toast.makeText(getApplicationContext(), "Issue in updating the record : " + message, Toast.LENGTH_SHORT).show();
            }

        }.httpConnect(HttpUpload.APIS.updateOpdStatus(idforupload,statusforupdate,filePart));

    }


    Bitmap fillPatientDetails()
    {

        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inMutable = true;


        Bitmap backgroundreport = BitmapFactory.decodeResource(getResources(),
                    R.drawable.opd_report_background,opt);


        EmbedText et = new EmbedText(PatientOpdReportActivity.this);

        backgroundreport = BitmapUtils.drawText(backgroundreport,_patientrecord.patientname,et.gx(1),et.gy(1));
        backgroundreport = BitmapUtils.drawText(backgroundreport,_patientrecord.hospitalname ,et.gx(14),et.gy(1));
        backgroundreport = BitmapUtils.drawText(backgroundreport,_patientrecord.age + " / " + _patientrecord.gender,et.gx(2),et.gy(2));
        backgroundreport = BitmapUtils.drawText(backgroundreport,_patientrecord.wardname ,et.gx(8),et.gy(2));
        backgroundreport = BitmapUtils.drawText(backgroundreport,"" + _patientrecord.IPnumber ,et.gx(13),et.gy(2));
        backgroundreport = BitmapUtils.drawText(backgroundreport,"" + _consultantpref.consultantname().getOr("") ,et.gx(11),et.gy(4));

        return backgroundreport;
    }



    Bitmap getPage(int index)
    {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inMutable = true;

        switch (index){
            case 2:
                return BitmapFactory.decodeResource(getResources(),
                        R.drawable.page2,opt);
            case 3:
                return BitmapFactory.decodeResource(getResources(),
                        R.drawable.page3,opt);

        }

        return null;
    }

}
