package projectspecificactivities.questionanswer;

/**
 * Created by Lenovo on 10-11-2017.
 */

public class Answer {
    String userid,checklistID,locationame,projectName,projectLocationDefId,roleName,checklist,questionGroupName,questionId,questionName,answer,formCountt,imagePath;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getLocationame() {
        return locationame;
    }

    public void setLocationame(String locationame) {
        this.locationame = locationame;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectLocationDefId() {
        return projectLocationDefId;
    }

    public void setProjectLocationDefId(String projectLocationDefId) {
        this.projectLocationDefId = projectLocationDefId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getChecklist() {
        return checklist;
    }

    public void setChecklist(String checklist) {
        this.checklist = checklist;
    }

    public String getQuestionGroupName() {
        return questionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        this.questionGroupName = questionGroupName;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionName() {
        return questionName;
    }

    public void setQuestionName(String questionName) {
        this.questionName = questionName;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getFormCountt() {
        return formCountt;
    }

    public void setFormCountt(String formCountt) {
        this.formCountt = formCountt;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getChecklistID() {
        return checklistID;
    }

    public void setChecklistID(String checklistID) {
        this.checklistID = checklistID;
    }
}
