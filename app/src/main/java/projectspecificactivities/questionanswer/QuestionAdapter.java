/*
package projectspecificactivities.questionanswer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.whitesun.sparshhospital.R;
import com.whitesun.sparshhospital.databinding.ItemQuestionBinding;

import java.util.List;

import httpdata.projectspecificmodels.Questions;

*/
/**
 * Created by rahul on 1/11/17.
 *//*


public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.QuestionViewHolder>{

    Context ctx;
    private List<Questions> questionList;

    public QuestionAdapter(Context ctx, List<Questions> questionList) {
        this.ctx = ctx;
        this.questionList = questionList;
    }

    @Override
    public QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(ctx);
        View view = inflater.inflate(R.layout.question_items_second, null);

        return  new QuestionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(QuestionViewHolder holder, int position) {

        Questions questions=questionList.get(position);
        holder.textView.setText(questions.getText());
        holder.textView.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return questionList.size();
    }

    class QuestionViewHolder extends RecyclerView.ViewHolder{

        TextView textView;
        Spinner spinner;
        EditText editText;

        public QuestionViewHolder(View itemView) {
            super(itemView);

            textView =itemView.findViewById(R.id.title);
            spinner=itemView.findViewById(R.id.spinnerAns);
            editText=itemView.findViewById(R.id.etAnswer);

        }
    }
}
*/
