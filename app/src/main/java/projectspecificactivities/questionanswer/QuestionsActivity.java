package projectspecificactivities.questionanswer;

import android.app.Activity;
/*import android.app.AlertDialog;*/
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.app.AlertDialog;

import com.android.databinding.library.baseAdapters.BR;
import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;
import com.whitesun.sparshhospital.R;
import com.whitesun.sparshhospital.databinding.ActivityQuestionsBinding;
import com.whitesun.sparshhospital.databinding.ActivitySelectChecklistBinding;
import com.whitesun.sparshhospital.databinding.ActivitySelectLocationBinding;
import com.whitesun.sparshhospital.databinding.ItemQuestionBinding;
import com.whitesun.sparshhospital.databinding.ItemSelectChecklistBinding;
import com.whitesun.sparshhospital.databinding.ItemSelectLocationBinding;
import com.whitesun.sparshhospital.databinding.ItemSelectQuestiongroupBinding;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import generic.activities.CameraPhotoActivity;
import httpdata.models.android.PhotoGallery;
import httpdata.projectspecificmodels.AnswerStore;
import httpdata.projectspecificmodels.CrudModel;
import httpdata.projectspecificmodels.LocationProject;
import httpdata.projectspecificmodels.QuestionGroup;
import httpdata.projectspecificmodels.Questions;
import httpdata.projectspecificmodels.UserRoleChecklist;
import lib.rest.HttpCall;
import lib.rest.HttpUpload;
import lib.timer.ScheduleTask;
import lib.ui.ProgDialog;
import lib.utils.common.Wlog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import projectspecificactivities.questiongroup.QuestionGroupActivity;
import projectspecificactivities.questiongroup.QuestionGroupActivity_;
import projectspecificactivities.selectchecklist.SelectChecklistActivity;
import projectspecificactivities.selectlocation.SelectLocationActivity;
import projectspecificactivities.selectproject.SelectProjectActivity_;
import projectspecificactivities.sharedpref.LoginPreferences_;

import static projectspecificactivities.questiongroup.QuestionGroupActivity.tempAnswerList1;

@Fullscreen
@EActivity
public class QuestionsActivity extends AppCompatActivity {

    ActivityQuestionsBinding vars;
    ObservableArrayList<Questions> listQuestions = new ObservableArrayList<>();

    @Pref
    LoginPreferences_ _logpref;

    int count = 0;
    static int item_id;
    LastAdapter lastAdapter;
    String photo_path = "";

    public int fornCount = 1;

    int qid;

    final String TAG = QuestionsActivity.class.getSimpleName();
    List<Answer> listanswer = new LinkedList<>();
    Answer answer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        handleclicks();
    }

    void init() {

        //first clear the list answers and tempAnswerList1
        listanswer.clear();
        tempAnswerList1.clear();

        vars = DataBindingUtil.setContentView(this, R.layout.activity_questions);

        downloadQuestionsByQuestionsGroup();

        vars.userName.setText("Welcome " + _logpref.name().get());
        vars.tvLocation.setText(_logpref.locationName().get() + " > " + _logpref.projectName().get() + " >\n" + _logpref.userRoleName().get() + " > " + _logpref.checklistName().get());
        vars.tvGroupTitle.setText(_logpref.questionGroupName().get());
        vars.tvGroupDescription.setText(_logpref.questionGroupDescription().get());


        // if question group will family then addmorebutton will visible otherwise it will hidden
        if (_logpref.questionGroupName().get().equals("Family details")) {
            //System.out.println("_logpref.questionGroupName().get(): "+_logpref.questionGroupName().get());
            vars.btAddMore.setVisibility(View.VISIBLE);
        } else {
            vars.btAddMore.setVisibility(View.GONE);
        }

        //recycler view items
        final ItemType<ItemQuestionBinding> itemtype = new ItemType<ItemQuestionBinding>(R.layout.item_question) {
            @Override
            public void onBind(@NotNull final Holder<ItemQuestionBinding> holder) {

                //Log.d("itemcount-", "" + count);
                count++;
                int type = holder.getBinding().getItemquestion().getType();

                //setting te view
                holder.getBinding().subTitle.setVisibility(View.GONE);
                holder.getBinding().title.setVisibility(View.GONE);
                holder.getBinding().qText.setVisibility(View.GONE);
                holder.getBinding().spinnerAns.setVisibility(View.GONE);
                holder.getBinding().rgRadio.setVisibility(View.GONE);
                holder.getBinding().etAnswer.setVisibility(View.GONE);
                holder.getBinding().btnCaptureImage.setVisibility(View.GONE);
                holder.getBinding().imageCaptured.setVisibility(View.GONE);
                holder.getBinding().qID.setVisibility(View.VISIBLE);
                holder.getBinding().ansID.setVisibility(View.VISIBLE);
                holder.getBinding().qID.setText(String.valueOf(holder.getBinding().getItemquestion().id));

/*

                //getting answer from spinner
                holder.getBinding().spinnerAns.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        holder.getBinding().getItemquestion().value = holder.getBinding().spinnerAns.getSelectedItem().toString();

                        System.out.println("hkhk:" + holder.getBinding().spinnerAns.getSelectedItem().toString());
                        if (!holder.getBinding().spinnerAns.getSelectedItem().toString().contains("Choose Your Answer") && !holder.getBinding().spinnerAns.getSelectedItem().toString().equals("") && holder.getBinding().spinnerAns.getSelectedItem().toString().length() != 0 && holder.getBinding().spinnerAns.getSelectedItem().toString() != null && !holder.getBinding().spinnerAns.getSelectedItem().equals(null)) {

                            answer = new Answer();
                            answer.setUserid(Integer.toString(_logpref.userID().get()));
                            answer.setChecklistID(Integer.toString(_logpref.checklistID().get()));
                            answer.setLocationame(_logpref.locationName().get());
                            answer.setProjectName(_logpref.projectName().get());
                            answer.setProjectLocationDefId(Integer.toString(_logpref.projectLocationDefID().get()));
                            answer.setRoleName(_logpref.userRoleName().get());
                            answer.setChecklist(_logpref.checklistName().get());
                            String questionGroupName = _logpref.questionGroupName().get();
                            answer.setQuestionGroupName(_logpref.questionGroupName().get());
                            answer.setQuestionId(String.valueOf(holder.getBinding().getItemquestion().id));
                            answer.setQuestionName(holder.getBinding().getItemquestion().text);
                            answer.setAnswer(holder.getBinding().spinnerAns.getSelectedItem().toString());
                            answer.setFormCountt(questionGroupName + " form" + Integer.toString(fornCount));
                            listanswer.add(answer);

                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });



                //getting answer from edittext
                holder.getBinding().etAnswer.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        System.out.println("qid1: " + qid + " ans1: " + holder.getBinding().etAnswer.getText().toString() + " s1:" + s);
                    }

                    // int qid;

                    @Override
                    public void afterTextChanged(Editable s) {


                        holder.getBinding().getItemquestion().value = holder.getBinding().etAnswer.getText().toString();

                        //null check(answer)
                        if (!holder.getBinding().etAnswer.getText().toString().contains("Choose Your Answer") && !holder.getBinding().etAnswer.getText().toString().equals("") && holder.getBinding().etAnswer.getText().toString().length() != 0) {


                            System.out.println("getItemId: " + holder.getItemId() + " getLayoutPosition: " + holder.getLayoutPosition() + " adapterposition: " + holder.getAdapterPosition());
                            System.out.println(" NO_ID: " + RecyclerView.NO_ID + " NO_POSITION: " + RecyclerView.NO_POSITION);
                            System.out.println("qid: " + qid + " ans: " + holder.getBinding().etAnswer.getText().toString() + " s:" + s);

                            //check input comming from same edittext?
                            //if data coming from same edit text remove the last item from listanswer and save in listanswer else save input without remove

                            if (qid == holder.getBinding().getItemquestion().id) {
                                // if(qid ==holder.getLayoutPosition()){
                                try {
                                    listanswer.remove(listanswer.size() - 1);
                                } catch (Exception e) {
                                    answer = new Answer();
                                    answer.setUserid(Integer.toString(_logpref.userID().get()));
                                    answer.setChecklistID(Integer.toString(_logpref.checklistID().get()));
                                    answer.setLocationame(_logpref.locationName().get());
                                    answer.setProjectName(_logpref.projectName().get());
                                    answer.setProjectLocationDefId(Integer.toString(_logpref.projectLocationDefID().get()));
                                    answer.setRoleName(_logpref.userRoleName().get());
                                    answer.setChecklist(_logpref.checklistName().get());
                                    String questionGroupName = _logpref.questionGroupName().get();
                                    answer.setQuestionGroupName(_logpref.questionGroupName().get());
                                    answer.setQuestionId(String.valueOf(holder.getBinding().getItemquestion().id));
                                    answer.setQuestionName(holder.getBinding().getItemquestion().text);
                                    answer.setAnswer(holder.getBinding().etAnswer.getText().toString());
                                    answer.setFormCountt(questionGroupName + " form" + Integer.toString(fornCount));
                                    listanswer.add(answer);
                                }
                                answer = new Answer();
                                answer.setUserid(Integer.toString(_logpref.userID().get()));
                                answer.setChecklistID(Integer.toString(_logpref.checklistID().get()));
                                answer.setLocationame(_logpref.locationName().get());
                                answer.setProjectName(_logpref.projectName().get());
                                answer.setProjectLocationDefId(Integer.toString(_logpref.projectLocationDefID().get()));
                                answer.setRoleName(_logpref.userRoleName().get());
                                answer.setChecklist(_logpref.checklistName().get());
                                String questionGroupName = _logpref.questionGroupName().get();
                                answer.setQuestionGroupName(_logpref.questionGroupName().get());
                                answer.setQuestionId(String.valueOf(holder.getBinding().getItemquestion().id));
                                answer.setQuestionName(holder.getBinding().getItemquestion().text);
                                answer.setAnswer(holder.getBinding().etAnswer.getText().toString());
                                answer.setFormCountt(questionGroupName + " form" + Integer.toString(fornCount));
                                listanswer.add(answer);
                            } else {
                                answer = new Answer();
                                answer.setUserid(Integer.toString(_logpref.userID().get()));
                                answer.setChecklistID(Integer.toString(_logpref.checklistID().get()));
                                answer.setLocationame(_logpref.locationName().get());
                                answer.setProjectName(_logpref.projectName().get());
                                answer.setProjectLocationDefId(Integer.toString(_logpref.projectLocationDefID().get()));
                                answer.setRoleName(_logpref.userRoleName().get());
                                answer.setChecklist(_logpref.checklistName().get());
                                String questionGroupName = _logpref.questionGroupName().get();
                                answer.setQuestionGroupName(_logpref.questionGroupName().get());
                                answer.setQuestionId(String.valueOf(holder.getBinding().getItemquestion().id));
                                answer.setQuestionName(holder.getBinding().getItemquestion().text);
                                answer.setAnswer(holder.getBinding().etAnswer.getText().toString());
                                answer.setFormCountt(questionGroupName + " form" + Integer.toString(fornCount));
                                listanswer.add(answer);
                            }
                            qid = holder.getBinding().getItemquestion().id;
                        }
                    }
                });

*/





                //getting answer from edittext
                holder.getBinding().etAnswer.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        System.out.println("qid1: " + qid + " ans1: " + holder.getBinding().etAnswer.getText().toString() + " s1:" + s);
                    }


                    @Override
                    public void afterTextChanged(Editable s) {


                        holder.getBinding().getItemquestion().value = holder.getBinding().etAnswer.getText().toString();

                        //null check(answer)
                        if (!holder.getBinding().etAnswer.getText().toString().contains("Choose Your Answer") && !holder.getBinding().etAnswer.getText().toString().equals("") && holder.getBinding().etAnswer.getText().toString().length() != 0) {


                            Log.d(TAG, "afterTextChanged: getTheIndexOfMatch: " + getTheIndexOfMatch(holder.getBinding().getItemquestion().id));
                            Log.d(TAG, "afterTextChanged: getTheIndexOfMatch1: " + getTheIndexOfMatch1(holder.getBinding().getItemquestion().id));
                            if(getTheIndexOfMatch(holder.getBinding().getItemquestion().id)==true) {
                                answer = new Answer();
                                answer.setUserid(Integer.toString(_logpref.userID().get()));
                                answer.setChecklistID(Integer.toString(_logpref.checklistID().get()));
                                answer.setLocationame(_logpref.locationName().get());
                                answer.setProjectName(_logpref.projectName().get());
                                answer.setProjectLocationDefId(Integer.toString(_logpref.projectLocationDefID().get()));
                                answer.setRoleName(_logpref.userRoleName().get());
                                answer.setChecklist(_logpref.checklistName().get());
                                String questionGroupName = _logpref.questionGroupName().get();
                                answer.setQuestionGroupName(_logpref.questionGroupName().get());
                                answer.setQuestionId(String.valueOf(holder.getBinding().getItemquestion().id));
                                answer.setQuestionName(holder.getBinding().getItemquestion().text);
                                answer.setAnswer(holder.getBinding().etAnswer.getText().toString());
                                answer.setFormCountt(questionGroupName + " form" + Integer.toString(fornCount));
                                listanswer.set(getTheIndexOfMatch1(holder.getBinding().getItemquestion().id),answer);
                            }else{
                                answer = new Answer();
                                answer.setUserid(Integer.toString(_logpref.userID().get()));
                                answer.setChecklistID(Integer.toString(_logpref.checklistID().get()));
                                answer.setLocationame(_logpref.locationName().get());
                                answer.setProjectName(_logpref.projectName().get());
                                answer.setProjectLocationDefId(Integer.toString(_logpref.projectLocationDefID().get()));
                                answer.setRoleName(_logpref.userRoleName().get());
                                answer.setChecklist(_logpref.checklistName().get());
                                String questionGroupName = _logpref.questionGroupName().get();
                                answer.setQuestionGroupName(_logpref.questionGroupName().get());
                                answer.setQuestionId(String.valueOf(holder.getBinding().getItemquestion().id));
                                answer.setQuestionName(holder.getBinding().getItemquestion().text);
                                answer.setAnswer(holder.getBinding().etAnswer.getText().toString());
                                answer.setFormCountt(questionGroupName + " form" + Integer.toString(fornCount));
                                listanswer.add(answer);
                            }

                        }
                    }
                });


                //getting answer from spinner
                holder.getBinding().spinnerAns.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        holder.getBinding().getItemquestion().value = holder.getBinding().spinnerAns.getSelectedItem().toString();

                        System.out.println("hkhk:" + holder.getBinding().spinnerAns.getSelectedItem().toString());
                        if (!holder.getBinding().spinnerAns.getSelectedItem().toString().contains("Choose Your Answer") && !holder.getBinding().spinnerAns.getSelectedItem().toString().equals("") && holder.getBinding().spinnerAns.getSelectedItem().toString().length() != 0 && holder.getBinding().spinnerAns.getSelectedItem().toString() != null && !holder.getBinding().spinnerAns.getSelectedItem().equals(null)) {


                            if(getTheIndexOfMatch(holder.getBinding().getItemquestion().id)==true) {
                                answer = new Answer();
                                answer.setUserid(Integer.toString(_logpref.userID().get()));
                                answer.setChecklistID(Integer.toString(_logpref.checklistID().get()));
                                answer.setLocationame(_logpref.locationName().get());
                                answer.setProjectName(_logpref.projectName().get());
                                answer.setProjectLocationDefId(Integer.toString(_logpref.projectLocationDefID().get()));
                                answer.setRoleName(_logpref.userRoleName().get());
                                answer.setChecklist(_logpref.checklistName().get());
                                String questionGroupName = _logpref.questionGroupName().get();
                                answer.setQuestionGroupName(_logpref.questionGroupName().get());
                                answer.setQuestionId(String.valueOf(holder.getBinding().getItemquestion().id));
                                answer.setQuestionName(holder.getBinding().getItemquestion().text);
                                answer.setAnswer(holder.getBinding().spinnerAns.getSelectedItem().toString());
                                answer.setFormCountt(questionGroupName + " form" + Integer.toString(fornCount));
                                listanswer.set(getTheIndexOfMatch1(holder.getBinding().getItemquestion().id),answer);
                            }else{
                                answer = new Answer();
                                answer.setUserid(Integer.toString(_logpref.userID().get()));
                                answer.setChecklistID(Integer.toString(_logpref.checklistID().get()));
                                answer.setLocationame(_logpref.locationName().get());
                                answer.setProjectName(_logpref.projectName().get());
                                answer.setProjectLocationDefId(Integer.toString(_logpref.projectLocationDefID().get()));
                                answer.setRoleName(_logpref.userRoleName().get());
                                answer.setChecklist(_logpref.checklistName().get());
                                String questionGroupName = _logpref.questionGroupName().get();
                                answer.setQuestionGroupName(_logpref.questionGroupName().get());
                                answer.setQuestionId(String.valueOf(holder.getBinding().getItemquestion().id));
                                answer.setQuestionName(holder.getBinding().getItemquestion().text);
                                answer.setAnswer(holder.getBinding().spinnerAns.getSelectedItem().toString());
                                answer.setFormCountt(questionGroupName + " form" + Integer.toString(fornCount));
                                listanswer.add(answer);
                            }

                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });



                //changing the view type in recycleview
                switch (type) {
                    case 1:
                        holder.getBinding().qText.setVisibility(View.VISIBLE);
                        holder.getBinding().qText.setText(holder.getBinding().getItemquestion().text);
                        holder.getBinding().etAnswer.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        holder.getBinding().qText.setVisibility(View.VISIBLE);
                        holder.getBinding().qText.setText(holder.getBinding().getItemquestion().text);
                        holder.getBinding().etAnswer.setVisibility(View.VISIBLE);
                        holder.getBinding().etAnswer.setInputType(InputType.TYPE_CLASS_NUMBER);
                        break;
                    case 3:
                        holder.getBinding().qText.setVisibility(View.VISIBLE);
                        holder.getBinding().qText.setText(holder.getBinding().getItemquestion().text);
                        holder.getBinding().spinnerAns.setVisibility(View.VISIBLE);

                        String[] result = holder.getBinding().getItemquestion().value.split("\\|");
                        ArrayList<String> list = new ArrayList<>(Arrays.asList(holder.getBinding().getItemquestion().value.split("\\|")));
                        list.add(0, "Choose Your Answer");
                        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(QuestionsActivity.this, android.R.layout.simple_spinner_item, list);
                        holder.getBinding().spinnerAns.setAdapter(spinnerAdapter);
                        break;
                    case 4:
                        holder.getBinding().qText.setVisibility(View.VISIBLE);
                        holder.getBinding().qText.setText(holder.getBinding().getItemquestion().text);
                        holder.getBinding().btnCaptureImage.setVisibility(View.VISIBLE);
                        holder.getBinding().imageCaptured.setVisibility(View.VISIBLE);


                        //getting the image from  camera
                        if (!holder.getBinding().getItemquestion().value.isEmpty()) {
                            System.out.println("imagepath: " + Uri.parse(new File(holder.getBinding().getItemquestion().value).toString()));
                            holder.getBinding().imageCaptured.setImageURI(Uri.parse(new File(holder.getBinding().getItemquestion().value).toString()));
                            answer = new Answer();
                            answer.setUserid(Integer.toString(_logpref.userID().get()));
                            answer.setChecklistID(Integer.toString(_logpref.checklistID().get()));
                            answer.setLocationame(_logpref.locationName().get());
                            answer.setProjectName(_logpref.projectName().get());
                            answer.setProjectLocationDefId(Integer.toString(_logpref.projectLocationDefID().get()));
                            answer.setRoleName(_logpref.userRoleName().get());
                            answer.setChecklist(_logpref.checklistName().get());
                            String questionGroupName = _logpref.questionGroupName().get();
                            answer.setQuestionGroupName(_logpref.questionGroupName().get());
                            answer.setQuestionId(String.valueOf(holder.getBinding().getItemquestion().id));
                            answer.setQuestionName(holder.getBinding().getItemquestion().text);
                            answer.setImagePath(Uri.parse(new File(holder.getBinding().getItemquestion().value).toString()).toString());
                            answer.setFormCountt(questionGroupName + " form" + Integer.toString(fornCount));
                            listanswer.add(answer);
                        }
                        holder.getBinding().btnCaptureImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                item_id = holder.getAdapterPosition();
                                CameraPhotoActivity.startBackCamera(QuestionsActivity.this, 200);
                            }
                        });


                        break;
                    case 5:
                        holder.getBinding().title.setVisibility(View.VISIBLE);
                        holder.getBinding().title.setText(holder.getBinding().getItemquestion().text);
                        holder.getBinding().qID.setVisibility(View.GONE);
                        holder.getBinding().ansID.setVisibility(View.GONE);
                        break;
                    case 6:
                        holder.getBinding().subTitle.setVisibility(View.VISIBLE);
                        holder.getBinding().subTitle.setText(holder.getBinding().getItemquestion().text);
                        holder.getBinding().qID.setVisibility(View.GONE);
                        holder.getBinding().ansID.setVisibility(View.GONE);
                        break;
                    default:
                }

            }
        };

        //adding server data(all questions) in recycleview adapter
        lastAdapter = new LastAdapter(listQuestions, BR.itemquestion)
                .map(Questions.class, itemtype)
                .into(vars.rvQuestions);
        /*vars.rvQuestions.setLayoutManager(new LinearLayoutManager(this));
        QuestionAdapter questionAdapter=new QuestionAdapter(this,listLocationProject);
        vars.rvQuestions.setAdapter(questionAdapter);
        */


    }


    public Boolean getTheIndexOfMatch(int Qidd) {

        for (Answer answer : listanswer) {
            String qids = answer.getQuestionId();
            String dat = Integer.toString(Qidd);
            Log.d(TAG, "getTheIndexOfMatch qids: " + qids);
            Log.d(TAG, "getTheIndexOfMatch: from edittext" + Qidd);
            if (qids.equals(dat)) {

                return true;

            }

        }
        return false;
    }


    public int getTheIndexOfMatch1(int Qidd) {
        for (Answer answer : listanswer) {
            String qids = answer.getQuestionId();
            String dat = Integer.toString(Qidd);
            Log.d(TAG, "getTheIndexOfMatch1 qids: " + qids);
            Log.d(TAG, "getTheIndexOfMatch1: from edittext" + Qidd);
            if (qids.equals(dat)) {
                int index= listanswer.indexOf(answer);
                return listanswer.indexOf(answer);

            }

        }
        return 999;
    }


    void handleclicks() {


        vars.btAddMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(QuestionsActivity.this, "Form" + fornCount + " Answers submitted successfully", Toast.LENGTH_SHORT).show();
                for (Answer s : listanswer) {
                    tempAnswerList1.add(s);
                }
                fornCount++;
                listanswer.clear();
                qid = 987666;
                // Intent intent = new Intent(getApplicationContext(), QuestionsActivity_.class);
                // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                // startActivity(intent);
                downloadQuestionsByQuestionsGroup();

            }
        });

        vars.btreset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadQuestionsByQuestionsGroup();

               /* AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                builder.setTitle("Warning !");
                builder.setMessage("It will Delete all the data of this page!\nDo You Want to Continue ?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        downloadProjectAndLoacation();
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //TODO
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();*/

            }
        });


        vars.btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        vars.btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                QuestionGroupActivity.tempAnswerList.addAll(listQuestions);

                //saving answer object in the tempAnswerlist
                // tempAnswerList1.addAll(listanswer);
                for (Answer s : listanswer) {
                    tempAnswerList1.add(s);
                }
               /* for (Answer s : tempAnswerList1) {
                    System.out.println("question name: " + s.getQuestionName() + " answer: " + s.getAnswer() + " userid: " + s.getUserid() + " formcount: " + s.getFormCountt());
                }*/

                //QuestionsActivity.answerStoreList.get(0).id
                // uploadImgeData(_logpref.userID().get(),2,1000,0,"I am answer",photo_path);

               /* Toast.makeText(getApplicationContext(),"Data Saved",Toast.LENGTH_SHORT).show();
               *//* QuestionGroupActivity_.intent(QuestionsActivity.this).start();

                for(int i=0;i<listLocationProject.size();i++)
                {
                    answerStoreList.add(listLocationProject.get(i));
                }*//*

                for(int i=1;i<QuestionsActivity.answerStoreList.size();i++)
                {

                    new HttpUpload<ResponseBody>()
                    {
                        @Override
                        protected void onComplete(ResponseBody response) {
                            super.onComplete(response);
                            try {
                                Log.d("check Upload","try");
                                if(response.string().toLowerCase().contains("success"))
                                {
                                    Log.d("check Upload","SUCCESS!!!");
                                }
                                finish();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        protected void onError(String message) {
                            Log.d("check Upload","error");
                            super.onError(message);
                            Wlog.i("received error !!" + message);
                            Toast.makeText(getApplicationContext(), "Issue in updating the record : " + message, Toast.LENGTH_SHORT).show();
                        }

                    }.httpConnect(HttpUpload.APIS.getuseranswer(_logpref.userID().get(),1000,QuestionsActivity.answerStoreList.get(i).id,QuestionsActivity.answerStoreList.get(i).value));
                    // httpConnect(HttpUpload.APIS.getuseranswer(2,100,2,"answer"));
                }*/
                setResult(RESULT_OK);
                finish();
            }
        });
    }


    @UiThread
    void fillLocation(List<Questions> locationProject) {
        //  List<Questions> locationProject1=List.reverse
        Collections.reverse(locationProject);
        listQuestions.clear();
        listQuestions.addAll(locationProject);
        for (Questions s : locationProject) {
            System.out.println("qqq: " + s.getText() + ":qid:" + s.getId() + ":qqqq:" + s.getQuestion_order());
        }
        //QuestionGroupActivity.tempAnswerMap.put(_logpref.questionGroupId().get(),listLocationProject);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 200) {
            if (resultCode == Activity.RESULT_OK) {
                ArrayList<PhotoGallery> pglist = data.getParcelableArrayListExtra(CameraPhotoActivity.EXTRA_RESULTPHOTOITEMS);
                photo_path = pglist.get(0).imagepath;

                Log.d("itemcount-", "path-" + photo_path);
                listQuestions.get(item_id).setValue(photo_path);
                lastAdapter.notifyItemChanged(item_id);
            } else {
                Wlog.d("Camera operation is cancelled");
            }
            return;
        }

    }

    /**
     * getting data from server
     *
     * @parameter get-questiongroupid
     * result-allQuestionList
     */
    void downloadQuestionsByQuestionsGroup() {
        ProgDialog.show(this, "Downloading Questions for You", true);
        new HttpCall<CrudModel<Questions>>() {

            @Override
            protected void onComplete(CrudModel<Questions> response) {

                super.onComplete(response);
                ProgDialog.close();
                //Log.d("download_users_location", String.valueOf(response.data.get(1)));
                fillLocation(response.data);
                System.out.println("QuestionsActivity: downloadProjectAndLoacation " + response.data);
            }

            @Override
            protected void onError(String message) {
                super.onError(message);

            }
        }.httpConnect(HttpCall.APIS.getquestion(_logpref.questionGroupId().get()));

    }


}
