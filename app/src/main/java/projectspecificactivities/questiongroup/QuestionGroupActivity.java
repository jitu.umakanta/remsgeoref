package projectspecificactivities.questiongroup;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.databinding.library.baseAdapters.BR;
import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;
import com.google.gson.Gson;
import com.whitesun.sparshhospital.R;
import com.whitesun.sparshhospital.databinding.ActivityQuestionGroupBinding;
import com.whitesun.sparshhospital.databinding.ItemSelectQuestiongroupBinding;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import genericservices.UploadBackgroundService;
import httpdata.projectspecificmodels.CrudModel;
import httpdata.projectspecificmodels.QuestionGroup;
import httpdata.projectspecificmodels.Questions;
import lib.rest.HttpCall;
import lib.rest.HttpUpload;
import lib.ui.ProgDialog;
import lib.utils.common.Wlog;
import offlinemodesave.Offline;
import offlinemodesave.UploadUserAnswerWithoutImageOffline;
import offlinemodesave.UploadUserAnswerWithoutImageOffline1;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import projectspecificactivities.questionanswer.Answer;
import projectspecificactivities.questionanswer.QuestionsActivity_;
import projectspecificactivities.sharedpref.LoginPreferences_;

@Fullscreen
@EActivity
public class QuestionGroupActivity extends AppCompatActivity {

    ActivityQuestionGroupBinding vars;
    ObservableArrayList<QuestionGroup> listLocationProject = new ObservableArrayList<>();
    public static ObservableArrayList<Questions> tempAnswerList = new ObservableArrayList<>();
    public static HashMap<Integer, ObservableArrayList<Questions>> tempAnswerMap = new HashMap<>();

    //saving listanswerobject in tempAnswerList1
    public static ObservableArrayList<Answer> tempAnswerList1 = new ObservableArrayList<>();

    public static ArrayList ll = new ArrayList();

    String uplrID;
    LastAdapter lastAdapter;
    public static final int REQUEST_GROUP_CODE = 101;
    @Pref
    LoginPreferences_ _logpref;
    static int itemID;


    //image upload
    String mediaPath;
    ProgressDialog progressDialog;

    private static final String TAG = QuestionGroupActivity.class.getSimpleName();
    List<UploadUserAnswerWithoutImageOffline> objlist = new LinkedList<>();
    List<UploadUserAnswerWithoutImageOffline1> objlist1 = new LinkedList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // saveAnswerInDb = new SaveAnswerInDb(getApplicationContext());

        uplrID = "" + _logpref.userID().get() + "-" + _logpref.locationID().get() + "-" +
                _logpref.projectDefID().get() + "-" + _logpref.userRoleID().get() + "-" + _logpref.checklistID().get();

        init();
        handleclicks();


    }

    void init() {

        //progressbar init
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Uploading...");

        vars = DataBindingUtil.setContentView(this, R.layout.activity_question_group);
        vars.userName.setText("Welcome " + _logpref.name().get());
        vars.tvLocation.setText(_logpref.locationName().get() + " > " + _logpref.projectName().get() + " >\n" + _logpref.userRoleName().get() + " > " + _logpref.checklistName().get());

        ItemType<ItemSelectQuestiongroupBinding> itemtype = new ItemType<ItemSelectQuestiongroupBinding>(R.layout.item_select_questiongroup) {
            @Override
            public void onBind(@NotNull final Holder<ItemSelectQuestiongroupBinding> holder) {

                if (holder.getBinding().getSelectquestiongroup().status > 1) {
                    holder.getBinding().statusImage.setVisibility(View.VISIBLE);
                } else {
                    holder.getBinding().statusImage.setVisibility(View.GONE);
                }

                //if(holder.getBinding().getSelectquestiongroup().state!=1)

                //onclick action on each items in recyceview
                holder.getBinding().cvChecklist.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (holder.getBinding().getSelectquestiongroup().status > 1) {
                            Toast.makeText(QuestionGroupActivity.this, "Already Answered!", Toast.LENGTH_SHORT).show();
                        } else {

                            _logpref.questionGroupId().put(Integer.parseInt("" + holder.getBinding().groupId.getText()));
                            _logpref.questionGroupName().put(holder.getBinding().groupName.getText().toString());
                            _logpref.questionGroupDescription().put(holder.getBinding().groupDescription.getText().toString());

                            QuestionsActivity_.intent(QuestionGroupActivity.this).startForResult(102);

                           /* Intent intent=new Intent(getApplicationContext(),QuestionGroupActivity.class);
                            startActivity(intent);*/

                            itemID = holder.getAdapterPosition();
                            Log.d("return", "after intent call  ");
                        }
                    }
                });
            }
        };


        lastAdapter = new LastAdapter(listLocationProject, BR.selectquestiongroup)
                .map(QuestionGroup.class, itemtype)
                .into(vars.rvQuestionGroup);

        downloadProjectAndLoacation();
    }

    void handleclicks() {

      /*  vars.buttonFileUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 0);
            }
        });*/

        vars.btrefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                QuestionGroupActivity.tempAnswerList.clear();
                downloadProjectAndLoacation();
            }
        });

        vars.btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                QuestionGroupActivity.tempAnswerList.clear();
                onBackPressed();
            }
        });

        vars.btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UploadUserAnswerWithoutImageOffline uploadUserAnswerWithoutImageOffline;
                UploadUserAnswerWithoutImageOffline1 uploadUserAnswerWithoutImageOffline1;


                for (Answer s : tempAnswerList1) {

                    System.out.println("question name123: " + s.getQuestionName() + " answer: " + s.getAnswer() + " userid: " + s.getUserid() + " formcount: " + s.getFormCountt() + " " + s.getImagePath() + " " + s.getLocationame() + " " + s.getProjectLocationDefId() + " " + s.getRoleName() + " " + s.getQuestionId() + " " + s.getProjectName());
                    System.out.println(Integer.parseInt(s.getUserid()) + " " + Integer.parseInt(s.getChecklistID()) + " " + Integer.parseInt(s.getQuestionId()) + "  " + Integer.parseInt(s.getProjectLocationDefId()) + " " + s.getAnswer() + " " + "0" + " " + "0" + " " + s.getFormCountt());


                    if (s.getImagePath() == null || s.getImagePath().equals("")) {

                        uploadUserAnswerWithoutImageOffline = new UploadUserAnswerWithoutImageOffline(s.getUserid(), s.getChecklistID(), s.getQuestionId(), s.getProjectLocationDefId(), s.getAnswer(), "0", "0", s.getFormCountt());
                        objlist.add(uploadUserAnswerWithoutImageOffline);


                        if (isNetworkAvailable() == true) {
                            uploadUserAnswerWithoutImage(Integer.parseInt(s.getUserid()), Integer.parseInt(s.getChecklistID()), Integer.parseInt(s.getQuestionId()), Integer.parseInt(s.getProjectLocationDefId()), s.getAnswer(), "0", "0", s.getFormCountt());
                        } else {
                            Toast.makeText(QuestionGroupActivity.this, "internet is not available .when internet will come it will send data to server", Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        // uploadUserAnswerWithoutImageOffline1=new UploadUserAnswerWithoutImageOffline(s.getUserid(), s.getChecklistID(),s.getQuestionId(), s.getProjectLocationDefId(), s.getAnswer(), "0", "0", s.getFormCountt());

                        uploadUserAnswerWithoutImageOffline1 = new UploadUserAnswerWithoutImageOffline1(s.getUserid(), s.getChecklistID(), s.getQuestionId(), s.getProjectLocationDefId(), "no answer", "0", s.getImagePath(), s.getFormCountt());
                        objlist1.add(uploadUserAnswerWithoutImageOffline1);

                       // uploadUserAnswerWithImage(Integer.parseInt(s.getUserid()), Integer.parseInt(s.getChecklistID()), Integer.parseInt(s.getQuestionId()), Integer.parseInt(s.getProjectLocationDefId()), "no answer", "0", s.getImagePath(), s.getFormCountt());


                    }

                }

                //saving answer in json in sharedpreference
                SharedPreferences pref = getApplicationContext().getSharedPreferences("answerwithoutimagepr", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                String connectionsJSONString = new Gson().toJson(objlist);
                editor.putString("answerwithoutimage", connectionsJSONString);
                editor.commit();




                //saving answer in json in sharedpreference
                SharedPreferences pref10 = getApplicationContext().getSharedPreferences("answerwithimagepr", MODE_PRIVATE);
                SharedPreferences.Editor editor10 = pref10.edit();
                String connectionsJSONString10 = new Gson().toJson(objlist1);
                editor10.putString("answerwithimage", connectionsJSONString10);
                editor10.commit();

/*

                SharedPreferences pref111 = getApplicationContext().getSharedPreferences("answerwithoutimagepr", MODE_PRIVATE);
                String connectionsJSONString111 = pref111.getString("answerwithoutimage", null);
                Log.d("aa", "QuestionGroupActivity: " + "onClick: " + "[view]: " + "connectionsJSONString11: " + connectionsJSONString111);


                SharedPreferences pref11 = getApplicationContext().getSharedPreferences("answerwithimagepr", MODE_PRIVATE);
                String connectionsJSONString11 = pref11.getString("answerwithimage", null);
                Log.d("aa", "QuestionGroupActivity: " + "onClick: " + "[view]: " + "connectionsJSONString11: " + connectionsJSONString11);
*/

                setResult(RESULT_OK);
                finish();

                //not by
                /*
                Toast.makeText(QuestionGroupActivity.this, "" + QuestionGroupActivity.tempAnswerList.size(), Toast.LENGTH_SHORT).show();
                int countNotAnswered = 0;
                for (int i = 0; i < listLocationProject.size(); i++) {
                    if (listLocationProject.get(i).status < 2) {
                        Toast.makeText(QuestionGroupActivity.this, "Please Answer All the Question Group", Toast.LENGTH_SHORT).show();
                        countNotAnswered++;
                        break;
                    }
                }
                if (countNotAnswered == 0) {
                    Toast.makeText(QuestionGroupActivity.this, "Uploading Started", Toast.LENGTH_SHORT).show();
                    int dataCount = 0;
                    for (int i = 0; i < QuestionGroupActivity.tempAnswerList.size(); i++) {
                        String answer_text = "";
                        String answer_filepath = "";
                        if (tempAnswerList.get(i).type == 4) {
                            answer_filepath = tempAnswerList.get(i).value;
                        } else {
                            answer_text = tempAnswerList.get(i).value;
                            answer_filepath = null;
                        }
                        uploadImageData(_logpref.userID().get(), tempAnswerList.get(i).id, 1000, 0,
                                answer_text, answer_filepath);
                        dataCount++;
                    }
                    Toast.makeText(QuestionGroupActivity.this, "Uploaded" + dataCount, Toast.LENGTH_SHORT).show();

                    if (dataCount != 0) {
                        setResult(RESULT_OK);
                        finish();
                    }
                }
                */



             /*   for(int i=1;i<QuestionGroupActivity.tempAnswerList.size();i++)
                {

                    // httpConnect(HttpUpload.APIS.getuseranswer(2,100,2,"answer"));
                }*/

                // QuestionsActivity.answerStoreList.clear();


                //SelectChecklistActivity_.intent(QuestionGroupActivity.this).start();
                //finish();
            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 102) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d("return", "request code matched");
                listLocationProject.get(itemID).status = 2;
                lastAdapter.notifyItemChanged(itemID);


                for (Answer s : tempAnswerList1) {
                    System.out.println("question name: " + s.getQuestionName() + " answer: " + s.getAnswer() + " userid: " + s.getUserid() + " formcount: " + s.getFormCountt() + s.getImagePath() + s.getLocationame() + s.getProjectLocationDefId() + s.getRoleName() + s.getQuestionId() + s.getProjectName());
                }

            }
            if (resultCode == Activity.RESULT_CANCELED) {
            }
        }

/*

        */
/**
 * IMAGE URI PATH GETTING
 *//*

        super.onActivityResult(requestCode, resultCode, data);


        try {
            // When an Image is picked
            if (requestCode == 0 && resultCode == RESULT_OK && null != data) {

                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                mediaPath = cursor.getString(columnIndex);
                System.out.println("mediaPath: " + mediaPath);
                // Set the Image in ImageView for Previewing the Media
                vars.imageView.setImageBitmap(BitmapFactory.decodeFile(mediaPath));
                cursor.close();

            } else {
                Toast.makeText(this, "You haven't picked Image/Video", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            System.out.println("imagepickexception: " + e);
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }

*/

    }


    @UiThread
    void fillLocation(List<QuestionGroup> locationProject) {
        listLocationProject.clear();
        listLocationProject.addAll(locationProject);
    }


    /**
     * getting data from server
     *
     * @parameter get-checklistid
     * result-allQuestionGroupList
     */
    void downloadProjectAndLoacation() {
        ProgDialog.show(this, "Downloading QuestionGroupList", true);
        new HttpCall<CrudModel<QuestionGroup>>() {

            @Override
            protected void onComplete(CrudModel<QuestionGroup> response) {

                super.onComplete(response);
                ProgDialog.close();
//                Log.d("download_users_location", String.valueOf(response.data.get(1)));
                fillLocation(response.data);
            }

            @Override
            protected void onError(String message) {
                super.onError(message);

            }
        }.httpConnect(HttpCall.APIS.getquestiongroup(_logpref.checklistID().get()));
    }

    /**
     * @param getUserid
     * @param ChecklistID
     * @param QuestionId
     * @param ProjectLocationDefId
     * @param Answer
     * @param AnswerNumber
     * @param sAnswerFilePath
     * @param FormCountt
     */
    private void uploadUserAnswerWithoutImage(int getUserid, int ChecklistID, int QuestionId, int ProjectLocationDefId, String Answer, String AnswerNumber, String sAnswerFilePath, String FormCountt) {
        Log.d(TAG, "uploadUserAnswerWithoutImage: " + "getUserid: " + getUserid + "ChecklistID: " + ChecklistID + "QuestionId: " + QuestionId + "ProjectLocationDefId: " + ProjectLocationDefId + "Answer: " + Answer + "AnswerNumber: " + AnswerNumber + "sAnswerFilePath: " + sAnswerFilePath + "FormCountt: " + FormCountt);
        //sending user answer without image
        new HttpUpload<ResponseBody>() {
            @Override
            protected void onComplete(ResponseBody response) {
                super.onComplete(response);

                System.out.println("response: " + response);
                progressDialog.dismiss();

            }

            @Override
            protected void onError(String message) {
                super.onError(message);
                Wlog.i("received error !!" + message);
                Toast.makeText(getApplicationContext(), "Issue in updating the record : " + message, Toast.LENGTH_SHORT).show();
            }

        }.httpConnect(HttpUpload.APIS.setuseranswer(getUserid, ChecklistID, QuestionId, ProjectLocationDefId, Answer, AnswerNumber, sAnswerFilePath, FormCountt));

    }

    /**
     * @param getUserid
     * @param ChecklistID
     * @param QuestionId
     * @param ProjectLocationDefId
     * @param Answer
     * @param AnswerNumber
     * @param imagePath
     * @param FormCountt
     */
    private void uploadUserAnswerWithImage(int getUserid, int ChecklistID, int QuestionId, int ProjectLocationDefId, String Answer, String AnswerNumber, String imagePath, String FormCountt) {

        Log.d(TAG, "uploadUserAnswerWithImage: " + "getUserid: " + getUserid + "ChecklistID: " + ChecklistID + "QuestionId: " + QuestionId + "ProjectLocationDefId: " + ProjectLocationDefId + "Answer: " + Answer + "AnswerNumber: " + AnswerNumber + "imagePath: " + imagePath + "FormCountt: " + FormCountt);

        //sending user answer with image
        mediaPath = imagePath;
        File file = new File(mediaPath);
        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("answer_filepath", file.getName(), requestBody);
        //giving the name to file
        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());

        //converting string to requestbody
       /* RequestBody user_def_id = RequestBody.create(okhttp3.MultipartBody.FORM, "" + _logpref.userID().get());
        RequestBody check_list = RequestBody.create(okhttp3.MultipartBody.FORM, "" + _logpref.checklistID().get());
        RequestBody uplr_id = RequestBody.create(okhttp3.MultipartBody.FORM, "" + _logpref.projectLocationDefID().get());
        RequestBody question_def_id = RequestBody.create(okhttp3.MultipartBody.FORM, "" + _logpref.questionGroupId().get());*/

        RequestBody Userid = RequestBody.create(okhttp3.MultipartBody.FORM, "" + getUserid);
        RequestBody check_list1 = RequestBody.create(okhttp3.MultipartBody.FORM, "" + ChecklistID);
        RequestBody QuestionId1 = RequestBody.create(okhttp3.MultipartBody.FORM, "" + QuestionId);
        RequestBody ProjectLocationDefId1 = RequestBody.create(okhttp3.MultipartBody.FORM, "" + ProjectLocationDefId);
        RequestBody Answer1 = RequestBody.create(okhttp3.MultipartBody.FORM, "" + Answer);
        RequestBody AnswerNumber1 = RequestBody.create(okhttp3.MultipartBody.FORM, "" + AnswerNumber);
        RequestBody FormCountt1 = RequestBody.create(okhttp3.MultipartBody.FORM, "" + FormCountt);
        new HttpUpload<ResponseBody>() {
            @Override
            protected void onComplete(ResponseBody response) {
                super.onComplete(response);


                progressDialog.dismiss();

            }

            @Override
            protected void onError(String message) {
                super.onError(message);
                Wlog.i("received error !!" + message);
                Toast.makeText(getApplicationContext(), "Issue in updating the record : " + message, Toast.LENGTH_SHORT).show();
            }

        }.httpConnect(HttpUpload.APIS.setuseranswerimage(Userid, check_list1, QuestionId1, ProjectLocationDefId1, Answer1, AnswerNumber1, fileToUpload, FormCountt1));

    }

    void uploadNormalData(int user_def_id, int question_def_id, int uplr_id, int answer_number, String answer_text, String filename) {

        /*
        new HttpUpload<ResponseBody>() {
            @Override
            protected void onComplete(ResponseBody response) {
                super.onComplete(response);
                try {
                    Log.d("check Upload", "try");
                    if (response.string().toLowerCase().contains("success")) {
                        Log.d("check Upload", "SUCCESS!!!");
                    }
                    finish();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected void onError(String message) {
                Log.d("check Upload", "error");
                super.onError(message);
                Wlog.i("received error !!" + message);
                Toast.makeText(getApplicationContext(), "Issue in updating the record : " + message, Toast.LENGTH_SHORT).show();
            }

        }.httpConnect(HttpUpload.APIS.getuseranswer(_logpref.userID().get(), QuestionGroupActivity.tempAnswerList.get(0).id, "uplr", QuestionGroupActivity.tempAnswerList.get(0).value, "0", 0));

        */

    }

    void uploadImageData(int user_def_id, int question_def_id, int uplr_id, int answer_number, String answer_text, String filename) {
        MultipartBody.Part filePart;
        if (filename == null || filename.equals("") || filename.length() == 0 || filename.isEmpty()) {
            File file = new File("fakefilename");
            filePart = MultipartBody.Part.createFormData("answer_filepath", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));

        } else {

            File file = new File(filename);
            filePart = MultipartBody.Part.createFormData("answer_filepath", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));

        }
        RequestBody up_user_def_id = RequestBody.create(okhttp3.MultipartBody.FORM, "" + user_def_id);
        RequestBody up_question_def_id = RequestBody.create(okhttp3.MultipartBody.FORM, "" + question_def_id);
        RequestBody up_uplr_id = RequestBody.create(okhttp3.MultipartBody.FORM, "" + uplr_id);
        RequestBody up_answer_text = RequestBody.create(okhttp3.MultipartBody.FORM, "" + answer_text);
        RequestBody up_answer_number = RequestBody.create(okhttp3.MultipartBody.FORM, "" + answer_number);

        new HttpUpload<ResponseBody>() {
            @Override
            protected void onComplete(ResponseBody response) {
                super.onComplete(response);

                try {
                    if (response.string().toLowerCase().contains("success")) {
                        Wlog.i("SUCCESS!!!");
                    }
                    finish();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            protected void onError(String message) {
                super.onError(message);
                Wlog.i("received error !!" + message);
                Toast.makeText(getApplicationContext(), "Issue in updating the record : " + message, Toast.LENGTH_SHORT).show();
            }

        }.httpConnect(HttpUpload.APIS.adduseranswerimage(up_user_def_id, up_question_def_id, up_uplr_id, up_answer_text, up_answer_number, filePart));


    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}

