package projectspecificactivities.selectchecklist;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.databinding.library.baseAdapters.BR;
import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;
import com.whitesun.sparshhospital.R;
import com.whitesun.sparshhospital.databinding.ActivitySelectChecklistBinding;
import com.whitesun.sparshhospital.databinding.ItemSelectChecklistBinding;
import com.whitesun.sparshhospital.databinding.ItemSelectTasklistBinding;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import httpdata.projectspecificmodels.CrudModel;
import httpdata.projectspecificmodels.UserRoleChecklist;
import httpdata.projectspecificmodels.UserRoleTasklist;
import lib.rest.HttpCall;
import lib.ui.ProgDialog;
import lib.utils.date.DateUtils;
import projectspecificactivities.login.LoginActivity_;
import projectspecificactivities.questiongroup.QuestionGroupActivity_;
import projectspecificactivities.sharedpref.LoginPreferences_;

@Fullscreen
@EActivity
public class SelectChecklistActivity extends AppCompatActivity {


    ActivitySelectChecklistBinding vars;
    ObservableArrayList<UserRoleChecklist> listLocationProject = new ObservableArrayList<>();
    ObservableArrayList<UserRoleTasklist> listLocationProject1 = new ObservableArrayList<>();
    static int itemID;
    LastAdapter lastAdapter;

    @Pref
    LoginPreferences_ _logpref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //show notification of the taskafter some time
        taskReminder("2");

        init();
        handleclicks();
        execute();


    }

    void init() {
        vars = DataBindingUtil.setContentView(this, R.layout.activity_select_checklist);
        vars.userName.setText("Welcome " + _logpref.name().get());
        vars.tvLocation.setText(_logpref.locationName().get() + " > " + _logpref.projectName().get() + " >\n" + _logpref.userRoleName().get());

        ItemType<ItemSelectChecklistBinding> itemtype = new ItemType<ItemSelectChecklistBinding>(R.layout.item_select_checklist) {
            @Override
            public void onBind(@NotNull final Holder<ItemSelectChecklistBinding> holder) {

                if (holder.getBinding().getSelectchecklist().status > 1) {
                    holder.getBinding().statusImage.setVisibility(View.VISIBLE);
                } else {
                    holder.getBinding().statusImage.setVisibility(View.GONE);
                }

                holder.getBinding().cvChecklist.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (holder.getBinding().getSelectchecklist().status > 1 && holder.getBinding().getSelectchecklist().checklist_type > 1) {
                            Toast.makeText(SelectChecklistActivity.this, "Already Answered!\nCan be answered only Once", Toast.LENGTH_SHORT).show();
                        } else {
                            _logpref.checklistID().put(Integer.parseInt("" + holder.getBinding().checklistID.getText()));
                            _logpref.checklistName().put(holder.getBinding().checklistName.getText().toString());
                            itemID = holder.getAdapterPosition();
                            QuestionGroupActivity_.intent(SelectChecklistActivity.this).startForResult(101);
                        }
                        // SelectChecklistActivity_.intent(SelectRoleActivity.this).start();
                        // QuestionsActivity_.intent(SelectChecklistActivity.this).start();

                       /* Intent intent=new Intent(SelectChecklistActivity.this, QuestionsActivity.class);
                        intent.putExtra("checklistID",Integer.parseInt(holder.getBinding().checklistID.getText().toString()));
                        startActivity(intent);*/
                    }
                });
            }
        };

        lastAdapter = new LastAdapter(listLocationProject, BR.selectchecklist)
                .map(UserRoleChecklist.class, itemtype)
                .into(vars.rvSelectChecklist);

        downloadProjectAndLoacation();
        downloadTaskLists();

        //onclick refresh action
        vars.btrefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadProjectAndLoacation();
                downloadTaskLists();
            }
        });

        //onclick back button
        vars.btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        vars.btLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginActivity_.intent(SelectChecklistActivity.this).start();
                finish();
            }
        });


        //Recycleview for task list
        ItemType<ItemSelectTasklistBinding> itemtype1 = new ItemType<ItemSelectTasklistBinding>(R.layout.item_select_tasklist) {
            @Override
            public void onBind(@NotNull final Holder<ItemSelectTasklistBinding> holder) {

                holder.getBinding().cvChecklist1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                       /* if (holder.getBinding().getSelectchecklist().status > 1 && holder.getBinding().getSelectchecklist().checklist_type > 1) {
                            Toast.makeText(SelectChecklistActivity.this, "Already Answered!\nCan be answered only Once", Toast.LENGTH_SHORT).show();
                        } else {
                            _logpref.checklistID().put(Integer.parseInt("" + holder.getBinding().tasklistID.getText()));
                            _logpref.checklistName().put(holder.getBinding().tasklistName.getText().toString());
                            itemID = holder.getAdapterPosition();
                            QuestionGroupActivity_.intent(SelectChecklistActivity.this).startForResult(101);
                        }*/
                        // SelectChecklistActivity_.intent(SelectRoleActivity.this).start();
                        // QuestionsActivity_.intent(SelectChecklistActivity.this).start();

                        /*Intent intent=new Intent(SelectChecklistActivity.this, QuestionsActivity.class);
                        intent.putExtra("checklistID",Integer.parseInt(holder.getBinding().checklistID.getText().toString()));
                        startActivity(intent);*/
                    }
                });
            }
        };

        lastAdapter = new LastAdapter(listLocationProject1, BR.selectchecklist)
                .map(UserRoleTasklist.class, itemtype1)
                .into(vars.wRecycleViewTaskList);
    }

    void handleclicks() {

    }

    void execute() {
    }

    @UiThread
    void fillLocation(List<UserRoleChecklist> locationProject) {
        listLocationProject.clear();
        for (int i = 0; i < locationProject.size(); i++) {
            listLocationProject.add(locationProject.get(i));
        }

    }

    @UiThread
    void fillTaskList(List<UserRoleTasklist> locationProject1) {
        listLocationProject1.clear();
        for (int i = 0; i < locationProject1.size(); i++) {
            listLocationProject1.add(locationProject1.get(i));
        }
        Log.d("listLocationProject1", "fillTaskList: " + listLocationProject1);

    }

    void downloadProjectAndLoacation() {
        ProgDialog.show(this, "Downloading Project and Location for You", true);
        new HttpCall<CrudModel<UserRoleChecklist>>() {

            @Override
            protected void onComplete(CrudModel<UserRoleChecklist> response) {

                super.onComplete(response);
                ProgDialog.close();
//                Log.d("download_users_location", String.valueOf(response.data.get(1)));
                fillLocation(response.data);
            }

            @Override
            protected void onError(String message) {
                super.onError(message);

            }
        }.httpConnect(HttpCall.APIS.getuserchecklist(_logpref.userRoleID().get()));
    }

    void downloadTaskLists() {
        ProgDialog.show(this, "Downloading TaskList", true);
        new HttpCall<CrudModel<UserRoleTasklist>>() {

            @Override
            protected void onComplete(CrudModel<UserRoleTasklist> response) {

                super.onComplete(response);
                ProgDialog.close();
//                Log.d("download_users_location", String.valueOf(response.data.get(1)));

                fillTaskList(response.data);
            }

            @Override
            protected void onError(String message) {
                super.onError(message);

            }
        }.httpConnect(HttpCall.APIS.getusertask());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.d("returnchk", "on activity result ");

        if (requestCode == 101) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d("returnchk", "request code matched for checklist");
                listLocationProject.get(itemID).status = 2;
                lastAdapter.notifyItemChanged(itemID);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    public void taskReminder(String repethours) {

        String currenttime = DateUtils.getCurrentdateTimeString("/");

       /* Notification notification = new Notification(this);
        notification.Notification();*/
    }
}
