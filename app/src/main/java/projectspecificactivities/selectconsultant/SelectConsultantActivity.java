package projectspecificactivities.selectconsultant;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.databinding.library.baseAdapters.BR;
import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;
import com.whitesun.sparshhospital.R;
import com.whitesun.sparshhospital.databinding.ActivitySelectConsultantBinding;
import com.whitesun.sparshhospital.databinding.ItemConsultantSelectBinding;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import httpdata.projectspecificmodels.Consultant;
import httpdata.projectspecificmodels.CrudModel;
import lib.rest.HttpCall;
import projectspecificactivities.selecthospital.SelectHospitalActivity;
import projectspecificactivities.selectopdpatient.SelectWaitingPatientActivity_;
import projectspecificactivities.sharedpref.ConsultantPreferences_;

@Fullscreen
@EActivity
public class SelectConsultantActivity extends AppCompatActivity {

    ActivitySelectConsultantBinding vars;

    ObservableArrayList<Consultant> consultantcards = new ObservableArrayList<>();

    @Pref
    ConsultantPreferences_ _consultantpref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        handleclicks();
        handleMenu();
        execute();
    }


    void init()
    {
        vars = DataBindingUtil.setContentView(this,R.layout.activity_select_consultant);

        ItemType<ItemConsultantSelectBinding> itemtype = new ItemType<ItemConsultantSelectBinding>(R.layout.item_consultant_select) {
            @Override
            public void onBind(@NotNull final Holder<ItemConsultantSelectBinding> holder) {

                holder.getBinding().btSelect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       // Toast.makeText(getApplicationContext(),"selected : " + holder.getBinding().getCarditem().name,Toast.LENGTH_SHORT).show();

                        _consultantpref.consultantname().put(holder.getBinding().getCarditem().name);
                        _consultantpref.consultantid().put(holder.getBinding().getCarditem().id);

                        SelectWaitingPatientActivity_.intent(SelectConsultantActivity.this).start();


                    }
                });
            }
        };

        new LastAdapter(consultantcards, BR.carditem)
                .map(Consultant.class,itemtype)
                .into(vars.rvhospitallist);

        downloadConsultants();


        vars.btrefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadConsultants();
            }
        });
    }


    void handleclicks()
    {

    }
    void handleMenu()
    {

    }
    void execute()
    {

    }

    @UiThread
    void fillconsultants(List<Consultant> hospitals)
    {
        consultantcards.clear();
        for(int i=0;i<hospitals.size();i++) {
            // Wlog.i("hospital : "   + response.data.get(i).name) ;

            consultantcards.add(hospitals.get(i));
        }

    }
    void downloadConsultants()
    {
        new HttpCall<CrudModel<Consultant>>()
        {
            @Override
            protected void onComplete(CrudModel<Consultant> response) {
                super.onComplete(response);

                fillconsultants(response.data);

            }
        }.httpConnect(HttpCall.APIS.getlistofconsultants());
    }



}
