package projectspecificactivities.selectdepartment;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.databinding.library.baseAdapters.BR;
import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;
import com.google.android.gms.maps.model.LatLng;
import com.whitesun.sparshhospital.R;
import com.whitesun.sparshhospital.databinding.ActivitySelectDepartmentBinding;
import com.whitesun.sparshhospital.databinding.ActivitySelectHospitalBinding;
import com.whitesun.sparshhospital.databinding.ItemHospitalSelectBinding;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.List;

import httpdata.projectspecificmodels.CrudModel;
import httpdata.projectspecificmodels.Hospital;
import httpdata.projectspecificmodels.PhotoPoint;
import lib.gps.GpsLocation;
import lib.rest.HttpCall;
import lib.rest.HttpUpload;
import lib.ui.ProgDialog;
import lib.utils.common.Wlog;
import lib.utils.file.FileUtils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import projectspecificactivities.capturedphoto.SelectREMS_;
import projectspecificactivities.opdreport.PatientOpdReportActivity;
import projectspecificactivities.selectconsultant.SelectConsultantActivity_;
import projectspecificactivities.selectopdpatient.SelectWaitingPatientActivity;
import projectspecificactivities.sharedpref.HospitalPreferences_;
import projectspecificactivities.sharedpref.LoginPreferences_;

@Fullscreen
@EActivity
public class SelectDepartment extends Activity {

    ActivitySelectDepartmentBinding vars;

    ObservableArrayList<PhotoPoint> photopointcards = new ObservableArrayList<>();

    @Pref
    LoginPreferences_ _loginpref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        handleclicks();
        handleMenu();
        execute();
    }


    void init()
    {
        vars = DataBindingUtil.setContentView(this,R.layout.activity_select_department);

        ItemType<ItemHospitalSelectBinding> itemtype = new ItemType<ItemHospitalSelectBinding>(R.layout.item_hospital_select) {
            @Override
            public void onBind(@NotNull final Holder<ItemHospitalSelectBinding> holder) {

                holder.getBinding().btSelect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //sendDataToActivity(holder.getBinding().getCarditem());

                        ProgDialog.show(SelectDepartment.this,"Capturing GPS",true);

                       final GpsLocation gps =  new GpsLocation(SelectDepartment.this)
                        {
                            @Override
                            protected void onLocationChange(Location loc, Double distancefromprevloc, String gpsstring) {
                                super.onLocationChange(loc, distancefromprevloc, gpsstring);

                                this.stop();


                                new HttpCall<CrudModel<ResponseBody>>()
                                {
                                    @Override
                                    protected void onComplete(CrudModel<ResponseBody> response) {
                                        super.onComplete(response);

                                    }
                                }.httpConnect(HttpCall.APIS.updategps(loc.getLatitude() + "",loc.getLongitude() + "",_loginpref.passcode().getOr(""),holder.getBinding().getCarditem().id ));


                            }
                        };


                       // SelectConsultantActivity_.intent(SelectDepartment.this).start();
                    }
                });
            }
        };

        new LastAdapter(photopointcards, BR.carditem)
                .map(PhotoPoint.class,itemtype)
                .into(vars.rvhospitallist);

        downloadhospitals();


        vars.btrefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadhospitals();
            }
        });

        vars.btrems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SelectREMS_.intent(SelectDepartment.this).start();
            }
        });

    }



    void handleclicks()
    {

    }
    void handleMenu()
    {

    }
    void execute()
    {

    }

    @UiThread
    void fillphotopoints(List<PhotoPoint> photoPoints)
    {
        photopointcards.clear();
        for(int i=0;i<photoPoints.size();i++) {
            // Wlog.i("hospital : "   + response.data.get(i).name) ;

            if(photoPoints.get(i).department_id.contains(_loginpref.department().getOr(""))) {
                photopointcards.add(photoPoints.get(i));
            }
        }

    }
    void downloadhospitals()
    {
        new HttpCall<CrudModel<PhotoPoint>>()
        {
            @Override
            protected void onComplete(CrudModel<PhotoPoint> response) {
                super.onComplete(response);

                /*hospitalcards.clear();
                for(int i=0;i<response.data.size();i++) {
                   // Wlog.i("hospital : "   + response.data.get(i).name) ;

                    hospitalcards.add(response.data.get(i));
                }*/
                fillphotopoints(response.data);



            }
        }.httpConnect(HttpCall.APIS.getlistofphotopoints());
    }



}
