package projectspecificactivities.selecthospital;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Toast;

import com.android.databinding.library.baseAdapters.BR;
import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;
import com.whitesun.sparshhospital.R;
import com.whitesun.sparshhospital.databinding.ActivitySelectHospitalBinding;
import com.whitesun.sparshhospital.databinding.ItemHospitalSelectBinding;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import httpdata.models.android.LocationCard;
import httpdata.projectspecificmodels.CrudModel;
import httpdata.projectspecificmodels.Hospital;
import lib.map.MapRoute;
import lib.rest.HttpCall;
import lib.rest.HttpUpload;
import lib.utils.common.Wlog;
import lib.utils.string.StringUtils;
import okhttp3.ResponseBody;
import projectspecificactivities.selectconsultant.SelectConsultantActivity;
import projectspecificactivities.selectconsultant.SelectConsultantActivity_;
import projectspecificactivities.sharedpref.ConsultantPreferences;
import projectspecificactivities.sharedpref.HospitalPreferences;
import projectspecificactivities.sharedpref.HospitalPreferences_;

@Fullscreen
@EActivity
public class SelectHospitalActivity extends Activity {

    ActivitySelectHospitalBinding vars;

    ObservableArrayList<Hospital> hospitalcards = new ObservableArrayList<>();


    @Pref
    HospitalPreferences_ _hospitalpref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        handleclicks();
        handleMenu();
        execute();
    }


    void init()
    {
        vars = DataBindingUtil.setContentView(this,R.layout.activity_select_hospital);

        ItemType<ItemHospitalSelectBinding> itemtype = new ItemType<ItemHospitalSelectBinding>(R.layout.item_hospital_select) {
            @Override
            public void onBind(@NotNull final Holder<ItemHospitalSelectBinding> holder) {

                holder.getBinding().btSelect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //sendDataToActivity(holder.getBinding().getCarditem());
                        Toast.makeText(getApplicationContext(),"selected : " + holder.getBinding().getCarditem().name,Toast.LENGTH_SHORT).show();


                        _hospitalpref.hospitalname().put(holder.getBinding().getCarditem().name);
                        _hospitalpref.hospitalid().put(holder.getBinding().getCarditem().id);

                        SelectConsultantActivity_.intent(SelectHospitalActivity.this).start();
                    }
                });
            }
        };

        new LastAdapter(hospitalcards, BR.carditem)
                .map(Hospital.class,itemtype)
                .into(vars.rvhospitallist);

        downloadhospitals();


        vars.btrefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadhospitals();
            }
        });
    }



    void handleclicks()
    {

    }
    void handleMenu()
    {

    }
    void execute()
    {

    }

    @UiThread
    void fillhospitals(List<Hospital> hospitals)
    {
        hospitalcards.clear();
        for(int i=0;i<hospitals.size();i++) {
            // Wlog.i("hospital : "   + response.data.get(i).name) ;

            hospitalcards.add(hospitals.get(i));
        }

    }
    void downloadhospitals()
    {
        new HttpCall<CrudModel<Hospital>>()
        {
            @Override
            protected void onComplete(CrudModel<Hospital> response) {
                super.onComplete(response);

                /*hospitalcards.clear();
                for(int i=0;i<response.data.size();i++) {
                   // Wlog.i("hospital : "   + response.data.get(i).name) ;

                    hospitalcards.add(response.data.get(i));
                }*/
                fillhospitals(response.data);



            }
        }.httpConnect(HttpCall.APIS.getlistofhospitals());
    }



}
