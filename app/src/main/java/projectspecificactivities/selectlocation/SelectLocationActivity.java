package projectspecificactivities.selectlocation;

import com.android.databinding.library.baseAdapters.BR;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.whitesun.sparshhospital.R;
import com.whitesun.sparshhospital.databinding.ActivitySelectLocationBinding;
import com.whitesun.sparshhospital.databinding.ItemSelectLocationBinding;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.List;

import configuration.DirectApiConfig;
import httpdata.projectspecificmodels.BreweryUsersLogin;
import httpdata.projectspecificmodels.CrudModel;
import httpdata.projectspecificmodels.LocationProject;
import lib.rest.HttpCall;
import lib.ui.ProgDialog;
import lib.utils.datautils.JsonUtil;
import projectspecificactivities.login.LoginActivity;
import projectspecificactivities.login.LoginActivity_;
import projectspecificactivities.selectchecklist.SelectChecklistActivity_;
import projectspecificactivities.selectproject.SelectProjectActivity_;
import projectspecificactivities.sharedpref.LoginPreferences_;

@Fullscreen
@EActivity
public class SelectLocationActivity extends AppCompatActivity {

    ActivitySelectLocationBinding vars;
    ObservableArrayList<LocationProject> listLocationProject = new ObservableArrayList<>();

    @Pref
    LoginPreferences_ _logpref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();
        handleclicks();
        execute();

    }

    void init() {
        vars = DataBindingUtil.setContentView(this, R.layout.activity_select_location);
        vars.userName.setText("Welcome " + _logpref.name().get());

        ItemType<ItemSelectLocationBinding> itemtype = new ItemType<ItemSelectLocationBinding>(R.layout.item_select_location) {
            @Override
            public void onBind(@NotNull final Holder<ItemSelectLocationBinding> holder) {

                holder.getBinding().cvLocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        _logpref.locationID().put(Integer.parseInt("" + holder.getBinding().locID.getText()));
                        _logpref.projectLocationDefID().put(Integer.parseInt("" + holder.getBinding().proLocDefID.getText()));
                        _logpref.locationName().put(holder.getBinding().locName.getText().toString());


                        SelectProjectActivity_.intent(SelectLocationActivity.this).start();
                    }
                });
            }
        };

        new LastAdapter(listLocationProject, BR.selectlocation)
                .map(LocationProject.class, itemtype)
                .into(vars.rvSelectLoacation);

        // downloadProjectAndLoacation();
        getOfflineUsersData();
        vars.btrefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadProjectAndLoacation();
            }
        });
        vars.btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginActivity_.intent(SelectLocationActivity.this).start();
                finish();
            }
        });
        vars.btLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginActivity_.intent(SelectLocationActivity.this).start();
                finish();
            }
        });


    }

    void handleclicks() {
    }

    void execute() {
    }

    @UiThread
    void fillLocation(List<LocationProject> locationProject) {
        listLocationProject.clear();
        for (int i = 0; i < locationProject.size(); i++) {
            listLocationProject.add(locationProject.get(i));
        }

    }


    void downloadProjectAndLoacation() {
        ProgDialog.show(this, "Downloading Project and Location for You", true);
        new HttpCall<CrudModel<LocationProject>>() {

            @Override
            protected void onComplete(CrudModel<LocationProject> response) {

                super.onComplete(response);
                ProgDialog.close();
//                Log.d("download_users_location", String.valueOf(response.data.get(1)));
                fillLocation(response.data);
            }

            @Override
            protected void onError(String message) {
                super.onError(message);

            }
        }.httpConnect(HttpCall.APIS.getlocationproject(_logpref.userID().get()));

    }


    void getOfflineUsersData() {
        String jsonstring = JsonUtil.getJsonStringFromInternalFile(SelectLocationActivity.this, DirectApiConfig.DF_MOBUSERPROJECTROLESDEF);
        Log.d("aa", "LoginActivity: " + "getOfflineUsersData: " + "[]: " + "DF_MOBUSERPROJECTROLESDEF: " + jsonstring);
        Type type1 = new TypeToken<CrudModel<LocationProject>>() {
        }.getType();
        CrudModel<LocationProject> projectusers = new Gson().fromJson(jsonstring, type1);
        fillLocation(projectusers.data);
    }


}
