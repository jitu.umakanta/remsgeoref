package projectspecificactivities.selectopdpatient;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.databinding.library.baseAdapters.BR;
import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;
import com.whitesun.sparshhospital.R;
import com.whitesun.sparshhospital.databinding.ActivitySelectHospitalBinding;
import com.whitesun.sparshhospital.databinding.ActivitySelectWaitingPatientBinding;
import com.whitesun.sparshhospital.databinding.ItemHospitalSelectBinding;
import com.whitesun.sparshhospital.databinding.ItemOpdpatientSelectBinding;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.jetbrains.annotations.NotNull;
import org.parceler.Parcels;

import java.util.List;

import httpdata.projectspecificmodels.CrudModel;
import httpdata.projectspecificmodels.Hospital;
import httpdata.projectspecificmodels.OutpatientRecord;
import lib.rest.HttpCall;
import lib.utils.common.Wlog;
import projectspecificactivities.opdreport.PatientOpdReportActivity;
import projectspecificactivities.opdreport.PatientOpdReportActivity_;
import projectspecificactivities.selectconsultant.SelectConsultantActivity_;
import projectspecificactivities.selecthospital.SelectHospitalActivity;
import projectspecificactivities.selecthospital.SelectHospitalActivity_;
import projectspecificactivities.sharedpref.ConsultantPreferences_;
import projectspecificactivities.sharedpref.HospitalPreferences_;

@Fullscreen
@EActivity
public class SelectWaitingPatientActivity extends AppCompatActivity {

    ActivitySelectWaitingPatientBinding vars;

    ObservableArrayList<OutpatientRecord> opdcards = new ObservableArrayList<>();


    @Pref
    HospitalPreferences_ _hospitalpref;

    @Pref
    ConsultantPreferences_ _consultantpref;



    public static void start(Context context)
    {
        Intent starter = new Intent(context, SelectWaitingPatientActivity_.class);
        starter.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(starter);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!checkIfValidConsultant())
        {
            SelectHospitalActivity_.intent(this).start();
            finish();
        }
        init();
        handleclicks();
        handleMenu();
        execute();
    }

    boolean checkIfValidConsultant()
    {
        if(
                (_consultantpref.consultantid().getOr(-1) != -1)
                        &&  (_hospitalpref.hospitalid().getOr(-1) != -1)
                )
        {
            return true;
        }

        return false;
    }

    void init()
    {
        vars = DataBindingUtil.setContentView(this,R.layout.activity_select_waiting_patient);

        ItemType<ItemOpdpatientSelectBinding> itemtype = new ItemType<ItemOpdpatientSelectBinding>(R.layout.item_opdpatient_select) {
            @Override
            public void onBind(@NotNull final Holder<ItemOpdpatientSelectBinding> holder) {

                holder.getBinding().btSelect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        PatientOpdReportActivity.start(SelectWaitingPatientActivity.this,holder.getBinding().getCarditem());

                    }
                });
            }
        };

        new LastAdapter(opdcards, BR.carditem)
                .map(OutpatientRecord.class,itemtype)
                .into(vars.rvopdlist);

        downloadopd();


        vars.btrefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadopd();
            }
        });
    }


    void handleclicks()
    {

    }
    void handleMenu()
    {

    }
    void execute()
    {

    }

    @UiThread
    void fillopd(List<OutpatientRecord> opdlist)
    {
        opdcards.clear();
        for(int i=0;i<opdlist.size();i++) {
            Wlog.i("opd : "   + opdlist.get(i).patientname) ;

            opdcards.add(opdlist.get(i));
        }

    }
    void downloadopd()
    {
        new HttpCall<CrudModel<OutpatientRecord>>()
        {
            @Override
            protected void onComplete(CrudModel<OutpatientRecord> response) {
                super.onComplete(response);

                fillopd(response.data);



            }

            @Override
            protected void onError(String message) {
                super.onError(message);
                Wlog.i("error !!" + message);
            }
        }.httpConnectWithProgress(this,getString(R.string.t_loadingwaitingpatients),true,HttpCall.APIS.getWaitingPatients());
    }

}
