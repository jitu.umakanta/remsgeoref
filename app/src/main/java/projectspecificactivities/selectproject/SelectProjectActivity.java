package projectspecificactivities.selectproject;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.android.databinding.library.baseAdapters.BR;
import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;
import com.whitesun.sparshhospital.R;
import com.whitesun.sparshhospital.databinding.ActivitySelectProjectBinding;
import com.whitesun.sparshhospital.databinding.ItemSelectProjectBinding;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import httpdata.projectspecificmodels.CrudModel;
import httpdata.projectspecificmodels.LocationProject;
import httpdata.projectspecificmodels.UserProject;
import lib.rest.HttpCall;
import lib.ui.ProgDialog;
import projectspecificactivities.login.LoginActivity_;
import projectspecificactivities.selectrole.SelectRoleActivity_;
import projectspecificactivities.sharedpref.LoginPreferences_;

@Fullscreen
@EActivity
public class SelectProjectActivity extends AppCompatActivity {

    ActivitySelectProjectBinding vars;
    ObservableArrayList<UserProject> listLocationProject = new ObservableArrayList<>();


    @Pref
    LoginPreferences_ _logpref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();
        handleclicks();
        execute();

    }

    void init() {
        vars = DataBindingUtil.setContentView(this, R.layout.activity_select_project);
        vars.tvLocation.setText(_logpref.locationName().get());
        vars.userName.setText("Welcome " + _logpref.name().get());
        int ss = _logpref.projectLocationDefID().get();


        ItemType<ItemSelectProjectBinding> itemtype = new ItemType<ItemSelectProjectBinding>(R.layout.item_select_project) {
            @Override
            public void onBind(@NotNull final Holder<ItemSelectProjectBinding> holder) {

                holder.getBinding().cvProject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        _logpref.projectDefID().put(Integer.parseInt("" + holder.getBinding().tvProjectId.getText()));
                        _logpref.projectName().put(holder.getBinding().tvProjectName.getText().toString());
                        SelectRoleActivity_.intent(SelectProjectActivity.this).start();
                    }
                });
            }
        };

        new LastAdapter(listLocationProject, BR.selectproject)
                .map(UserProject.class, itemtype)
                .into(vars.rvSelectProject);

        downloadProjectAndLoacation();

        vars.btrefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadProjectAndLoacation();
            }
        });

        vars.btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        vars.btLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginActivity_.intent(SelectProjectActivity.this).start();
                finish();
            }
        });

    }

    void handleclicks() {
    }

    void execute() {
    }

    @UiThread
    void fillLocation(List<UserProject> locationProject) {
        listLocationProject.clear();
        for (int i = 0; i < locationProject.size(); i++) {
            listLocationProject.add(locationProject.get(i));
        }

    }


    void downloadProjectAndLoacation() {
        ProgDialog.show(this, "Downloading Project and Location for You", true);
        new HttpCall<CrudModel<UserProject>>() {

            @Override
            protected void onComplete(CrudModel<UserProject> response) {

                super.onComplete(response);
                ProgDialog.close();
//                Log.d("download_users_location", String.valueOf(response.data.get(1)));

                fillLocation(response.data);
            }

            @Override
            protected void onError(String message) {
                super.onError(message);

            }
        }.httpConnect(HttpCall.APIS.getuserproject(_logpref.userID().get(), _logpref.projectLocationDefID().get()));

    }
}
