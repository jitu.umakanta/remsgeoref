package projectspecificactivities.selectrole;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.android.databinding.library.baseAdapters.BR;
import com.github.nitrico.lastadapter.Holder;
import com.github.nitrico.lastadapter.ItemType;
import com.github.nitrico.lastadapter.LastAdapter;
import com.whitesun.sparshhospital.R;
import com.whitesun.sparshhospital.databinding.ActivitySelectLocationBinding;
import com.whitesun.sparshhospital.databinding.ActivitySelectRoleBinding;
import com.whitesun.sparshhospital.databinding.ItemSelectLocationBinding;
import com.whitesun.sparshhospital.databinding.ItemSelectRoleBinding;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import httpdata.projectspecificmodels.CrudModel;
import httpdata.projectspecificmodels.LocationProject;
import httpdata.projectspecificmodels.UserRole;
import lib.rest.HttpCall;
import lib.ui.ProgDialog;
import projectspecificactivities.login.LoginActivity_;
import projectspecificactivities.selectchecklist.SelectChecklistActivity_;
import projectspecificactivities.selectlocation.SelectLocationActivity;
import projectspecificactivities.selectproject.SelectProjectActivity_;
import projectspecificactivities.sharedpref.LoginPreferences_;

@Fullscreen
@EActivity
public class SelectRoleActivity extends AppCompatActivity {


    ActivitySelectRoleBinding vars;
    ObservableArrayList<UserRole> listLocationProject = new ObservableArrayList<>();

    @Pref
    LoginPreferences_ _logpref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();
        handleclicks();
        execute();

    }
    void init(){
        vars= DataBindingUtil.setContentView(this,R.layout.activity_select_role);
        vars.userName.setText("Welcome "+_logpref.name().get());
        vars.tvLocation.setText(_logpref.locationName().get()+" > "+_logpref.projectName().get());

        ItemType<ItemSelectRoleBinding> itemtype = new ItemType<ItemSelectRoleBinding>(R.layout.item_select_role) {
            @Override
            public void onBind(@NotNull final Holder<ItemSelectRoleBinding> holder) {

                holder.getBinding().cvRole.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        _logpref.userRoleID().put(Integer.parseInt(""+holder.getBinding().roleID.getText()));
                        _logpref.userRoleName().put(holder.getBinding().roleName.getText().toString());

                        SelectChecklistActivity_.intent(SelectRoleActivity.this).start();
                    }
                });
            }
        };

        new LastAdapter(listLocationProject, BR.selectrole)
                .map(UserRole.class,itemtype)
                .into(vars.rvSelectRole);

        downloadProjectAndLoacation();

        vars.btrefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadProjectAndLoacation();
            }
        });

        vars.btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        vars.btLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginActivity_.intent(SelectRoleActivity.this).start();
                finish();
            }
        });


    }
    void handleclicks(){}
    void execute(){}

    @UiThread
    void fillLocation(List<UserRole> locationProject)
    {
        listLocationProject.clear();
        for(int i=0;i<locationProject.size();i++) {
            listLocationProject.add(locationProject.get(i));
        }

    }


    void downloadProjectAndLoacation(){
        ProgDialog.show(this,"Downloading Project and Location for You",true);
        new HttpCall<CrudModel<UserRole>>()
        {

            @Override
            protected void onComplete(CrudModel<UserRole> response) {

                super.onComplete(response);
                ProgDialog.close();
//                Log.d("download_users_location", String.valueOf(response.data.get(1)));
                fillLocation(response.data);
            }

            @Override
            protected void onError(String message) {
                super.onError(message);

            }
        }.httpConnect(HttpCall.APIS.getuserroles(_logpref.userID().get(),_logpref.projectLocationDefID().get()));
    }
}
