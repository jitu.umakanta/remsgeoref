package projectspecificactivities.sharedpref;

import org.androidannotations.annotations.sharedpreferences.DefaultInt;
import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by sumeendranath on 13/09/17.
 */

@SharedPref(value=SharedPref.Scope.UNIQUE)
public interface ConsultantPreferences {

    @DefaultInt(-1)
    int consultantid();

    @DefaultString("")
    String consultantname();
}
