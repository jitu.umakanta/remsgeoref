package projectspecificactivities.sharedpref;

import org.androidannotations.annotations.sharedpreferences.DefaultInt;
import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by sumeendranath on 13/09/17.
 */

@SharedPref(value=SharedPref.Scope.UNIQUE)
public interface LoginPreferences {

    @DefaultString("")
    String passcode();

    @DefaultString("")
    String department();

    @DefaultString("")
    String name();

    @DefaultString("")
    String lastlogintime();

    @DefaultString("")
    String lastexittime();

    @DefaultInt(-1)
    int userID();

    @DefaultInt(-1)
    int locationID();

    @DefaultInt(-1)
    int projectLocationDefID();

    @DefaultString("")
    String locationName();

    @DefaultInt(-1)
    int projectDefID();

    @DefaultString("")
    String projectName();

    @DefaultString("")
    String userRoleName();

    @DefaultInt(-1)
    int userRoleID();

    @DefaultString("")
    String checklistName();

    @DefaultInt(-1)
    int checklistID();

    @DefaultString("")
    String questionGroupName();

    @DefaultInt(-1)
    int questionGroupId();

    @DefaultString("")
    String questionGroupDescription();
}
