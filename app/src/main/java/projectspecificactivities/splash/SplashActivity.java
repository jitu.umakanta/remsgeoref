package projectspecificactivities.splash;

import android.databinding.ObservableArrayList;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;


import com.google.gson.Gson;
import com.whitesun.sparshhospital.R;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.UiThread;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import configuration.DirectApiConfig;
import generic.activities.CheckPermissionsActivity_;
import httpdata.projectspecificmodels.CrudModel;
import httpdata.projectspecificmodels.UserRoleTasklist;
import httpdata.restapi.APIInterface;
import lib.rest.APIClient;
import lib.rest.HttpCall;
import lib.timer.ScheduleTask;
import lib.timer.SetDelay;
import lib.ui.ProgDialog;
import lib.utils.date.DateUtils;
import lib.vars.Values;
import notification.Notification;
import okhttp3.ResponseBody;
import projectspecificactivities.login.LoginActivity_;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Fullscreen
@EActivity
public class SplashActivity extends AppCompatActivity {
    ObservableArrayList<UserRoleTasklist> listLocationProject12 = new ObservableArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        init();
        handleclicks();
        execute();

    }

    void init() {
        downloadTaskLists();
        //by me no use
       // downloadOfflineAllDataAtOnce();

//        for (int i = 0; i < listLocationProject12.size(); i++) {
//            String starttime = listLocationProject12.get(i).start_time;
//            String interval = listLocationProject12.get(i).period;
//
//            //converting interval to period....and...starttime to date
//            int period = Integer.parseInt(interval);
//            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
//            Date date = new Date();
//            try {
//                date = formatter.parse(starttime);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            Log.d("aa", "SplashActivity: " + "onDelayCompleted: " + "[]: " + "date: " + date);
//            //timer
//            Timer timer = new Timer();
//            timer.schedule(new TimerTask() {
//                @Override
//                public void run() {
//                    new Notification(getApplicationContext()).Notification();
//                }
//            }, date, period);
//        }
        // 03:04 PM " 23-11-2017  " by jitu '
        //displaying notification
        SetDelay sd = new SetDelay(3000);
        sd.setOnDelayCompletedListener(new SetDelay.OnDelayCompletedListener() {
            @Override
            public void onDelayCompleted() {
                for (int i = 0; i < listLocationProject12.size(); i++) {
                    try {
                        String interval = listLocationProject12.get(i).period;
                        int period = Integer.parseInt(interval);

                        Log.d("aa", "SplashActivity: " + "init: " + "[]: " + "period: " + period);
                        int delay = period * 1;
                        SetDelay sd = new SetDelay(delay);
                        sd.setOnDelayCompletedListener(new SetDelay.OnDelayCompletedListener() {
                            @Override
                            public void onDelayCompleted() {
                                new Notification(getApplicationContext()).Notification();
                            }
                        });
                    } catch (Exception e) {
                        Log.d("aa", "SplashActivity: " + "init: " + "[]: " + "exception: ");
                    }
                }
            }
        });

    }

    void handleclicks() {
    }

    void execute() {

        SetDelay sd = new SetDelay(2000);
        sd.setOnDelayCompletedListener(new SetDelay.OnDelayCompletedListener() {
            @Override
            public void onDelayCompleted() {
                LoginActivity_.intent(SplashActivity.this).start();
                //SelectWaitingPatientActivity_.intent(SplashActivity.this).start();
                nextAction();
            }
        });
    }


    void nextAction() {
        CheckPermissionsActivity_.intent(SplashActivity.this).start();
        finish();
    }




    /**
     * download all the task list of the user without any input parameter
     */
    void downloadTaskLists() {
        ProgDialog.show(this, "Downloading TaskList", true);
        new HttpCall<CrudModel<UserRoleTasklist>>() {

            @Override
            protected void onComplete(CrudModel<UserRoleTasklist> response) {

                super.onComplete(response);
                ProgDialog.close();
                listLocationProject12.clear();
                for (int i = 0; i < response.data.size(); i++) {
                    String startTime = response.data.get(i).start_time;
                    String period = response.data.get(i).period;
                    listLocationProject12.add(response.data.get(i));
                }


            }

            @Override
            protected void onError(String message) {
                super.onError(message);

            }
        }.httpConnect(HttpCall.APIS.getusertask());
    }


    /*

    *//**
     * download the zip folder which contain all the local json file
     *//*
    public void downloadOfflineAllDataAtOnce() {

        //  FileDownloadService downloadService = ServiceGenerator.create(FileDownloadService.class);
        APIInterface getResponse = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> call = getResponse.downloadFileWithFixedUrl();

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("aa", "SplashActivity: " + "onResponse: " + "[call, response]: " + "zip response: " + response.body());
                // boolean writtenToDisk = writeResponseBodyToDisk(response.body());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("ll", "error");
            }
        });
    }

    public void getAllJsonStringFromJsonFile() {

        String jsonString = inputStreamToString(getResources().openRawResource(R.raw.amu_ground_overlay));
        CrudModel myModel = new Gson().fromJson(jsonString, CrudModel.class);


    }


    *//**
     * convert local json file to json string
     *
     * @param inputStream
     * @return
     *//*
    public String inputStreamToString(InputStream inputStream) {
        try {
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes, 0, bytes.length);
            String json = new String(bytes);
            return json;
        } catch (IOException e) {
            return null;
        }
    }

*/
}
