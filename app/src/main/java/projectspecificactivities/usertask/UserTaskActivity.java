package projectspecificactivities.usertask;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.whitesun.sparshhospital.R;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;

@Fullscreen
@EActivity
public class UserTaskActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_task);
    }
}
